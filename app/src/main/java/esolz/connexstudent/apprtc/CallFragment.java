package esolz.connexstudent.apprtc;

import android.app.Activity;
import android.app.Dialog;
import android.app.Fragment;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.Color;
import android.media.AudioManager;
import android.os.Bundle;
import android.os.Handler;
import android.text.TextUtils;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.SeekBar;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import org.webrtc.RendererCommon.ScalingType;

import java.util.zip.Inflater;

import esolz.connexstudent.R;
import esolz.connexstudent.customview.AvenirLight;
import esolz.connexstudent.customview.AvenirRegular;
import esolz.connexstudent.database.ConnexDB;


/**
 * Fragment for call control.
 */
public class CallFragment extends Fragment {
    private View controlView;
    private AvenirRegular contactView;
    private ImageView disconnectButton, muteUnMute, muteUnMuteAudio;
    private ImageView cameraSwitchButton, callerViewMain;
    private ImageView videoStreaminButton, speakerManagement;
    private ImageButton videoScalingButton;
    private TextView captureFormatText;
    private SeekBar captureFormatSlider;
    private OnCallEvents callEvents;
    private ScalingType scalingType;
    private boolean videoCallEnabled = true;
    private boolean isMute = false;
    private AudioManager audioManager = null;
    private View connectingText = null;
    private AudioManager mAudioMgr = null;
    private final String TAG = "CallFragment";
    private ConnexDB cDB = null;
    private boolean isSpeakerOn = false;
    private Dialog dLog = null;

    //============call waiting service management


    private CallBrodCastReceiver receiver = null;


    private AudioInOutChangeReceiver audioInOutReceiver = null;

    /**
     * Call control interface for container activity.
     */
    public interface OnCallEvents {
        public void onCallHangUp();

        public void onCameraSwitch();

        public void onVideoScalingSwitch(ScalingType scalingType);

        public void onCaptureFormatChange(int width, int height, int framerate);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        controlView = inflater.inflate(R.layout.fragment_call, container, false);





        cDB = new ConnexDB(getActivity());

        // Create UI controls.
        connectingText = controlView.findViewById(R.id.connecting_status);
        contactView = (AvenirRegular) controlView.findViewById(R.id.contact_name_call);

        disconnectButton = (ImageView) controlView.findViewById(R.id.button_call_disconnect);
        videoStreaminButton = (ImageView) controlView.findViewById(R.id.onoffvideocall);
        cameraSwitchButton = (ImageView) controlView.findViewById(R.id.button_call_switch_camera);
        callerViewMain = (ImageView) controlView.findViewById(R.id.callerview);

        videoScalingButton = (ImageButton) controlView.findViewById(R.id.button_call_scaling_mode);//=====Gone


        captureFormatText = (TextView) controlView.findViewById(R.id.capture_format_text_call);

        captureFormatSlider = (SeekBar) controlView.findViewById(R.id.capture_format_slider_call);

        muteUnMute = (ImageView) controlView.findViewById(R.id.mute_unmute_iview);
        muteUnMuteAudio = (ImageView) controlView.findViewById(R.id.mute_management);
        //======================


        controlView.findViewById(R.id.mute_unmute).setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {

                audioManager = (AudioManager) getActivity().getSystemService(Context.AUDIO_SERVICE);
                audioManager.isMicrophoneMute();



                if (isMute) {
                    isMute = false;
                    audioManager.setMicrophoneMute(false);
                    muteUnMute.setBackgroundResource(R.drawable.ic_action_mic_off);
                } else {
                    isMute = true;
                    audioManager.setMicrophoneMute(true);
                    muteUnMute.setBackgroundResource(R.drawable.ic_action_mic_on);
                }

            }
        });


        //======mute unmute for voice call only
        muteUnMuteAudio.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {

                audioManager = (AudioManager) getActivity().getSystemService(Context.AUDIO_SERVICE);
                audioManager.isMicrophoneMute();
                if (isMute) {
                    isMute = false;
                    audioManager.setMicrophoneMute(false);
                    muteUnMuteAudio.setBackgroundResource(R.drawable.mute);

                } else {
                    isMute = true;
                    audioManager.setMicrophoneMute(true);
                    muteUnMuteAudio.setBackgroundResource(R.drawable.unmute);

                }

            }
        });


        controlView.findViewById(R.id.live_streaming_toggle).setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                try {
                    if (((CallActivity) getActivity()).manageVideoStreaming()) {
                        videoStreaminButton.setBackgroundResource(R.drawable.ic_action_videocam_off);
                    } else {
                        videoStreaminButton.setBackgroundResource(R.drawable.ic_action_videocam);
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }

            }
        });

        // Add buttons click events.
        disconnectButton.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                callEvents.onCallHangUp();
            }
        });

        cameraSwitchButton.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                callEvents.onCameraSwitch();
            }
        });

        videoScalingButton.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                if (scalingType == ScalingType.SCALE_ASPECT_FILL) {
                    videoScalingButton.setBackgroundResource(
                            R.drawable.ic_action_full_screen);
                    scalingType = ScalingType.SCALE_ASPECT_FIT;
                } else {
                    videoScalingButton.setBackgroundResource(
                            R.drawable.ic_action_return_from_full_screen);
                    scalingType = ScalingType.SCALE_ASPECT_FILL;
                }
                callEvents.onVideoScalingSwitch(scalingType);
            }
        });


        //==============speaker management


        mAudioMgr = (AudioManager) getActivity().getSystemService(Context.AUDIO_SERVICE);

        speakerManagement = (ImageView) controlView.findViewById(R.id.speaker_on_off);

        if (mAudioMgr.isSpeakerphoneOn()) {
            isSpeakerOn = true;
            speakerManagement.setBackgroundColor(Color.GREEN);
        } else {
            speakerManagement.setBackgroundColor(Color.RED);
        }

        speakerManagement.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (isSpeakerOn) {
                    isSpeakerOn = false;
                    mAudioMgr.setSpeakerphoneOn(false);
                    speakerManagement.setBackgroundColor(Color.RED);
                } else {
                    isSpeakerOn = true;
                    mAudioMgr.setSpeakerphoneOn(true);
                    speakerManagement.setBackgroundColor(Color.GREEN);
                }
            }
        });


        scalingType = ScalingType.SCALE_ASPECT_FILL;

        return controlView;
    }

    @Override
    public void onStart() {
        super.onStart();

        boolean captureSliderEnabled = false;
        Bundle args = getArguments();
        if (args != null) {
            String contactName = args.getString(CallActivity.EXTRA_ROOMID);
            contactView.setText(contactName);
            videoCallEnabled = args.getBoolean(CallActivity.EXTRA_VIDEO_CALL, true);
            captureSliderEnabled = videoCallEnabled
                    && args.getBoolean(CallActivity.EXTRA_VIDEO_CAPTUREQUALITYSLIDER_ENABLED, false);
        }
        if (!videoCallEnabled) {//------video call is not on
            cameraSwitchButton.setVisibility(View.GONE);
            callerViewMain.setVisibility(View.VISIBLE);
            controlView.findViewById(R.id.live_streaming_toggle).setVisibility(View.GONE);
            controlView.findViewById(R.id.mute_unmute).setVisibility(View.GONE);
            controlView.findViewById(R.id.overlay).setVisibility(View.VISIBLE);
            controlView.findViewById(R.id.voicecall_holder).setVisibility(View.VISIBLE);
            Picasso.with(getActivity()).load(args.getString("PROFILE_IMAGE")).fit().centerCrop().into(callerViewMain);


            //-------calller name showing


            ((AvenirLight) controlView.findViewById(R.id.caller_name)).setText("" + args.getString("CALLER_NAME"));


        } else {

            controlView.findViewById(R.id.voicecall_holder).setVisibility(View.GONE);
        }
        if (captureSliderEnabled) {
            captureFormatSlider.setOnSeekBarChangeListener(
                    new CaptureQualityController(captureFormatText, callEvents));
        } else {
            captureFormatText.setVisibility(View.GONE);
            captureFormatSlider.setVisibility(View.GONE);
        }
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        callEvents = (OnCallEvents) activity;



    }


    public void hideConnecting() {
        connectingText.setVisibility(View.GONE);
    }






    //==========some code testing for headphone problem solving


    public class AudioInOutChangeReceiver extends BroadcastReceiver {

        private static final String ACTION_HEADSET_PLUG_STATE = "android.intent.action.HEADSET_PLUG";

        private AudioInOutChangeReceiver(Context aContext) {
            Log.d(TAG, "AudioInOutChangeReceiver()");
        }

        @Override
        public void onReceive(Context context, Intent intent) {
            // initial audio manager
//            initiAudioManager(context);

            final String action = intent.getAction();
            // check action should not empty
            if (TextUtils.isEmpty(action)) return;

            //Broadcast Action: Wired Headset plugged in or unplugged.
            if (action.equals(ACTION_HEADSET_PLUG_STATE)) {
                updateHeadSetDeviceInfo(context, intent);

            }
        }

        public void updateHeadSetDeviceInfo(Context context, Intent intent) {
            //0 for unplugged, 1 for plugged.
            int state = intent.getIntExtra("state", -1);

            //StringBuilder stateName = null;

            //Headset type, human readable string
            String name = intent.getStringExtra("name");

            // - 1 if headset has a microphone, 0 otherwise, 1 mean h2w
            int microPhone = intent.getIntExtra("microphone", -1);

            switch (state) {
                case 0:
                    speakerManagement.setEnabled(true);

                    if (isSpeakerOn) {
                        mAudioMgr.setSpeakerphoneOn(true);
                    } else {
                        mAudioMgr.setSpeakerphoneOn(false);
                    }
                    mAudioMgr.setMode(AudioManager.MODE_IN_COMMUNICATION);

                    //stateName = new StringBuilder("Headset is unplugged");
                    //Log.d(TAG, stateName.toString());
                    break;
                case 1:

                    speakerManagement.setEnabled(false);
                    if (mAudioMgr.isSpeakerphoneOn()) {
                        mAudioMgr.setSpeakerphoneOn(false);
                        mAudioMgr.setMode(AudioManager.MODE_IN_COMMUNICATION);
                    }
                    //s
                    //stateName = new StringBuilder("Headset is plugged");
                    //Log.d(TAG, stateName.toString());
                    break;
                default:
                    Log.d(TAG, "I have no idea what the headset state is");
            }
        }


    }


    @Override
    public void onResume() {
        super.onResume();
        // register for normal head set plugged in/out

        if (audioInOutReceiver == null) {
            audioInOutReceiver = new AudioInOutChangeReceiver(getActivity());
        }

        if (receiver == null) {
            receiver = new CallBrodCastReceiver();
        }

        getActivity().registerReceiver(audioInOutReceiver, new IntentFilter(Intent.ACTION_HEADSET_PLUG));

        getActivity().registerReceiver(receiver, new IntentFilter("RESULT_RECEIVER_HOLDING"));

    }

    @Override
    public void onPause() {
        super.onPause();
        getActivity().unregisterReceiver(audioInOutReceiver);
        getActivity().unregisterReceiver(receiver);
    }

    //================

    private class CallBrodCastReceiver extends BroadcastReceiver {

        @Override
        public void onReceive(Context context, final Intent intent) {
            if (intent.getStringExtra("MODE").equals("voice") || intent.getStringExtra("MODE").equals("video")) {
                //=========dialog asking for type of call
                dLog = new Dialog(getActivity(), R.style.MaterialDialogSheet);
                dLog.setContentView(R.layout.dilaog_another_calling);
                dLog.setCanceledOnTouchOutside(true);
                dLog.getWindow().setLayout(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT);
                dLog.getWindow().setGravity(Gravity.BOTTOM);
                dLog.show();
                //======view clicking management


                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        try {
                            dLog.dismiss();
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                }, 15000);

                if (cDB.getUserNameFromImage(intent.getStringExtra("PHONE")).equalsIgnoreCase("")) {
                    ((AvenirRegular) dLog.findViewById(R.id.hold_caller_name)).setText(intent.getStringExtra("PHONE"));
                } else {
                    ((AvenirRegular) dLog.findViewById(R.id.hold_caller_name)).setText(cDB.getUserNameFromImage(intent.getStringExtra("PHONE")));
                }

                dLog.findViewById(R.id.holder_call_rcv).setOnClickListener(new OnClickListener() {

                    @Override
                    public void onClick(View v) {
                        dLog.findViewById(R.id.holding_pbar).setVisibility(View.VISIBLE);
                        dLog.findViewById(R.id.holder_action_bucket).setVisibility(View.GONE);


                        ((CallActivity) getActivity()).disconnect();




                        Intent i = new Intent(getActivity(), ConnectActivity.class);
                        i.setAction(ConnectActivity.ACTION_NOTIFICATION);
                        i.putExtra("ROOM_NAME", intent.getStringExtra("ROOM_NAME"));

                        if (cDB == null) {
                            cDB = new ConnexDB(getActivity());
                        }
                        //getUserNameFromPhone

                        if (cDB.getUserNameFromPhone(intent.getStringExtra("PHONE")).equalsIgnoreCase("") || cDB.getUserNameFromPhone(intent.getStringExtra("PHONE")).equalsIgnoreCase("null")) {
                            i.putExtra("CALLER_NAME", intent.getStringExtra("PHONE"));
                            Log.i("DHAMNA_ID", "" + intent.getStringExtra("PHONE"));
                        } else {

                            i.putExtra("CALLER_NAME", cDB.getUserNameFromPhone(intent.getStringExtra("PHONE")));
                            Log.i("DHAMNA_ID", "" + cDB.getUserNameFromPhone(intent.getStringExtra("PHONE")));

                        }

                        i.putExtra("MODE", intent.getStringExtra("MODE"));
                        i.putExtra("PHONE", intent.getStringExtra("PHONE"));


                        i.putExtra("OPONENT_ID", intent.getStringExtra("OPONENT_ID"));


                        startActivity(i);

                    }
                });

                dLog.findViewById(R.id.holder_call_cut).setOnClickListener(new OnClickListener() {

                    @Override
                    public void onClick(View v) {
                        dLog.dismiss();
                    }
                });
            } else {
                dLog.dismiss();
            }
        }
    }


    //    public void pushHelperDhoperUrlAcceptence(final Intent intent) {
//        String URL;
//        if (intent.getStringExtra("MODE").equalsIgnoreCase("voice")) {
//            URL = ConnexConstante.DOMAIN_URL + "push_send_v2?roomId=" + intent.getStringExtra("ROOM_NAME") + "&sender_id=" + ConnexApplication.getInstance().getUserID() + "&receiver_id=" + intent.getStringExtra("OPONENT_ID") + "&mod=voiceaccept&pem_mod=prod";
//        } else {
//            URL = ConnexConstante.DOMAIN_URL + "push_send_v2?roomId=" + intent.getStringExtra("ROOM_NAME") + "&sender_id=" + ConnexApplication.getInstance().getUserID() + "&receiver_id=" + intent.getStringExtra("OPONENT_ID") + "&mod=videoaccept&pem_mod=prod";
//        }
//        Log.i(TAG, URL);
//        JsonObjectRequest jsonObjReq = new JsonObjectRequest(Request.Method.GET, URL,
//                new Response.Listener<JSONObject>() {
//                    @Override
//                    public void onResponse(JSONObject response) {
//                        Log.i(TAG, response.toString());
//                        dLog.findViewById(R.id.holding_pbar).setVisibility(View.GONE);
//                        dLog.findViewById(R.id.holder_action_bucket).setVisibility(View.VISIBLE);
//
//                        try {
//                            if (response.getBoolean("response")) {
//
//                                //OPONENT_ID
//                                dLog.dismiss();
//
//
//                                //                                ((CallActivity) getActivity()).disconnect();
//
//                                Intent i = getActivity().getIntent();
//
//                                //((CallActivity) getActivity()).disconnect();
//
//                                if (cDB.getUserNameFromImage(intent.getStringExtra("OPONENT_PHONE")).equalsIgnoreCase("")) {
//                                    i.putExtra("CALLER_NAME", intent.getStringExtra("OPONENT_PHONE"));
//                                } else {
//                                    i.putExtra("CALLER_NAME", cDB.getUserNameFromImage(intent.getStringExtra("OPONENT_PHONE")));
//                                }
//                                //i.putExtra("CALLER_NAME", intent.getStringExtra("CALLER_NAME"));
//                                i.putExtra("MODE", intent.getStringExtra("MODE"));
//                                intent.putExtra(CallActivity.EXTRA_ROOMID, intent.getStringExtra("ROOM_NAME"));
//                                if (intent.getStringExtra("MODE").equalsIgnoreCase("voice")) {
//                                    i.putExtra(CallActivity.EXTRA_VIDEO_CALL, false);
//                                } else {
//                                    i.putExtra(CallActivity.EXTRA_VIDEO_CALL, true);
//                                }
//                                i.putExtra("PROFILE_IMAGE", "http://connex.tech/api/assets/images/connec-bg4.png");
//
//                                getActivity().startActivity(i);
//
//                            } else {
//                                Toast.makeText(getActivity(), "Failed to create Meeting, Try Again!", Toast.LENGTH_SHORT).show();
//                            }
//                        } catch (Exception e) {
//                            e.printStackTrace();
//                            Toast.makeText(getActivity(), "Failed to create Meeting, Try Again!", Toast.LENGTH_SHORT).show();
//                        }
//                    }
//                }, new Response.ErrorListener() {
//
//            @Override
//            public void onErrorResponse(VolleyError error) {
//                dLog.findViewById(R.id.holding_pbar).setVisibility(View.GONE);
//                dLog.findViewById(R.id.holder_action_bucket).setVisibility(View.VISIBLE);
//                Log.i(TAG, "Error: " + error.getMessage());
//                Toast.makeText(getActivity(), "Failed to create Meeting, Try Again!", Toast.LENGTH_SHORT).show();
//            }
//        });
//        ConnexApplication.getInstance().addToRequestQueue(jsonObjReq);
//    }


//    public void connexCalling(final String receiverID, final String mode) {
//
//        Intent intent = new Intent(getActivity(), ConnectActivity.class);
//        intent.setAction(ConnectActivity.ACTION_JOIN_HOLDER);
//        intent.putExtra("RECEIVER_ID", receiverID);
//        //intent.putExtra("MODE", "voice");
//        intent.putExtra("MODE", mode);
//        startActivity(intent);
//
//    }


}
