/*
 *  Copyright 2014 The WebRTC Project Authors. All rights reserved.
 *
 *  Use of this source code is governed by a BSD-style license
 *  that can be found in the LICENSE file in the root of the source
 *  tree. An additional intellectual property rights grant can be found
 *  in the file PATENTS.  All contributing project authors may
 *  be found in the AUTHORS file in the root of the source tree.
 */

//-------Code edited by Saikat Pakira @ Esolz on 8th June,2016

package esolz.connexstudent.apprtc;

import android.Manifest;
import android.app.AlertDialog;
import android.app.NotificationManager;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.media.AudioManager;
import android.media.MediaPlayer;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.PowerManager;
import android.preference.PreferenceManager;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.NotificationCompat;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.webkit.URLUtil;
import android.widget.ImageView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.squareup.picasso.Picasso;

import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

import esolz.connexstudent.ConnexService.RingToneService;
import esolz.connexstudent.R;
import esolz.connexstudent.appliaction.ConnexApplication;
import esolz.connexstudent.appliaction.ConnexConstante;
import esolz.connexstudent.customview.AvenirLight;
import esolz.connexstudent.customview.AvenirRoman;
import esolz.connexstudent.database.ConnexDB;


/**
 * Handles the initial setup where the user selects which room to join.
 */


public class ConnectActivity extends AppCompatActivity implements SensorEventListener {


    final int MY_PERMISSIONS_REQUEST_READ_CONTACTS = 1;
    private CallAcceptenceReceiver receiver = null;

    public String OPONENT_ID = "";
    private final String IOS_TAG = "prod";

    public static final String ACTION_APP = "ACTION_APP";
    public static final String ACTION_NOTIFICATION = "ACTION_NOTIFICATION";
    public static final String ACTION_JOIN_HOLDER = "ACTION_JOIN_HOLDER";

    public static final String ACTION_NOTIFICATION_END_RINGING = "ACTION_NOTIFICATION_END_RINGING";

    private static final String TAG = "ConnectActivity";
    private static boolean commandLineRun = false;
    private ConnexDB cDB = null;

    private SharedPreferences sharedPref;
    private String keyprefVideoCallEnabled;
    private String keyprefResolution;
    private String keyprefFps;
    private String keyprefCaptureQualitySlider;
    private String keyprefVideoBitrateType;
    private String keyprefVideoBitrateValue;
    private String keyprefVideoCodec;
    private String keyprefAudioBitrateType;
    private String keyprefAudioBitrateValue;
    private String keyprefAudioCodec;
    private String keyprefHwCodecAcceleration;
    private String keyprefCaptureToTexture;
    private String keyprefNoAudioProcessingPipeline;
    private String keyprefAecDump;
    private String keyprefOpenSLES;
    private String keyprefDisplayHud;
    private String keyprefTracing;
    private String keyprefRoomServerUrl;
    private String keyprefRoom;
    private String keyprefRoomList;

    private MediaPlayer mPlayer = null;
    private Handler threadmanager = null;
    private Runnable callRejectionThred = null;
    private String PROFILE_IMAGE = "";

    //================Added Code of Saikat on 8th June

    private String ROOM_NAME = "";
    private String RECEIVER_ID = "";


    //==============Power Locker


    private PowerManager powerManager = null;
    private PowerManager.WakeLock wakeLock = null;


    //================proximity sencer management


    private SensorManager mSensorManager;
    private Sensor mSensor;
    private AudioManager mAudioMgr = null;


    //=============user busy management

    private SharedPreferences sprefState = null;


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);


        //=============Power locker

        sprefState = getSharedPreferences("ConnectionActivity", MODE_PRIVATE);

        SharedPreferences.Editor edit = sprefState.edit();
        edit.putString("status", "busy");
        edit.commit();

        threadmanager = new Handler();


        callRejectionThred = new Runnable() {
            @Override
            public void run() {

                //======Missed Call and no answer

                SimpleDateFormat frmatr = new SimpleDateFormat("dd MMM,yyyy-HH:mm");

                cDB.insertInLogTable(getIntent().getStringExtra("PHONE"),
                        cDB.getUserNameFromPhone(getIntent().getStringExtra("PHONE")),
                        cDB.getUserImage(getIntent().getStringExtra("PHONE")),
                        "missed",
                        frmatr.format(new Date(Calendar.getInstance().getTimeInMillis())));

                fireMissedCall(cDB.getUserNameFromPhone(getIntent().getStringExtra("PHONE")));
                //==================================


                Intent i = new Intent(getApplicationContext(), RingToneService.class);
                i.setAction(RingToneService.ACTION_KILL);
                startService(i);
                finish();
            }
        };


        powerManager = (PowerManager) getSystemService(POWER_SERVICE);
        wakeLock = powerManager.newWakeLock(PowerManager.PARTIAL_WAKE_LOCK,
                "RingToneServiceLockerActivity");
        wakeLock.acquire();


        mSensorManager = (SensorManager) getSystemService(SENSOR_SERVICE);
        mSensor = mSensorManager.getDefaultSensor(Sensor.TYPE_PROXIMITY);


        cDB = new ConnexDB(getApplicationContext());
        // Get setting keys.
        PreferenceManager.setDefaultValues(this, R.xml.preferences, false);
        sharedPref = PreferenceManager.getDefaultSharedPreferences(this);
        keyprefVideoCallEnabled = getString(R.string.pref_videocall_key);
        keyprefResolution = getString(R.string.pref_resolution_key);
        keyprefFps = getString(R.string.pref_fps_key);
        keyprefCaptureQualitySlider = getString(R.string.pref_capturequalityslider_key);
        keyprefVideoBitrateType = getString(R.string.pref_startvideobitrate_key);
        keyprefVideoBitrateValue = getString(R.string.pref_startvideobitratevalue_key);
        keyprefVideoCodec = getString(R.string.pref_videocodec_key);
        keyprefHwCodecAcceleration = getString(R.string.pref_hwcodec_key);
        keyprefCaptureToTexture = getString(R.string.pref_capturetotexture_key);
        keyprefAudioBitrateType = getString(R.string.pref_startaudiobitrate_key);
        keyprefAudioBitrateValue = getString(R.string.pref_startaudiobitratevalue_key);
        keyprefAudioCodec = getString(R.string.pref_audiocodec_key);
        keyprefNoAudioProcessingPipeline = getString(R.string.pref_noaudioprocessing_key);
        keyprefAecDump = getString(R.string.pref_aecdump_key);
        keyprefOpenSLES = getString(R.string.pref_opensles_key);
        keyprefDisplayHud = getString(R.string.pref_displayhud_key);
        keyprefTracing = getString(R.string.pref_tracing_key);
        keyprefRoomServerUrl = getString(R.string.pref_room_server_url_key);
        keyprefRoom = getString(R.string.pref_room_key);
        keyprefRoomList = getString(R.string.pref_room_list_key);

        setContentView(R.layout.activity_connect);

        getWindow().setFlags(WindowManager.LayoutParams.FLAG_TURN_SCREEN_ON, WindowManager.LayoutParams.FLAG_TURN_SCREEN_ON);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_SHOW_WHEN_LOCKED, WindowManager.LayoutParams.FLAG_SHOW_WHEN_LOCKED);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON, WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);

        //=============permission management
        findViewById(R.id.notifoicatio).setVisibility(View.GONE);


        permissionChk();


        findViewById(R.id.rcv_call).setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {


                //======Missed Call and no answer


                threadmanager.removeCallbacks(callRejectionThred);

                SimpleDateFormat frmatr = new SimpleDateFormat("dd MMM,yyyy-HH:mm");

                cDB.insertInLogTable(getIntent().getStringExtra("PHONE"),
                        cDB.getUserNameFromPhone(getIntent().getStringExtra("PHONE")),
                        cDB.getUserImage(getIntent().getStringExtra("PHONE")),
                        "received",
                        frmatr.format(new Date(Calendar.getInstance().getTimeInMillis())));

                //==================================


                Intent i = new Intent(getApplicationContext(), RingToneService.class);
                i.setAction(RingToneService.ACTION_KILL_WITH_NOTHING);
                startService(i);


                ROOM_NAME = getIntent().getStringExtra("ROOM_NAME");

                pushHelperDhoperUrlAcceptence();

            }
        });


        findViewById(R.id.end_call).setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {

                if (getIntent().getAction().equals(ACTION_APP)) {
                    try {
                        if (mPlayer.isPlaying()) {
                            mPlayer.stop();
                            mPlayer.release();
                        }
                        finish();
                    } catch (Exception e) {
                        e.printStackTrace();
                    }

                    dialoerRejection(ConnexConstante.DOMAIN_URL + "push_send_v2?roomId=000000&sender_id=" + ConnexApplication.getInstance().getUserID() + "&receiver_id=" + RECEIVER_ID + "&mod=callEnded&pem_mod=prod");

                } else {


                    //======Missed Call and Rejected by You


                    //======Missed Call and no answer


                    threadmanager.removeCallbacks(callRejectionThred);

                    SimpleDateFormat frmatr = new SimpleDateFormat("dd MMM,yyyy-HH:mm");

                    cDB.insertInLogTable(getIntent().getStringExtra("PHONE"),
                            cDB.getUserNameFromPhone(getIntent().getStringExtra("PHONE")),
                            cDB.getUserImage(getIntent().getStringExtra("PHONE")),
                            "missed",
                            frmatr.format(new Date(Calendar.getInstance().getTimeInMillis())));

                    fireMissedCall(cDB.getUserNameFromPhone(getIntent().getStringExtra("PHONE")));
                    //==================================

                    //==================================

                    Intent i = new Intent(getApplicationContext(), RingToneService.class);
                    i.setAction(RingToneService.ACTION_KILL);
                    startService(i);
                }

                finish();
            }
        });

        if (mPlayer == null && getIntent().getAction().equals(ACTION_APP)) {


            AudioManager audioManager = (AudioManager) getSystemService(Context.AUDIO_SERVICE);
            audioManager.setMode(AudioManager.MODE_IN_COMMUNICATION);
            audioManager.setSpeakerphoneOn(false);


            mPlayer = MediaPlayer.create(ConnectActivity.this, R.raw.dialing);
            mPlayer.setAudioStreamType(AudioManager.MODE_IN_COMMUNICATION);


        }


        //==============sencer listener


        mSensorManager.registerListener(this, mSensor,
                SensorManager.SENSOR_DELAY_NORMAL);


    }


    public void permissionChk() {
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED || ActivityCompat.checkSelfPermission(this, Manifest.permission.READ_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED || ActivityCompat.checkSelfPermission(this, Manifest.permission.READ_PHONE_STATE) != PackageManager.PERMISSION_GRANTED || ActivityCompat.checkSelfPermission(this, Manifest.permission.READ_CONTACTS) != PackageManager.PERMISSION_GRANTED || ActivityCompat.checkSelfPermission(this, Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED) {
            // TODO: Consider calling
            if (ActivityCompat.shouldShowRequestPermissionRationale(this,
                    Manifest.permission.READ_EXTERNAL_STORAGE) && ActivityCompat.shouldShowRequestPermissionRationale(this,
                    Manifest.permission.WRITE_EXTERNAL_STORAGE) && ActivityCompat.shouldShowRequestPermissionRationale(this,
                    Manifest.permission.MODIFY_AUDIO_SETTINGS) && ActivityCompat.shouldShowRequestPermissionRationale(this,
                    Manifest.permission.RECORD_AUDIO) && ActivityCompat.shouldShowRequestPermissionRationale(this,
                    Manifest.permission.CAMERA)) {
                ActivityCompat.requestPermissions(this,
                        new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE,
                                Manifest.permission.READ_EXTERNAL_STORAGE,
                                Manifest.permission.CAMERA
                                , Manifest.permission.MODIFY_AUDIO_SETTINGS,
                                Manifest.permission.RECORD_AUDIO},
                        MY_PERMISSIONS_REQUEST_READ_CONTACTS);
            } else {
                // No explanation needed, we can request the permission.
                ActivityCompat.requestPermissions(this,
                        new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE,
                                Manifest.permission.READ_EXTERNAL_STORAGE,
                                Manifest.permission.MODIFY_AUDIO_SETTINGS,
                                Manifest.permission.CAMERA,
                                Manifest.permission.RECORD_AUDIO},
                        MY_PERMISSIONS_REQUEST_READ_CONTACTS);
            }
            return;
        } else {
            if (getIntent().getAction().equals(ACTION_APP)) {
                RECEIVER_ID = getIntent().getStringExtra("RECEIVER_ID");
                pushHelperDhoperUrl();

            } else if (getIntent().getAction().equals(ACTION_JOIN_HOLDER)) {

                OPONENT_ID = getIntent().getStringExtra("OPONENT_ID");
                ROOM_NAME = getIntent().getStringExtra("ROOM_NAME");
                pushHelperDhoperUrlAcceptence();

            } else {
//                Intent i = new Intent(getApplicationContext(), RingToneService.class);
//                i.setAction(RingToneService.ACTION_START);
//                startService(i);

                OPONENT_ID = getIntent().getStringExtra("OPONENT_ID");

                threadmanager.postDelayed(callRejectionThred, 15000);


                findViewById(R.id.notifoicatio).setVisibility(View.VISIBLE);
                PROFILE_IMAGE = cDB.getUserImage(getIntent().getStringExtra("PHONE"));

                Picasso.with(getApplicationContext()).load(PROFILE_IMAGE).fit().centerCrop().into((ImageView) findViewById(R.id.dialer_bg));

                ((AvenirRoman) findViewById(R.id.caller_name)).setText(getIntent().getStringExtra("CALLER_NAME") + "\nincoming...");

                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                    Window window = getWindow();
                    window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
                    window.setStatusBarColor(Color.BLACK);
                }
            }
        }
    }


    @Override
    public void onRequestPermissionsResult(int requestCode, String permissions[], int[] grantResults) {
        switch (requestCode) {
            case MY_PERMISSIONS_REQUEST_READ_CONTACTS: {
                if (grantResults.length > 1
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED &&
                        grantResults[1] == PackageManager.PERMISSION_GRANTED &&
                        grantResults[2] == PackageManager.PERMISSION_GRANTED &&
                        grantResults[3] == PackageManager.PERMISSION_GRANTED &&
                        grantResults[4] == PackageManager.PERMISSION_GRANTED) {

                    if (getIntent().getAction().equals(ACTION_APP)) {
                        RECEIVER_ID = getIntent().getStringExtra("RECEIVER_ID");
                        pushHelperDhoperUrl();
                    } else if (getIntent().getAction().equals(ACTION_JOIN_HOLDER)) {
                        OPONENT_ID = getIntent().getStringExtra("OPONENT_ID");
                        ROOM_NAME = getIntent().getStringExtra("ROOM_NAME");
                        pushHelperDhoperUrlAcceptence();

                    } else {

//                        Intent i = new Intent(getApplicationContext(), RingToneService.class);
//                        i.setAction(RingToneService.ACTION_START);
//                        startService(i);

//                        ROOM_NAME = getIntent().getStringExtra("ROOM_NAME");
//                        connectToRoom(0);

                        OPONENT_ID = getIntent().getStringExtra("OPONENT_ID");


                        threadmanager.postDelayed(callRejectionThred, 20000);


                        findViewById(R.id.notifoicatio).setVisibility(View.VISIBLE);
                        PROFILE_IMAGE = cDB.getUserImage(getIntent().getStringExtra("PHONE"));
                        Picasso.with(getApplicationContext()).load(PROFILE_IMAGE).fit().centerCrop().into((ImageView) findViewById(R.id.dialer_bg));


                        ((AvenirRoman) findViewById(R.id.caller_name)).setText(getIntent().getStringExtra("CALLER_NAME") + "\n\n" + "incoming...");
                        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                            Window window = getWindow();
                            window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
                            window.setStatusBarColor(Color.BLACK);
                        }

                    }
                } else {
                    android.support.v7.app.AlertDialog.Builder alertDialogBuilder = new android.support.v7.app.AlertDialog.Builder(ConnectActivity.this);
                    alertDialogBuilder.setMessage("With out these permissions, we cant move into meeting room. Would you like to continue?");
                    alertDialogBuilder.setPositiveButton("Retry", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface arg0, int arg1) {
                            permissionChk();
                        }
                    });
                    alertDialogBuilder.setNegativeButton("DISCARD", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            finish();
                        }
                    });
                    android.support.v7.app.AlertDialog alertDialog = alertDialogBuilder.create();
                    alertDialog.show();



                    //==================================

                    Intent i = new Intent(getApplicationContext(), RingToneService.class);
                    i.setAction(RingToneService.ACTION_KILL);
                    startService(i);

                }
                return;
            }
        }
    }


//    @Override
//    public boolean onCreateOptionsMenu(Menu menu) {
//        getMenuInflater().inflate(R.menu.connect_menu, menu);
//        return true;
//    }
//
//    @Override
//    public boolean onOptionsItemSelected(MenuItem item) {
//        // Handle presses on the action bar items.
//        if (item.getItemId() == R.id.action_settings) {
//            Intent intent = new Intent(this, SettingsActivity.class);
//            startActivity(intent);
//            return true;
//        } else {
//            return super.onOptionsItemSelected(item);
//        }
//    }

//    @Override
//    public void onPause() {
//        super.onPause();
//        SharedPreferences.Editor editor = sharedPref.edit();
//        editor.putString(keyprefRoom, ROOM_NAME);
//        editor.commit();
//    }
//
//    @Override
//    public void onResume() {
//        super.onResume();
//        ROOM_NAME = sharedPref.getString(keyprefRoom, "");
//    }

//    @Override
//    protected void onActivityResult(
//            int requestCode, int resultCode, Intent data) {
//        if (requestCode == CONNECTION_REQUEST && commandLineRun) {
//            Log.d(TAG, "Return: " + resultCode);
//            setResult(resultCode);
//            commandLineRun = false;
//            finish();
//        }
//    }


    //============main calling method

    private void connectToRoom(int runTimeMs) {
        // Get room name (random for loopback).

        String roomId = ROOM_NAME;

        String roomUrl = sharedPref.getString(keyprefRoomServerUrl, getString(R.string.pref_room_server_url_default));

        // Video call enabled flag.
        boolean videoCallEnabled = sharedPref.getBoolean(keyprefVideoCallEnabled,
                Boolean.valueOf(getString(R.string.pref_videocall_default)));

        // Get default codecs.
        String videoCodec = sharedPref.getString(keyprefVideoCodec,
                getString(R.string.pref_videocodec_default));
        String audioCodec = sharedPref.getString(keyprefAudioCodec,
                getString(R.string.pref_audiocodec_default));

        // Check HW codec flag.
        boolean hwCodec = sharedPref.getBoolean(keyprefHwCodecAcceleration,
                Boolean.valueOf(getString(R.string.pref_hwcodec_default)));

        // Check Capture to texture.
        boolean captureToTexture = sharedPref.getBoolean(keyprefCaptureToTexture,
                Boolean.valueOf(getString(R.string.pref_capturetotexture_default)));

        // Check Disable Audio Processing flag.
        boolean noAudioProcessing = sharedPref.getBoolean(
                keyprefNoAudioProcessingPipeline,
                Boolean.valueOf(getString(R.string.pref_noaudioprocessing_default)));

        // Check Disable Audio Processing flag.
        boolean aecDump = sharedPref.getBoolean(
                keyprefAecDump,
                Boolean.valueOf(getString(R.string.pref_aecdump_default)));

        // Check OpenSL ES enabled flag.
        boolean useOpenSLES = sharedPref.getBoolean(
                keyprefOpenSLES,
                Boolean.valueOf(getString(R.string.pref_opensles_default)));

        // Get video resolution from settings.
        int videoWidth = 0;
        int videoHeight = 0;
        String resolution = sharedPref.getString(keyprefResolution,
                getString(R.string.pref_resolution_default));
        String[] dimensions = resolution.split("[ x]+");
        if (dimensions.length == 2) {
            try {
                videoWidth = Integer.parseInt(dimensions[0]);
                videoHeight = Integer.parseInt(dimensions[1]);
            } catch (NumberFormatException e) {
                videoWidth = 0;
                videoHeight = 0;
                Log.e(TAG, "Wrong video resolution setting: " + resolution);
            }
        }

        // Get camera fps from settings.
        int cameraFps = 0;
        String fps = sharedPref.getString(keyprefFps,
                getString(R.string.pref_fps_default));
        String[] fpsValues = fps.split("[ x]+");
        if (fpsValues.length == 2) {
            try {
                cameraFps = Integer.parseInt(fpsValues[0]);
            } catch (NumberFormatException e) {
                Log.e(TAG, "Wrong camera fps setting: " + fps);
            }
        }

        // Check capture quality slider flag.
        boolean captureQualitySlider = sharedPref.getBoolean(keyprefCaptureQualitySlider,
                Boolean.valueOf(getString(R.string.pref_capturequalityslider_default)));

        // Get video and audio start bitrate.
        int videoStartBitrate = 0;
        String bitrateTypeDefault = getString(
                R.string.pref_startvideobitrate_default);
        String bitrateType = sharedPref.getString(
                keyprefVideoBitrateType, bitrateTypeDefault);
        if (!bitrateType.equals(bitrateTypeDefault)) {
            String bitrateValue = sharedPref.getString(keyprefVideoBitrateValue,
                    getString(R.string.pref_startvideobitratevalue_default));
            videoStartBitrate = Integer.parseInt(bitrateValue);
        }
        int audioStartBitrate = 0;
        bitrateTypeDefault = getString(R.string.pref_startaudiobitrate_default);
        bitrateType = sharedPref.getString(
                keyprefAudioBitrateType, bitrateTypeDefault);
        if (!bitrateType.equals(bitrateTypeDefault)) {
            String bitrateValue = sharedPref.getString(keyprefAudioBitrateValue,
                    getString(R.string.pref_startaudiobitratevalue_default));
            audioStartBitrate = Integer.parseInt(bitrateValue);
        }

        // Check statistics display option.
        boolean displayHud = sharedPref.getBoolean(keyprefDisplayHud,
                Boolean.valueOf(getString(R.string.pref_displayhud_default)));

        boolean tracing = sharedPref.getBoolean(
                keyprefTracing, Boolean.valueOf(getString(R.string.pref_tracing_default)));

        // Start AppRTCDemo activity.
        Log.v(TAG, "Connecting to room " + ROOM_NAME + " at CAlltype " + getIntent().getStringExtra("MODE"));

        if (validateUrl(roomUrl)) {
            Uri uri = Uri.parse(roomUrl);
            Intent intent = new Intent(this, CallActivity.class);
            intent.setData(uri);

            //-----RECEIVER_ID


            if (getIntent().getAction().equals(ACTION_APP)) {

                if (cDB.getUserNameFromID(RECEIVER_ID).equalsIgnoreCase("")) {
                    intent.putExtra("CALLER_NAME", getIntent().getStringExtra("RECEIVER_NAME"));
                } else {
                    intent.putExtra("CALLER_NAME", cDB.getUserNameFromID(RECEIVER_ID));
                }
            } else {
                intent.putExtra("CALLER_NAME", getIntent().getStringExtra("CALLER_NAME"));
            }


            intent.putExtra(CallActivity.EXTRA_ROOMID, roomId);
            intent.putExtra(CallActivity.EXTRA_LOOPBACK, false);


            if (getIntent().getStringExtra("MODE").equalsIgnoreCase("voice")) {
                intent.putExtra(CallActivity.EXTRA_VIDEO_CALL, false);
            } else {
                intent.putExtra(CallActivity.EXTRA_VIDEO_CALL, true);
            }

            intent.putExtra(CallActivity.EXTRA_VIDEO_WIDTH, videoWidth);
            intent.putExtra(CallActivity.EXTRA_VIDEO_HEIGHT, videoHeight);

            Log.i("Config", "Fps : " + cameraFps);

            intent.putExtra(CallActivity.EXTRA_VIDEO_FPS, 30);


            intent.putExtra(CallActivity.EXTRA_VIDEO_CAPTUREQUALITYSLIDER_ENABLED, true);

            intent.putExtra(CallActivity.EXTRA_VIDEO_BITRATE, 50000);
            Log.i("Config", "videoStartBitrate : " + videoStartBitrate);

            intent.putExtra(CallActivity.EXTRA_VIDEOCODEC, "H264");
            Log.i("Config", "videoCodec : " + videoCodec);

            intent.putExtra(CallActivity.EXTRA_HWCODEC_ENABLED, hwCodec);
            intent.putExtra(CallActivity.EXTRA_CAPTURETOTEXTURE_ENABLED, captureToTexture);
            intent.putExtra(CallActivity.EXTRA_NOAUDIOPROCESSING_ENABLED,
                    noAudioProcessing);
            intent.putExtra(CallActivity.EXTRA_AECDUMP_ENABLED, aecDump);
            intent.putExtra(CallActivity.EXTRA_OPENSLES_ENABLED, useOpenSLES);
            intent.putExtra(CallActivity.EXTRA_AUDIO_BITRATE, audioStartBitrate);
            intent.putExtra(CallActivity.EXTRA_AUDIOCODEC, audioCodec);
            intent.putExtra(CallActivity.EXTRA_DISPLAY_HUD, displayHud);
            intent.putExtra(CallActivity.EXTRA_TRACING, tracing);
            intent.putExtra(CallActivity.EXTRA_CMDLINE, commandLineRun);
            intent.putExtra(CallActivity.EXTRA_RUNTIME, runTimeMs);
            intent.putExtra("PROFILE_IMAGE", PROFILE_IMAGE);

            startActivity(intent);
            finish();
        }
    }

    private boolean validateUrl(String url) {
        if (URLUtil.isHttpsUrl(url) || URLUtil.isHttpUrl(url)) {
            return true;
        }

        new AlertDialog.Builder(this)
                .setTitle(getText(R.string.invalid_url_title))
                .setMessage(getString(R.string.invalid_url_text, url))
                .setCancelable(false)
                .setNeutralButton(R.string.ok, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        dialog.cancel();
                    }
                }).create().show();
        return false;
    }


    public void pushHelperDhoperUrl() {
        ROOM_NAME = "" + Calendar.getInstance().getTimeInMillis();
        String URL;
        if (getIntent().getStringExtra("MODE").equalsIgnoreCase("voice")) {
            URL = ConnexConstante.DOMAIN_URL + "push_send_v2?roomId=" + ROOM_NAME + "&sender_id=" + ConnexApplication.getInstance().getUserID() + "&receiver_id=" + RECEIVER_ID + "&mod=voice&pem_mod=prod";
        } else {
            URL = ConnexConstante.DOMAIN_URL + "push_send_v2?roomId=" + ROOM_NAME + "&sender_id=" + ConnexApplication.getInstance().getUserID() + "&receiver_id=" + RECEIVER_ID + "&mod=video&pem_mod=prod";
        }
        Log.i(TAG, URL);
        JsonObjectRequest jsonObjReq = new JsonObjectRequest(Request.Method.GET, URL,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        Log.i(TAG, response.toString());
                        try {
                            if (response.getBoolean("response")) {
                                Log.i(TAG, ROOM_NAME);
//                                connectToRoom(0);
                                //==========opening dialong screen


                                findViewById(R.id.notifoicatio).setVisibility(View.VISIBLE);

                                PROFILE_IMAGE = cDB.getUserImageFromID(RECEIVER_ID);

                                Picasso.with(getApplicationContext()).load(PROFILE_IMAGE).fit().centerCrop().into((ImageView) findViewById(R.id.dialer_bg));


                                if (cDB.getUserNameFromID(RECEIVER_ID).trim().length() > 0) {
                                    ((AvenirRoman) findViewById(R.id.caller_name)).setText("Calling \n\n" + cDB.getUserNameFromID(RECEIVER_ID) + "");
                                } else {
                                    ((AvenirRoman) findViewById(R.id.caller_name)).setText("Calling \n\n" + getIntent().getStringExtra("RECEIVER_NAME") + "");
                                }


                                findViewById(R.id.rcv_call).setVisibility(View.GONE);
                                findViewById(R.id.message_withreject).setVisibility(View.GONE);
                                ((AvenirLight) findViewById(R.id.rejection_call_text)).setText("End Call");

                                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                                    Window window = getWindow();
                                    window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
                                    window.setStatusBarColor(Color.BLACK);
                                }


                                //======Dialed Call by You

                                SimpleDateFormat frmatr = new SimpleDateFormat("dd MMM,yyyy-HH:mm");

                                cDB.insertInLogTable(cDB.getUserPhoneFromID(RECEIVER_ID),
                                        cDB.getUserNameFromID(RECEIVER_ID),
                                        PROFILE_IMAGE,
                                        "called",
                                        frmatr.format(new Date(Calendar.getInstance().getTimeInMillis())));

                                //==================================


                                mPlayer.start();


                                new Handler().postDelayed(new Runnable() {
                                    @Override
                                    public void run() {
                                        try {
                                            if (mPlayer.isPlaying()) {
                                                mPlayer.stop();
                                                mPlayer.release();
                                            }
                                            finish();
                                        } catch (Exception e) {
                                            e.printStackTrace();
                                        }

                                    }
                                }, 19000);

                            } else {
                                Toast.makeText(ConnectActivity.this, "Failed to create Meeting, Try Again!", Toast.LENGTH_SHORT).show();
                                onBackPressed();
                            }
                        } catch (Exception e) {
                            e.printStackTrace();
                            Toast.makeText(ConnectActivity.this, "Failed to create Meeting, Try Again!", Toast.LENGTH_SHORT).show();
                            onBackPressed();
                        }
                    }
                }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                Log.i(TAG, "Error: " + error.getMessage());
                Toast.makeText(ConnectActivity.this, "Failed to create Meeting, Try Again!", Toast.LENGTH_SHORT).show();
                onBackPressed();
            }
        });
        ConnexApplication.getInstance().addToRequestQueue(jsonObjReq);
    }


    @Override
    public void onSensorChanged(SensorEvent event) {


//        if (mAudioMgr == null) {
//            mAudioMgr = (AudioManager) getSystemService(Context.AUDIO_SERVICE);
//        }
//
//
//        if (event.values[0] == 0) {
//            //-----near
//            if (mPlayer != null) {
//                Log.i("AudioManager", "I am near to you...");
//
//                try {
//                    float volume = (float) (1 - (Math.log(100 - 10) / Math.log(100)));
//                    mPlayer.setVolume(volume, volume);
//                } catch (Exception e) {
//                    e.printStackTrace();
//                }
//
//
//                //mPlayer.setAudioStreamType(AudioManager.MODE_IN_CALL);
//
//                //mAudioMgr.setMode(AudioManager.MODE_IN_CALL);
//
//            }
//
//        } else {
//            //-----far
//            if (mPlayer != null) {
//                Log.i("AudioManager", "I am far from you...");
//
//
//                try {
//                    float volume = (float) (1 - (Math.log(100 - 99) / Math.log(100)));
//                    mPlayer.setVolume(volume, volume);
//                } catch (Exception e) {
//                    e.printStackTrace();
//                }
//
//
//                //mPlayer.setAudioStreamType(AudioManager.MODE_RINGTONE);
//                //mAudioMgr.setMode(AudioManager.MODE_NORMAL);
//            }
//        }
    }

    @Override
    public void onAccuracyChanged(Sensor sensor, int accuracy) {

    }


    private class CallAcceptenceReceiver extends BroadcastReceiver {

        @Override
        public void onReceive(Context context, Intent intent) {
            Log.d("MyGcmListenerService", " onReceive ");


            try {
                if (mPlayer.isPlaying()) {
                    mPlayer.stop();
                    mPlayer.release();
                }
                finish();
            } catch (Exception e) {
                e.printStackTrace();
            }

            if (intent.getStringExtra("MODE").equals("reject")) {

                //Toast.makeText(ConnectActivity.this, "reject", Toast.LENGTH_SHORT).show();
                //======Missed Call and no answer

                threadmanager.removeCallbacks(callRejectionThred);


                SimpleDateFormat frmatr = new SimpleDateFormat("dd MMM,yyyy-HH:mm");

                cDB.insertInLogTable(getIntent().getStringExtra("PHONE"),
                        cDB.getUserNameFromPhone(getIntent().getStringExtra("PHONE")),
                        cDB.getUserImage(getIntent().getStringExtra("PHONE")),
                        "missed",
                        frmatr.format(new Date(Calendar.getInstance().getTimeInMillis())));


                fireMissedCall(cDB.getUserNameFromPhone(getIntent().getStringExtra("PHONE")));


                //==================================


                Intent i = new Intent(getApplicationContext(), RingToneService.class);
                i.setAction(RingToneService.ACTION_KILL);
                startService(i);
                finish();
            } else {
                // Toast.makeText(ConnectActivity.this, "non reject", Toast.LENGTH_SHORT).show();
                connectToRoom(0);
            }


        }
    }


    @Override
    protected void onStart() {
        if (receiver == null) {
            receiver = new CallAcceptenceReceiver();
        }
        Log.d("MyGcmListenerService", " registerReceiver ");

        registerReceiver(receiver, new IntentFilter("RESULT_RECEIVER_CONNECT"));

        super.onStart();

    }


    @Override
    protected void onStop() {
        try {

            Log.d("MyGcmListenerService", " UnregisterReceiver ");

            unregisterReceiver(receiver);
        } catch (Exception e) {
            e.printStackTrace();
        }
        super.onStop();
    }


    public void pushHelperDhoperUrlAcceptence() {
        String URL;
        if (getIntent().getStringExtra("MODE").equalsIgnoreCase("voice")) {
            URL = ConnexConstante.DOMAIN_URL + "push_send_v2?roomId=" + ROOM_NAME + "&sender_id=" + ConnexApplication.getInstance().getUserID() + "&receiver_id=" + OPONENT_ID + "&mod=voiceaccept&pem_mod=prod";
        } else {
            URL = ConnexConstante.DOMAIN_URL + "push_send_v2?roomId=" + ROOM_NAME + "&sender_id=" + ConnexApplication.getInstance().getUserID() + "&receiver_id=" + OPONENT_ID + "&mod=videoaccept&pem_mod=prod";
        }
        Log.i(TAG, URL);
        JsonObjectRequest jsonObjReq = new JsonObjectRequest(Request.Method.GET, URL,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        Log.i(TAG, response.toString());
                        try {
                            if (response.getBoolean("response")) {
                                Log.i(TAG, ROOM_NAME);
                                connectToRoom(0);
                            } else {
                                Toast.makeText(ConnectActivity.this, "Failed to create Meeting, Try Again!", Toast.LENGTH_SHORT).show();
                                onBackPressed();
                            }
                        } catch (Exception e) {
                            e.printStackTrace();
                            Toast.makeText(ConnectActivity.this, "Failed to create Meeting, Try Again!", Toast.LENGTH_SHORT).show();
                            onBackPressed();
                        }
                    }
                }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                Log.i(TAG, "Error: " + error.getMessage());
                Toast.makeText(ConnectActivity.this, "Failed to create Meeting, Try Again!", Toast.LENGTH_SHORT).show();
                onBackPressed();
            }
        });
        ConnexApplication.getInstance().addToRequestQueue(jsonObjReq);
    }


    public void dialoerRejection(final String URL) {
        JsonObjectRequest jsonObjReq = new JsonObjectRequest(Request.Method.GET, URL,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        Log.i(TAG, response.toString());
                        Log.i(TAG, URL);

                        try {
                            if (response.getBoolean("response")) {

                            } else {
                            }
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                Log.i(TAG, "Error: " + error.getMessage());
            }
        });
        ConnexApplication.getInstance().addToRequestQueue(jsonObjReq);
    }


    @Override
    protected void onDestroy() {
        super.onDestroy();


        sprefState = getSharedPreferences("ConnectionActivity", MODE_PRIVATE);

        SharedPreferences.Editor edit = sprefState.edit();
        edit.putString("status", "free");
        edit.commit();


        if (wakeLock != null) {
            wakeLock.release();
        }
    }


    public void fireMissedCall(final String name) {
        if (!name.equals("")) {
            Uri defaultSoundUri = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
            NotificationCompat.Builder notificationBuilder = new NotificationCompat.Builder(this)
                    .setSmallIcon(R.drawable.ic_notification)
                    .setContentTitle("Connex")
                    .setContentText("You have a missed call from " + name)
                    .setAutoCancel(true)
                    .setSound(defaultSoundUri);
            NotificationManager notificationManager =
                    (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
            notificationManager.cancel(3);
            notificationManager.notify(3, notificationBuilder.build());
        }
    }
}
