package esolz.connexstudent.chatDilaog;

import android.app.NotificationManager;
import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import esolz.connexstudent.ChatManagement;
import esolz.connexstudent.R;
import esolz.connexstudent.adapters.ChatMemberListAdapter;
import esolz.connexstudent.database.ConnexDB;
import esolz.connexstudent.models.ContactDataType;

/**
 * Created by Saikat's Mac on 11/07/16.
 */

public class ChatMemberList extends Fragment {


    private RecyclerView memebrList = null;
    private ConnexDB cDB = null;
    private ChatMemberListAdapter mainAdap = null;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        if (cDB == null) {
            cDB = new ConnexDB(getActivity());
        }
        return inflater.inflate(R.layout.fragments_chatmember_room, container, false);
    }


    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);


        NotificationManager notificationManager =
                (NotificationManager) getActivity().getSystemService(Context.NOTIFICATION_SERVICE);
        notificationManager.cancel(5);

        memebrList = (RecyclerView) view.findViewById(R.id.memeber_list);
        memebrList.setHasFixedSize(true);
        memebrList.setLayoutManager(new LinearLayoutManager(getActivity()));
        mainAdap = new ChatMemberListAdapter(getActivity(), cDB.fetchAllConnexContacts());
        memebrList.setAdapter(mainAdap);

        mainAdap.setOnItemClickListener(new ChatMemberListAdapter.onItemClickListener() {
            @Override
            public void onDetailsClickd(final ContactDataType data) {

                getActivity().runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        try {
                            ((ChatManagement) getActivity()).openChatRoom(data.getDbID(), data.getProfImage(), data.getName());
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                });

            }
        });


        view.findViewById(R.id.back_first).setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                try {
                    getActivity().onBackPressed();
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });


    }


}
