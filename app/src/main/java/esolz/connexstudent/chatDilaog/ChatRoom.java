package esolz.connexstudent.chatDilaog;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.media.MediaPlayer;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.squareup.picasso.Picasso;

import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

import esolz.connexstudent.BaseActivity.ParentFragment;
import esolz.connexstudent.R;
import esolz.connexstudent.adapters.ChatRoomAdapter;
import esolz.connexstudent.appliaction.ConnexApplication;
import esolz.connexstudent.appliaction.ConnexConstante;
import esolz.connexstudent.criptography.SecurityManagement;
import esolz.connexstudent.customview.AvenirLight;
import esolz.connexstudent.customview.CirculerTransformation;
import esolz.connexstudent.database.ConnexDB;
import esolz.connexstudent.models.ChatDataModel;

/**
 * Created by Saikat's Mac on 11/07/16.
 */

public class ChatRoom extends ParentFragment {


    private RecyclerView chatViewMain = null;
    private ChatManagementReceiver receiver = null;
    private EditText chatBox = null;
    private ImageView sendButton = null;
    private String oponentID = "", oponentImg = "", oponentName = "";
    private ConnexDB cDB = null;
    private ChatRoomAdapter mainadap = null;
    private final String TAG = "ChatRoom";
    private MediaPlayer mPlayer = null;

    private final String key = "keliyebrindabondekhiyedobo";


    public static ChatRoom getInstance(final String oponentID, final String oponentImg, final String oponentName) {
        ChatRoom frag_ = new ChatRoom();
        frag_.oponentID = oponentID;
        frag_.oponentName = oponentName;
        frag_.oponentImg = oponentImg;
        return frag_;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        if (cDB == null) {
            cDB = new ConnexDB(getActivity());
        }

        if (mPlayer == null) {
            mPlayer = MediaPlayer.create(getActivity(), R.raw.coolnotification);
        }

        return inflater.inflate(R.layout.fragment_chat_room, container, false);
    }


    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        chatBox = (EditText) view.findViewById(R.id.chat_message);
        sendButton = (ImageView) view.findViewById(R.id.action_sent);

        chatViewMain = (RecyclerView) view.findViewById(R.id.char_room);
        chatViewMain.setHasFixedSize(false);
        LinearLayoutManager lManager = new LinearLayoutManager(getActivity());
        lManager.setOrientation(LinearLayoutManager.VERTICAL);
        lManager.setReverseLayout(true);
        lManager.setStackFromEnd(true);
        chatViewMain.setLayoutManager(lManager);
        chatViewMain.setItemAnimator(new DefaultItemAnimator());

        if (mainadap == null) {
            mainadap = new ChatRoomAdapter(getActivity(), cDB.fetchAllChatofaUser(oponentID));
        }
        chatViewMain.setAdapter(mainadap);


        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                chatViewMain.smoothScrollToPosition(0);
            }
        }, 100);


        sendButton.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {

//                ChatDataModel model_ = new ChatDataModel();
//                SimpleDateFormat sFormater = new SimpleDateFormat("dd MMMM, yyyy hh:mm a");
//                model_.setDateTime(sFormater.format(new Date(Calendar.getInstance().getTimeInMillis())));
//                model_.setSenderID(ConnexApplication.getInstance().getUserID());
//                model_.setReceiverID(oponentID);
//                model_.setMessage(chatBox.getText().toString().trim());
//                model_.setIsLoadingFailed(false);
//                cDB.insertChat(model_);

                if (chatBox.getText().toString().trim().length() > 0) {
                    pushHelperDhoperUrlAcceptence(chatBox.getText().toString().trim());
                    chatBox.setText("");
                }
            }
        });


        view.findViewById(R.id.back_second).setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                try {
                    getActivity().onBackPressed();
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });


        Picasso.with(getActivity()).load(oponentImg).transform(new CirculerTransformation()).fit().centerCrop().into((ImageView) view.findViewById(R.id.profile_img));

        ((AvenirLight) view.findViewById(R.id.opoenentname)).setText(oponentName);

    }

    @Override
    public void onStart() {
        if (receiver == null) {
            receiver = new ChatManagementReceiver();
        }
        getActivity().registerReceiver(receiver, new IntentFilter("CHAT_ROOM_ENTRY"));

        super.onStart();
    }


    @Override
    public void onStop() {
        getActivity().unregisterReceiver(receiver);
        super.onStop();
    }


    //=============Chat Receiver

    private class ChatManagementReceiver extends BroadcastReceiver {

        @Override
        public void onReceive(Context context, Intent intent) {
            Log.d("MyGcmListenerService", " onReceive ");

            if (intent != null) {
                if (intent.getStringExtra("SENDER_ID").equalsIgnoreCase(oponentID)) {
                    //-------show to list with a sound.
                    if (mainadap == null) {
                        mainadap = new ChatRoomAdapter(getActivity(), cDB.fetchAllChatofaUser(oponentID));
                        chatViewMain.setAdapter(mainadap);
                    } else {
                        mainadap.chageDataSet(cDB.fetchAllChatofaUser(oponentID));
                    }

                    try {
                        if (mPlayer.isPlaying()) {
                            mPlayer.stop();
                        }
                        mPlayer.start();
                    } catch (Exception e) {
                        e.printStackTrace();
                    }

                }
            }

        }
    }


    //================sending chat

    public void pushHelperDhoperUrlAcceptence(final String message) {
        String URL = "";
        try {
            URL = ConnexConstante.DOMAIN_URL + "chat_message?sender_id=" + ConnexApplication.getInstance().getUserID() + "&receiver_id=" + oponentID + "&chat_text=" + SecurityManagement.cipher(key, message);
            // URLEncoder.encode(message, "UTF-8");
        } catch (Exception e) {
            e.printStackTrace();
        }
        Log.i(TAG, URL);
        JsonObjectRequest jsonObjReq = new JsonObjectRequest(Request.Method.GET, URL,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        Log.i(TAG, response.toString());
                        try {
                            ChatDataModel model_ = new ChatDataModel();

                            if (response.getBoolean("response")) {

                                SimpleDateFormat sFormater = new SimpleDateFormat("dd MMMM, yyyy hh:mm a");
                                model_.setDateTime(sFormater.format(new Date(Calendar.getInstance().getTimeInMillis())));
                                model_.setSenderID(ConnexApplication.getInstance().getUserID());
                                model_.setReceiverID(oponentID);
                                model_.setMessage(response.getJSONObject("chat").getString("text"));
                                model_.setIsLoadingFailed(true);
                                cDB.insertChat(model_);

                            } else {

                                SimpleDateFormat sFormater = new SimpleDateFormat("dd MMMM, yyyy hh:mm a");
                                model_.setDateTime(sFormater.format(new Date(Calendar.getInstance().getTimeInMillis())));
                                model_.setSenderID(ConnexApplication.getInstance().getUserID());
                                model_.setReceiverID(oponentID);
                                model_.setMessage(response.getJSONObject("chat").getString("text"));
                                model_.setIsLoadingFailed(false);
                                cDB.insertChat(model_);

                                Toast.makeText(getActivity(), "Failed to send message", Toast.LENGTH_SHORT).show();
                            }

                            if (mainadap == null) {
                                mainadap = new ChatRoomAdapter(getActivity(), cDB.fetchAllChatofaUser(oponentID));
                                chatViewMain.setAdapter(mainadap);
                            } else {
                                mainadap.addData(model_);
                            }

                            chatViewMain.smoothScrollToPosition(0);

                        } catch (Exception e) {
                            e.printStackTrace();
                            Toast.makeText(getActivity(), "Failed to send message", Toast.LENGTH_SHORT).show();
                        }
                    }
                }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                Log.i(TAG, "Error: " + error.getMessage());
                Toast.makeText(getActivity(), "Failed to send message", Toast.LENGTH_SHORT).show();
            }
        });
        ConnexApplication.getInstance().addToRequestQueue(jsonObjReq);
    }


    @Override
    public void onDestroy() {
        super.onDestroy();

        try {
            if (mPlayer.isPlaying()) {
                mPlayer.stop();
            }
            mPlayer.release();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
