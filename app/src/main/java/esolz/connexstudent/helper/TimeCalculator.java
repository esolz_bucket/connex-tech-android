package esolz.connexstudent.helper;

import android.util.Log;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

/**
 * Created by Saikat's Mac on 07/06/16.
 */

public class TimeCalculator {


    public synchronized static boolean isInBetween(final String startTime, final String endTime) {

        Calendar cal = Calendar.getInstance();
        cal.set(1990, 9, 29);
        Date currentDate = new Date(cal.getTimeInMillis());
        SimpleDateFormat sDate = new SimpleDateFormat("dd-MM-yyyy hh:mm a");

        try {
            Date fetchedTimeEnd = new Date(sDate.parse("29-10-1990 " + endTime).getTime());
            Date fetchedTimeStart = new Date(sDate.parse("29-10-1990 " + startTime).getTime());

            Log.i("onBindViewHolder", "End : " + fetchedTimeEnd.toString());
            Log.i("onBindViewHolder", "fStart : " + fetchedTimeStart.toString());
            Log.i("onBindViewHolder", "currentDate : " + currentDate.toString());


            if (currentDate.before(fetchedTimeEnd) && currentDate.after(fetchedTimeStart)) {
                Log.i("onBindViewHolder", " Current time in between given slot");
                return true;
            }
//            else if (currentDate.before(fetchedTimeStart)) {
//                Log.i("onBindViewHolder", " Current time in before starting");
//            }
            else {

                Log.i("onBindViewHolder", " Else...End before " + currentDate.before(fetchedTimeEnd));
                Log.i("onBindViewHolder", " Else...End after " + currentDate.after(fetchedTimeEnd));

                Log.i("onBindViewHolder", " Else...Start before " + currentDate.before(fetchedTimeStart));
                Log.i("onBindViewHolder", " Else...Start after " + currentDate.after(fetchedTimeStart));
                return false;

            }

        } catch (ParseException e) {
            e.printStackTrace();
            return false;
        }
    }
}
