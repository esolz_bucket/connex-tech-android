package esolz.connexstudent;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.provider.ContactsContract;
import android.provider.Settings;
import android.provider.Telephony;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.text.Html;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.NetworkResponse;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.StringRequest;
import com.squareup.picasso.Picasso;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.LinkedList;
import java.util.Map;

import esolz.connexstudent.adapters.COntactDetailsMultipleAdapter;
import esolz.connexstudent.appliaction.ConnexApplication;
import esolz.connexstudent.appliaction.ConnexConstante;
import esolz.connexstudent.apprtc.ConnectActivity;
import esolz.connexstudent.database.ConnexDB;
import esolz.connexstudent.models.ContactDataType;

public class ContactDetailsActivity extends AppCompatActivity {


    //==============User permission checking

    final int MY_PERMISSIONS_REQUEST_READ_CONTACTS = 1;
    Intent intentCalling = null;

    //private ContactDataType dlogData = null;
    private ImageView profileImage = null, isFavourite = null;

    //    private AvenirRoman profileName, profileNumber;
//    private AvenirRegular invitationText = null;

    private final String TAG = "ContactDetailsActivity";
    private ConnexDB cDB = null;


    //================

    private int CONTACT_ID = 0;
    private LinkedList<ContactDataType> detailsData = null;
    private RecyclerView detailedView = null;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_contact_details);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        //getSupportActionBar().setHomeAsUpIndicator(R.drawable.ic_action_arrow_left);


        CONTACT_ID = getIntent().getIntExtra("CONTACT_ID", -1);

        getSupportActionBar().setTitle(getIntent().getStringExtra("PROFILE_NAME"));


        //=======main block
        //dlogData = ConnexApplication.getInstance().getSelectedContacts();


        cDB = new ConnexDB(getApplicationContext());

        profileImage = (ImageView) findViewById(R.id.profile_image);

        //https://s32.postimg.org/i0eb1uk79/banner_1024_x_500.png

        if (getIntent().getStringExtra("PROFILE_IMAGE") != null) {
            if (getIntent().getStringExtra("PROFILE_IMAGE").isEmpty()) {
                Picasso.with(getApplicationContext()).load("https://s32.postimg.org/i0eb1uk79/banner_1024_x_500.png").fit().centerCrop().into(profileImage);
            } else {
                Picasso.with(getApplicationContext()).load(getIntent().getStringExtra("PROFILE_IMAGE")).fit().centerCrop().into(profileImage);
            }
        } else {
            Picasso.with(getApplicationContext()).load("https://s32.postimg.org/i0eb1uk79/banner_1024_x_500.png").fit().centerCrop().into(profileImage);
        }


        isFavourite = (ImageView) findViewById(R.id.is_favourite);

        detailedView = (RecyclerView) findViewById(R.id.detaildListing);
        detailedView.setHasFixedSize(false);
        detailedView.setLayoutManager(new LinearLayoutManager(getApplicationContext()));


        if (cDB.isFavourite("" + CONTACT_ID).equalsIgnoreCase("no")) {
            isFavourite.setBackgroundResource(R.drawable.ic_action_not_favorite);
        } else {
            isFavourite.setBackgroundResource(R.drawable.ic_action_favorite);
        }


//            findViewById(R.id.connex_vdcall).setOnClickListener(new View.OnClickListener() {
//
//                @Override
//                public void onClick(View v) {
//                    Log.i(TAG, "Clicked...");
//
//                    Log.i(TAG, "receiverID : " + receiverID);
//                    Intent intent = new Intent(ContactDetailsActivity.this, ConnectActivity.class);
//                    intent.setAction(ConnectActivity.ACTION_APP);
//                    intent.putExtra("RECEIVER_ID", receiverID);
//                    intent.putExtra("MODE", "video");
//                    startActivity(intent);
//                }
//            });
//
//
//            findViewById(R.id.connexblock_call).setOnClickListener(new View.OnClickListener() {
//
//                @Override
//                public void onClick(View v) {
//                    Log.i(TAG, "Clicked...");
//                    Log.i(TAG, "receiverID : " + receiverID);
//                    Intent intent = new Intent(ContactDetailsActivity.this, ConnectActivity.class);
//                    intent.setAction(ConnectActivity.ACTION_APP);
//                    intent.putExtra("RECEIVER_ID", receiverID);
//                    intent.putExtra("MODE", "voice");
//                    startActivity(intent);
//                }
//            });


        //======favourite management


        //favouriteManagement

        isFavourite.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {

                //cDB.fetchAllContacts();

                if (cDB.isFavourite("" + CONTACT_ID).equalsIgnoreCase("yes")) {
                    cDB.favouriteManagement("" + CONTACT_ID, "no");
                    isFavourite.setBackgroundResource(R.drawable.ic_action_not_favorite);
                } else {
                    cDB.favouriteManagement("" + CONTACT_ID, "yes");
                    isFavourite.setBackgroundResource(R.drawable.ic_action_favorite);
                }

            }
        });


        //===========phone message


//        findViewById(R.id.phn_message).setOnClickListener(new View.OnClickListener() {
//
//            @Override
//            public void onClick(View v) {
//                openMessagePanel();
//            }
//        });


        Log.i(TAG, "DetailLoader...");

        (new DetailLoader()).executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == android.R.id.home) {
            onBackPressed();
        }
        return super.onOptionsItemSelected(item);
    }


    public void openMessagePanel() {
        String defaultSmsPackageName = Telephony.Sms.getDefaultSmsPackage(this); // Need to change the build to API 19

        Intent sendIntent = new Intent(Intent.ACTION_SEND);
        sendIntent.setType("text/plain");
        sendIntent.putExtra(Intent.EXTRA_TEXT, "");
        if (defaultSmsPackageName != null) {
            sendIntent.setPackage(defaultSmsPackageName);
        }
        startActivity(sendIntent);
    }


    public void getUserInfo(final String jsonData) {
        final String URL = ConnexConstante.DOMAIN_URL + "connex_user_sync";
        Log.i(TAG, URL);
        StringRequest sr = new StringRequest(Request.Method.POST, URL,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String result) {
                        Log.i(TAG, result.toString());

                        try {
                            JSONObject response2 = new JSONObject(result);
                            if (response2.getBoolean("response")) {
                                JSONArray maininnerArray = response2.getJSONArray("all_connex");
                                if (maininnerArray.length() > 0) {
                                    for (int i = 0; i < maininnerArray.length(); i++) {
                                        for (int j = 0; j < detailsData.size(); j++) {
                                            if (detailsData.get(j).getPhone().equalsIgnoreCase(maininnerArray.getJSONObject(i).getString("phone"))) {
                                                detailsData.get(j).setDbID(maininnerArray.getJSONObject(i).getString("id"));
                                            }
                                        }
                                        cDB.changingConnexUserStatus(maininnerArray.getJSONObject(i).getString("phone"), "yes", maininnerArray.getJSONObject(i).getString("id"));
                                    }
                                }
                                detailedView.setAdapter(new COntactDetailsMultipleAdapter(ContactDetailsActivity.this, detailsData, false));
                            } else {
                                Toast.makeText(getApplicationContext(), "Failed to sync with server...", Toast.LENGTH_SHORT).show();
                            }
                        } catch (Exception e) {
                            e.printStackTrace();
                            Toast.makeText(getApplicationContext(), "Failed to sync with server...", Toast.LENGTH_SHORT).show();
                        }
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                VolleyLog.d("Output : ", "Error: " + error.getMessage());
                String json = null;
                NetworkResponse response = error.networkResponse;
                if (response != null && response.data != null) {
                    switch (response.statusCode) {
                        case 400:
                            json = new String(response.data);
                            if (json != null) displayMessage(json);
                            break;
                    }
                }
            }
        }) {
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<String, String>();
                Log.i(TAG, "Post Param : " + jsonData);
                params.put("details_info", jsonData);
                return params;
            }

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put("Content-Type", "application/x-www-form-urlencoded");
                return params;
            }
        };
        ConnexApplication.getInstance().addToRequestQueue(sr);
    }


    public void displayMessage(String toastString) {
        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(ContactDetailsActivity.this);
        alertDialogBuilder.setTitle("Error");
        alertDialogBuilder
                .setMessage(Html.fromHtml(toastString))
                .setCancelable(false)
                .setPositiveButton("Retry", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        dialog.cancel();
                    }
                });
        AlertDialog alertDialog = alertDialogBuilder.create();
        alertDialog.show();
    }


    public String createJsonData(final LinkedList<ContactDataType> dlogData) {

        JSONArray mainArray = new JSONArray();
        for (int i = 0; i < dlogData.size(); i++) {
            try {
                if (dlogData.get(i).getDbID().trim().equals("") || dlogData.get(i).getDbID().trim().equals("-1")) {
                    JSONObject mainObject = new JSONObject();
                    mainObject.put("name", dlogData.get(i).getName().trim());
                    mainObject.put("phone", dlogData.get(i).getPhone().trim());
                    mainObject.put("email", dlogData.get(i).getEmail().trim());
                    mainObject.put("profileimg", "");
                    mainArray.put(mainObject);
                }
            } catch (Exception e) {
                e.printStackTrace();


            }
        }

        try {
            if (mainArray.length() > 0) {
                Log.i(TAG, mainArray.toString());
                //return URLEncoder.encode(mainArray.toString(), "UTF-8");
                return mainArray.toString();
            } else {
                return "";
            }
        } catch (Exception e) {
            e.printStackTrace();
            return "";
        }
    }


    private String fetchEmailID(final String phoneNO) {

        Log.i(TAG, "Yes i am here..." + phoneNO);


        String email = "";

        Cursor emailCur = getContentResolver().query(
                ContactsContract.CommonDataKinds.Email.CONTENT_URI,
                null,
                ContactsContract.CommonDataKinds.Email.CONTACT_ID + " = ?",
                new String[]{phoneNO}, null);


        for (int i = 0; emailCur.getCount() > 0 && i < emailCur.getCount(); i++) {
            emailCur.moveToPosition(i);
            email = emailCur.getString(
                    emailCur.getColumnIndex(ContactsContract.CommonDataKinds.Email.DATA));
//            String emailType = emailCur.getString(
//                    emailCur.getColumnIndex(ContactsContract.CommonDataKinds.Email.TYPE));


            Log.i(TAG, "email : " + email);
        }

        emailCur.close();

        return email;
    }


    //=======================


    public class DetailLoader extends AsyncTask<Void, Void, Void> {

        @Override
        protected Void doInBackground(Void... params) {

            if (detailsData == null) {
                detailsData = new LinkedList<ContactDataType>();
            } else {
                detailsData.clear();
            }


            Uri dataUri = ContactsContract.CommonDataKinds.Phone.CONTENT_URI;

            Cursor dataCursor = getContentResolver().query(
                    dataUri,
                    new String[]{ContactsContract.CommonDataKinds.Phone.NUMBER,
                            ContactsContract.CommonDataKinds.Phone.DISPLAY_NAME,
                            ContactsContract.CommonDataKinds.Phone.PHOTO_URI},
                    ContactsContract.Data.CONTACT_ID + "=" + CONTACT_ID,
                    null, ContactsContract.CommonDataKinds.Phone.NUMBER);


            int contactNameIndex = dataCursor.getColumnIndex(ContactsContract.CommonDataKinds.Phone.DISPLAY_NAME);
            int contactPhoneIndex = dataCursor.getColumnIndex(ContactsContract.CommonDataKinds.Phone.NUMBER);
            int contactPhotoIndex = dataCursor.getColumnIndex(ContactsContract.CommonDataKinds.Phone.PHOTO_URI);


            for (int i = 0; i < dataCursor.getCount(); i++) {
                dataCursor.moveToPosition(i);
                Log.i(TAG, "DetailLoader...loop.");


                String phoneNumber = "";

                ContactDataType temp_ = new ContactDataType();

                temp_.setName(dataCursor.getString(contactNameIndex));
                temp_.setProfImage(dataCursor.getString(contactPhotoIndex));

                phoneNumber = dataCursor.getString(contactPhoneIndex);
                temp_.setOriginalPhoneNumber(phoneNumber);
                phoneNumber = phoneNumber.replaceAll("\\s", "");// **********removing
                phoneNumber = phoneNumber.replaceAll(" ", ""); // all
                phoneNumber = phoneNumber.replaceAll("[(.^)]*", "");
                phoneNumber = phoneNumber.replaceAll("-", "");
//                  phoneNumber = phoneNumber.replaceAll("\\+", "");
                phoneNumber = phoneNumber.replaceAll("\\,", "");
                phoneNumber = phoneNumber.replaceAll("  ", "");
                phoneNumber = phoneNumber.trim();

                if (phoneNumber.length() >= 10) {
                    phoneNumber = phoneNumber.substring((phoneNumber.length() - 10), phoneNumber.length());
                    temp_.setPhone(phoneNumber);
                    ConnexApplication.getInstance().getMainContactsData().add(temp_);
                    ConnexApplication.getInstance().getMapIndex().put(temp_.getName().substring(0, 1).toUpperCase(), ConnexApplication.getInstance().getMainContactsData().indexOf(temp_));
                }

                temp_.setDbID(cDB.getUserDBID(temp_.getPhone()));
                temp_.setEmail(fetchEmailID("" + CONTACT_ID));


                Log.i(TAG, "getPhone : " + temp_.getPhone());
                Log.i(TAG, "getDbID : " + temp_.getDbID());


                if (detailsData.size() > 0) {
                    if (!detailsData.get(detailsData.size() - 1).getPhone().equalsIgnoreCase(temp_.getPhone())) {
                        detailsData.add(temp_);
                    }
                } else {
                    detailsData.add(temp_);
                }

            }


            dataCursor.close();

            return null;
        }


        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);


            final String jsonResult = createJsonData(detailsData);

            Log.i(TAG, "jsonResult : " + jsonResult);
            Log.i(TAG, "size : " + detailsData.size());


            if (!jsonResult.equals("")) {
                //=========set adapter
                detailedView.setAdapter(new COntactDetailsMultipleAdapter(ContactDetailsActivity.this, detailsData, true));
                getUserInfo(jsonResult);
            } else {
                detailedView.setAdapter(new COntactDetailsMultipleAdapter(ContactDetailsActivity.this, detailsData, false));
            }

        }
    }


    public void connexCalling(final String receiverID, final String mode) {

        intentCalling = new Intent(ContactDetailsActivity.this, ConnectActivity.class);
        intentCalling.setAction(ConnectActivity.ACTION_APP);
        intentCalling.putExtra("RECEIVER_ID", receiverID);
        //intent.putExtra("MODE", "voice");
        intentCalling.putExtra("MODE", mode);

        permissionChk();


    }


    public void inviteOthers() {
        Intent s = new Intent(Intent.ACTION_SEND);
        s.setType("text/plain");
        s.putExtra(Intent.EXTRA_TEXT, "Hey! Download Connex for secure voice and video calling! www.connex.tech/dw/");
        s.putExtra(Intent.EXTRA_SUBJECT, "I am inviting you!");
        startActivity(Intent.createChooser(s, "Connex"));
    }


    //===============Permission management

    public void permissionChk() {
        if (ActivityCompat.checkSelfPermission(this, android.Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED || ActivityCompat.checkSelfPermission(this, android.Manifest.permission.READ_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED || ActivityCompat.checkSelfPermission(this, android.Manifest.permission.READ_PHONE_STATE) != PackageManager.PERMISSION_GRANTED || ActivityCompat.checkSelfPermission(this, android.Manifest.permission.READ_CONTACTS) != PackageManager.PERMISSION_GRANTED || ActivityCompat.checkSelfPermission(this, android.Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED) {
            // TODO: Consider calling
            if (ActivityCompat.shouldShowRequestPermissionRationale(this,
                    android.Manifest.permission.READ_EXTERNAL_STORAGE) && ActivityCompat.shouldShowRequestPermissionRationale(this,
                    android.Manifest.permission.WRITE_EXTERNAL_STORAGE) && ActivityCompat.shouldShowRequestPermissionRationale(this,
                    android.Manifest.permission.MODIFY_AUDIO_SETTINGS) && ActivityCompat.shouldShowRequestPermissionRationale(this,
                    android.Manifest.permission.RECORD_AUDIO) && ActivityCompat.shouldShowRequestPermissionRationale(this,
                    android.Manifest.permission.CAMERA)) {
                ActivityCompat.requestPermissions(this,
                        new String[]{android.Manifest.permission.WRITE_EXTERNAL_STORAGE,
                                android.Manifest.permission.READ_EXTERNAL_STORAGE,
                                android.Manifest.permission.CAMERA
                                , android.Manifest.permission.MODIFY_AUDIO_SETTINGS,
                                android.Manifest.permission.RECORD_AUDIO},
                        MY_PERMISSIONS_REQUEST_READ_CONTACTS);
            } else {
                // No explanation needed, we can request the permission.
                ActivityCompat.requestPermissions(this,
                        new String[]{android.Manifest.permission.WRITE_EXTERNAL_STORAGE,
                                android.Manifest.permission.READ_EXTERNAL_STORAGE,
                                android.Manifest.permission.MODIFY_AUDIO_SETTINGS,
                                android.Manifest.permission.CAMERA,
                                android.Manifest.permission.RECORD_AUDIO},
                        MY_PERMISSIONS_REQUEST_READ_CONTACTS);
            }
            return;
        } else {
            startActivity(intentCalling);
        }
    }


    @Override
    public void onRequestPermissionsResult(int requestCode, String permissions[], int[] grantResults) {
        switch (requestCode) {
            case MY_PERMISSIONS_REQUEST_READ_CONTACTS: {
                if (grantResults.length > 1
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED &&
                        grantResults[1] == PackageManager.PERMISSION_GRANTED &&
                        grantResults[2] == PackageManager.PERMISSION_GRANTED &&
                        grantResults[3] == PackageManager.PERMISSION_GRANTED &&
                        grantResults[4] == PackageManager.PERMISSION_GRANTED) {

                    startActivity(intentCalling);


                } else {
                    android.support.v7.app.AlertDialog.Builder alertDialogBuilder = new android.support.v7.app.AlertDialog.Builder(ContactDetailsActivity.this);
                    alertDialogBuilder.setMessage("You haven't provide all permissions.");
                    alertDialogBuilder.setPositiveButton("Permissions", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface arg0, int arg1) {

                            final Intent i = new Intent();
                            i.setAction(Settings.ACTION_APPLICATION_DETAILS_SETTINGS);
                            i.addCategory(Intent.CATEGORY_DEFAULT);
                            i.setData(Uri.parse("package:" + ContactDetailsActivity.this.getPackageName()));
                            i.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                            i.addFlags(Intent.FLAG_ACTIVITY_NO_HISTORY);
                            i.addFlags(Intent.FLAG_ACTIVITY_EXCLUDE_FROM_RECENTS);
                            startActivity(i);
                        }
                    });
                    alertDialogBuilder.setNegativeButton("DISCARD", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            finish();
                        }
                    });
                    android.support.v7.app.AlertDialog alertDialog = alertDialogBuilder.create();
                    alertDialog.show();

                }
                return;
            }
        }
    }


}
