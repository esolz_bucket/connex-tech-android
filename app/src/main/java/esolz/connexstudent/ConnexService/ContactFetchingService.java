package esolz.connexstudent.ConnexService;

import android.app.Service;
import android.content.Intent;
import android.database.Cursor;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.IBinder;
import android.provider.ContactsContract;
import android.support.annotation.Nullable;
import android.util.Log;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.net.URLEncoder;

import esolz.connexstudent.appliaction.ConnexConstante;
import esolz.connexstudent.database.ConnexDB;
import esolz.connexstudent.models.ContactDataType;
import okhttp3.MediaType;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;

/**
 * Created by Saikat's Mac on 24/06/16.
 */


public class ContactFetchingService extends Service {


    public static final String ACTION_ON = "connex.contacts.start";
    public static final String ACTION_KILL = "connex.contacts.kill";

    private boolean shouldRunAsynk = false;
    private final String TAG = "ContactFetchingService";
    private int mainCouter = 0;
    private ConnexDB cDB = null;
    public static final MediaType JSON = MediaType.parse("application/json; charset=utf-8");

    @Override
    public void onCreate() {
        super.onCreate();
    }


    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {

        if (intent != null) {

            if (intent.getAction().equals(ACTION_ON)) {
                if (cDB == null) {
                    cDB = new ConnexDB(getApplicationContext());
                }

                if (shouldRunAsynk == false) {
                    shouldRunAsynk = true;
                    (new FetchPhoneNumber()).executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
                }


            } else {
                //-----ACTION KILL
                killMePlz();
            }
        }

        return START_NOT_STICKY;
    }

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }


    private void killMePlz() {

        shouldRunAsynk = false;
    }


    public void sentMessageToIntent(final String data_) {
        Log.d(TAG, "testingbysaikat");
        Intent intent = new Intent();
        intent.setAction("RESULT_RECEIVER_SEVERSYNK");
        intent.putExtra("SynkedID", data_);
        sendBroadcast(intent);
    }


    private class FetchPhoneNumber extends AsyncTask<Void, String, Void> {


        @Override
        protected void onProgressUpdate(String... values) {
            super.onProgressUpdate(values);

            //sentMessageToIntent("running");
            sentMessageToIntent(values[0]);

        }

        @Override
        protected Void doInBackground(Void... params) {


            cDB.makingSynkNo();
            mainCouter = 0;
            Uri dataUri = ContactsContract.CommonDataKinds.Phone.CONTENT_URI;

            Cursor dataCursor = getContentResolver().query(
                    dataUri,
                    new String[]{ContactsContract.CommonDataKinds.Phone.NUMBER, ContactsContract.CommonDataKinds.Phone.DISPLAY_NAME, ContactsContract.CommonDataKinds.Phone.PHOTO_URI, ContactsContract.CommonDataKinds.Phone.CONTACT_ID},
                    null,
                    null, ContactsContract.CommonDataKinds.Phone.DISPLAY_NAME);

            if (dataCursor.getCount() > 0) {


                int contactNameIndex = dataCursor.getColumnIndex(ContactsContract.CommonDataKinds.Phone.DISPLAY_NAME);
                int contactPhoneIndex = dataCursor.getColumnIndex(ContactsContract.CommonDataKinds.Phone.NUMBER);
                int contactPhotoIndex = dataCursor.getColumnIndex(ContactsContract.CommonDataKinds.Phone.PHOTO_URI);
                int contactPhoneIDIndex = dataCursor.getColumnIndex(ContactsContract.CommonDataKinds.Phone.CONTACT_ID);


                for (int i = 0; i < dataCursor.getCount(); i++) {


                    dataCursor.moveToPosition(i);

                    String phoneNumber = "";

                    ContactDataType temp_ = new ContactDataType();

                    temp_.setName(dataCursor.getString(contactNameIndex));
                    temp_.setProfImage(dataCursor.getString(contactPhotoIndex));
                    temp_.setPhoneContactID(dataCursor.getString(contactPhoneIDIndex));

                    //Log.i(TAG, "Name : " + temp_.getName());

                    phoneNumber = dataCursor.getString(contactPhoneIndex);
                    temp_.setOriginalPhoneNumber(phoneNumber);
                    phoneNumber = phoneNumber.replaceAll("\\s", "");// **********removing
                    phoneNumber = phoneNumber.replaceAll(" ", ""); // all
                    phoneNumber = phoneNumber.replaceAll("[(.^)]*", "");
                    phoneNumber = phoneNumber.replaceAll("-", "");
                    phoneNumber = phoneNumber.replaceAll("\\,", "");
                    phoneNumber = phoneNumber.replaceAll("  ", "");
                    phoneNumber = phoneNumber.trim();

                    if (phoneNumber.length() >= 10) {
                        phoneNumber = phoneNumber.substring((phoneNumber.length() - 10), phoneNumber.length());
                        temp_.setPhone(phoneNumber);
                        Log.i(TAG, "Phone ID IN : " + temp_.getPhoneContactID());
                        mainCouter++;


                        //================synck with server


                        OkHttpClient client = new OkHttpClient();
                        Request request = new Request.Builder()
                                .url(ConnexConstante.DOMAIN_URL + "connex_user_sync?details_info=" + createJsonData(temp_))
                                .addHeader("cache-control", "no-cache")
                                .build();
                        try {
                            Response response = client.newCall(request).execute();
                            String tempResponse_ = response.body().string();
                            Log.i("tempResponse", tempResponse_);

                            //=====parsing the result

                            JSONObject mainObj = new JSONObject(tempResponse_);
                            if (mainObj.getString("response").equalsIgnoreCase("TRUE") && mainObj.getJSONArray("all_connex").length() > 0) {
                                mainObj = mainObj.getJSONArray("all_connex").getJSONObject(0);
                                temp_.setIsConnexUser("yes");
                                temp_.setDbID(mainObj.getString("id"));
                                publishProgress(dataCursor.getString(contactPhoneIDIndex));
                            }

                        } catch (Exception e) {
                            e.printStackTrace();
                        }

                        cDB.insertDatatoContacts(temp_);


                        //==================================

//                        ConnexApplication.getInstance().getMainContactsData().add(temp_);
//                        ConnexApplication.getInstance().getMapIndex().put(temp_.getName().substring(0, 1).toUpperCase(), ConnexApplication.getInstance().getMainContactsData().indexOf(temp_));
                    }
                    Log.i(TAG, "Phone ID Out : " + temp_.getPhoneContactID());

//                    if (mainCouter % 100 == 0 && i > 0) {
//                        Log.i(TAG, "Service running, index : " + i);
//                    }
//
//                    if (shouldRunAsynk == false) {
//                        Log.i("tempResponse", "Those gys killed me.....Haaa  Haaaaa!! :-(");
//                        break;
//                    }


                }
                dataCursor.close();
            }

            return null;
        }

        @Override
        protected void onPostExecute(Void result) {


            //if (shouldRunAsynk) {
            //=======delete synked=no data from db
            cDB.deleteNonSynkedData();
            //}

            shouldRunAsynk = false;

            //sentMessageToIntent("done");

        }

    }


    //============making JSON for server synking


    private String createJsonData(final ContactDataType dlogData) {
        JSONArray mainArray = new JSONArray();
        try {
            JSONObject mainObject = new JSONObject();
            mainObject.put("name", dlogData.getName().trim());
            mainObject.put("phone", dlogData.getPhone().trim());
            mainObject.put("email", dlogData.getEmail().trim());
            mainObject.put("profileimg", "");
            mainArray.put(mainObject);
        } catch (JSONException e) {
            e.printStackTrace();
        }

        try {
            if (mainArray.length() > 0) {
                Log.i(TAG, mainArray.toString());

                Log.i("tempResponse", mainArray.toString());
                return URLEncoder.encode(mainArray.toString(), "UTF-8");
                //return mainArray.toString();

            } else {
                return "";
            }
        } catch (Exception e) {
            e.printStackTrace();
            return "";
        }
    }


}
