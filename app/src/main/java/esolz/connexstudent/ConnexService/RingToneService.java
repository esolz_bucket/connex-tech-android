package esolz.connexstudent.ConnexService;

import android.app.NotificationManager;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.media.AudioManager;
import android.media.MediaPlayer;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Handler;
import android.os.IBinder;
import android.os.PowerManager;
import android.support.annotation.Nullable;
import android.util.Log;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;

import org.json.JSONObject;

import esolz.connexstudent.appliaction.ConnexApplication;
import esolz.connexstudent.appliaction.ConnexConstante;
import esolz.connexstudent.apprtc.ConnectActivity;
import esolz.connexstudent.database.ConnexDB;

/**
 * Created by Saikat's Mac on 25/06/16.
 */

public class RingToneService extends Service {


    public static final String ACTION_START = "connex.RingToneService.start";
    public static final String ACTION_KILL = "connex.RingToneService.kill";
    public static final String ACTION_KILL_WITH_NOTHING = "connex.RingToneService.ACTION_KILL_WITH_NOTHING";
    private final String TAG = "RingToneService";
    private String OPONENT_ID = "";
    //private Ringtone r = null;
    private boolean isRinging = false;
    private Handler callback = null;
    private Runnable ringingThread = null;
    private PowerManager powerManager = null;
    private PowerManager.WakeLock wakeLock = null;
    private MediaPlayer mpLayer = null;
    private ConnexDB cDB = null;
    private SharedPreferences sprefState = null;

    @Override
    public void onDestroy() {
        super.onDestroy();

        try {
            if (wakeLock != null) {
                wakeLock.release();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    @Override
    public void onCreate() {
        super.onCreate();
        callback = new Handler();
        ringingThread = new Runnable() {
            @Override
            public void run() {
                if (isRinging) {
                    isRinging = false;
                    mpLayer.stop();
                    mpLayer.release();
                    try {
                        wakeLock.release();
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                    NotificationManager notificationManager =
                            (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
                    notificationManager.cancel(2);

                    //==========fire notification of rejection of the call
                    //URL = ConnexConstante.DOMAIN_URL + "push_send_v2?roomId=000000&sender_id=" + ConnexApplication.getInstance().getUserID() + "&receiver_id=8&mod=reject";

                    pushHelperDhoperUrl(ConnexConstante.DOMAIN_URL + "push_send_v2?roomId=000000&sender_id=" + ConnexApplication.getInstance().getUserID() + "&receiver_id=" + OPONENT_ID + "&mod=reject&pem_mod=prod");
                }
            }
        };
    }


    public void pushHelperDhoperUrl(final String URL) {
        JsonObjectRequest jsonObjReq = new JsonObjectRequest(Request.Method.GET, URL,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        Log.i(TAG, response.toString());
                        Log.i(TAG, URL);

                        try {
                            if (response.getBoolean("response")) {

                            } else {
                            }
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                Log.i(TAG, "Error: " + error.getMessage());
            }
        });
        ConnexApplication.getInstance().addToRequestQueue(jsonObjReq);
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {

        if (intent != null) {


            if (intent.getAction().equals(ACTION_START)) {

                sprefState = getSharedPreferences("CallActivity", MODE_PRIVATE);

//                if (sprefState.getString("status", "").equalsIgnoreCase("busy")) {
//
//
//                    hittingBrodcastManagementHoldMangement(data.getString("roomId"), data.getString("from_name"), data.getString("mod"), data.getString("phone"));//phone
//
//
//                } else {
//                    sprefState = getSharedPreferences("ConnectionActivity", MODE_PRIVATE);
//                    if (sprefState.getString("status", "").equalsIgnoreCase("busy")) {
//
//
//                    }
//                }




                if (isRinging == false) {
                    isRinging = true;

                    OPONENT_ID = intent.getStringExtra("OPONENT_ID");

                    powerManager = (PowerManager) getSystemService(POWER_SERVICE);
                    wakeLock = powerManager.newWakeLock(PowerManager.PARTIAL_WAKE_LOCK,
                            "RingToneServiceLocker");
                    wakeLock.acquire();

                    if (mpLayer == null) {
                        Uri notification = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_RINGTONE);
                        AudioManager audioManager = (AudioManager) getSystemService(Context.AUDIO_SERVICE);
                        audioManager.setMode(AudioManager.STREAM_MUSIC);
                        audioManager.setSpeakerphoneOn(true);
                        mpLayer = MediaPlayer.create(getApplicationContext(), notification);
                        mpLayer.setAudioStreamType(AudioManager.STREAM_MUSIC);
                    }


                    mpLayer.start();

                    Intent i = new Intent(getApplicationContext(), ConnectActivity.class);
                    i.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                    i.setAction(ConnectActivity.ACTION_NOTIFICATION);
                    i.putExtra("ROOM_NAME", intent.getStringExtra("ROOM_NAME"));

                    if (cDB == null) {
                        cDB = new ConnexDB(getApplicationContext());
                    }
                    //getUserNameFromPhone

                    if (cDB.getUserNameFromPhone(intent.getStringExtra("PHONE")).equalsIgnoreCase("")) {
                        i.putExtra("CALLER_NAME", intent.getStringExtra("PHONE"));
                    } else {
                        i.putExtra("CALLER_NAME", cDB.getUserNameFromPhone(intent.getStringExtra("PHONE")));
                    }
                    i.putExtra("MODE", intent.getStringExtra("MODE"));
                    i.putExtra("PHONE", intent.getStringExtra("PHONE"));
                    i.putExtra("OPONENT_ID", intent.getStringExtra("OPONENT_ID"));
                    startActivity(i);
                    callback.postDelayed(ringingThread, 20000);
                }
            } else if (intent.getAction().equals(ACTION_KILL_WITH_NOTHING)) {
                if (isRinging) {
                    if (callback != null) {
                        callback.removeCallbacks(ringingThread);
                    }
                    isRinging = false;
                    mpLayer.stop();
                    try {
                        wakeLock.release();
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                    NotificationManager notificationManager =
                            (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
                    notificationManager.cancel(2);
                }
            } else {
                if (isRinging) {
                    if (callback != null) {
                        callback.removeCallbacks(ringingThread);
                    }
                    isRinging = false;
                    mpLayer.stop();
                    try {
                        wakeLock.release();
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                    pushHelperDhoperUrl(ConnexConstante.DOMAIN_URL + "push_send_v2?roomId=000000&sender_id=" + ConnexApplication.getInstance().getUserID() + "&receiver_id=" + OPONENT_ID + "&mod=reject&pem_mod=prod");
                    NotificationManager notificationManager =
                            (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
                    notificationManager.cancel(2);
                }
//                NotificationManager notificationManager =
//                        (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
//                notificationManager.cancelAll();
                stopSelf();
            }
        }
        return START_NOT_STICKY;
    }

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }


//    public void sentMessageToIntent(final String mode) {
//        Intent intent = new Intent();
//        intent.setAction("RESULT_RECEIVER_RINGING");
//        intent.putExtra("MODE", mode);
//        sendBroadcast(intent);
//    }


}
