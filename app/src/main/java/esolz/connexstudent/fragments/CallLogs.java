package esolz.connexstudent.fragments;

import android.app.AlertDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Html;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.NetworkResponse;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.StringRequest;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.LinkedList;
import java.util.Map;

import esolz.connexstudent.ContactDetailsActivity;
import esolz.connexstudent.R;
import esolz.connexstudent.adapters.CallLogsAdapter;
import esolz.connexstudent.appliaction.ConnexApplication;
import esolz.connexstudent.appliaction.ConnexConstante;
import esolz.connexstudent.apprtc.ConnectActivity;
import esolz.connexstudent.database.ConnexDB;
import esolz.connexstudent.models.CallLogsDType;

/**
 * Created by Saikat's Mac on 06/07/16.
 */


public class CallLogs extends Fragment {


    private ConnexDB cDB = null;
    private RecyclerView logView = null;
    private View errorMsg = null;
    private final String TAG = "CallLogs";

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        if (cDB == null) {
            cDB = new ConnexDB(getActivity());
        }
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragments_call_logs, container, false);
    }


    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        errorMsg = view.findViewById(R.id.error_msg);
        logView = (RecyclerView) view.findViewById(R.id.call_logs);
        logView.setHasFixedSize(true);
        logView.setLayoutManager(new LinearLayoutManager(getActivity()));


        //=========clear log management


        getActivity().findViewById(R.id.clear_log).setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {

                if (cDB.fetchCallLogs().size() > 0) {
                    AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(getActivity());
                    alertDialogBuilder
                            .setMessage("Do you want to clear call logs ?")
                            .setCancelable(false)
                            .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int id) {
                                    cDB.clearLogData();
                                    refreshMe();
                                }
                            })
                            .setNegativeButton("No", new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int id) {
                                }
                            });
                    AlertDialog alertDialog = alertDialogBuilder.create();
                    alertDialog.show();
                } else {
                    Toast.makeText(getActivity(), "Call logs already empty", Toast.LENGTH_SHORT).show();
                }
            }
        });


    }


    @Override
    public void onResume() {
        super.onResume();
        refreshMe();
    }


    private void refreshMe() {
        LinkedList<CallLogsDType> tempdata = cDB.fetchCallLogs();
        if (tempdata.size() > 0) {
            errorMsg.setVisibility(View.GONE);
        } else {
            errorMsg.setVisibility(View.VISIBLE);
        }

        logView.setAdapter(new CallLogsAdapter(getActivity(), tempdata, new CallLogsAdapter.onItemClickListener() {
            @Override
            public void onItemClicked(final String phoneNumber) {

                getActivity().runOnUiThread(new Runnable() {
                    @Override
                    public void run() {

                        String[] temp_ = cDB.getUsermockUpData(phoneNumber);

                        if (temp_[0].equalsIgnoreCase("")) {

                            //=========dialog asking for type of call
                            final Dialog dLog = new Dialog(getActivity(), R.style.MaterialDialogSheet);
                            dLog.setContentView(R.layout.dialog_call_type_asking);
                            dLog.setCanceledOnTouchOutside(true);
                            dLog.getWindow().setLayout(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT);
                            dLog.getWindow().setGravity(Gravity.BOTTOM);
                            dLog.show();
                            //======view clicking management

                            dLog.findViewById(R.id.unknown_voice_call).setOnClickListener(new View.OnClickListener() {

                                @Override
                                public void onClick(View v) {
                                    dLog.dismiss();
                                    String temp_number;
                                    if (phoneNumber.length() > 10) {
                                        temp_number = phoneNumber.substring(1, phoneNumber.length());
                                    } else {
                                        temp_number = phoneNumber;
                                    }
                                    getUserInfo(createJsonData(temp_number), "voice", temp_number);
                                }
                            });


                            dLog.findViewById(R.id.unknown_vid_call).setOnClickListener(new View.OnClickListener() {

                                @Override
                                public void onClick(View v) {

                                    dLog.dismiss();

                                    String temp_number;
                                    if (phoneNumber.length() > 10) {
                                        temp_number = phoneNumber.substring(1, phoneNumber.length());
                                    } else {
                                        temp_number = phoneNumber;
                                    }
                                    getUserInfo(createJsonData(temp_number), "video", temp_number);
                                }
                            });

                        } else {
                            Intent i = new Intent(getActivity(), ContactDetailsActivity.class);
                            i.putExtra("CONTACT_ID", Integer.parseInt(temp_[0]));
                            i.putExtra("PROFILE_IMAGE", temp_[2]);
                            i.putExtra("PROFILE_NAME", temp_[1]);
                            startActivity(i);

                        }
                    }
                });

            }
        }));
    }


    public void connexCalling(final String receiverID, final String mode, final String phoneNumber) {
        Intent intent = new Intent(getActivity(), ConnectActivity.class);
        intent.setAction(ConnectActivity.ACTION_APP);
        intent.putExtra("RECEIVER_ID", receiverID);
        intent.putExtra("RECEIVER_NAME", phoneNumber);
        //intent.putExtra("MODE", "voice");
        intent.putExtra("MODE", mode);
        startActivity(intent);

    }


    public void getUserInfo(final String jsonData, final String mode, final String userName) {
        final ProgressDialog pDialog = new ProgressDialog(getActivity());
        pDialog.setCanceledOnTouchOutside(false);
        pDialog.setMessage("Creating meeting...");
        pDialog.show();

        final String URL = ConnexConstante.DOMAIN_URL + "connex_user_sync";
        Log.i(TAG, URL);
        StringRequest sr = new StringRequest(Request.Method.POST, URL,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String result) {

                        pDialog.dismiss();

                        Log.i(TAG, result.toString());
                        try {
                            JSONObject response2 = new JSONObject(result);
                            if (response2.getBoolean("response")) {
                                JSONArray maininnerArray = response2.getJSONArray("all_connex");
                                if (maininnerArray.length() > 0) {
                                    Log.i(TAG, "unknown no ID : " + maininnerArray.getJSONObject(0).getString("id"));
                                    connexCalling(maininnerArray.getJSONObject(0).getString("id"), mode, userName);
                                }
                            } else {
                                Toast.makeText(getActivity(), "Failed to sync with server...", Toast.LENGTH_SHORT).show();
                            }
                        } catch (Exception e) {
                            e.printStackTrace();
                            Toast.makeText(getActivity(), "Failed to sync with server...", Toast.LENGTH_SHORT).show();
                        }
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                pDialog.dismiss();
                VolleyLog.d("Output : ", "Error: " + error.getMessage());
                String json = null;
                NetworkResponse response = error.networkResponse;
                if (response != null && response.data != null) {
                    switch (response.statusCode) {
                        case 400:
                            json = new String(response.data);
                            if (json != null) displayMessage(json);
                            break;
                    }
                }
            }
        }) {
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<String, String>();
                Log.i(TAG, "Post Param : " + jsonData);
                params.put("details_info", jsonData);
                return params;
            }

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put("Content-Type", "application/x-www-form-urlencoded");
                return params;
            }
        };
        ConnexApplication.getInstance().addToRequestQueue(sr);
    }


    public void displayMessage(String toastString) {
        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(getActivity());
        alertDialogBuilder.setTitle("Error");
        alertDialogBuilder
                .setMessage(Html.fromHtml(toastString))
                .setCancelable(false)
                .setPositiveButton("Retry", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        dialog.cancel();
                    }
                });
        AlertDialog alertDialog = alertDialogBuilder.create();
        alertDialog.show();
    }


    public String createJsonData(final String phonenumber) {
        JSONArray mainArray = new JSONArray();
        try {
            JSONObject mainObject = new JSONObject();
            mainObject.put("name", phonenumber);
            mainObject.put("phone", phonenumber);
            mainObject.put("email", "");
            mainObject.put("profileimg", "");
            mainArray.put(mainObject);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return mainArray.toString();
    }

}
