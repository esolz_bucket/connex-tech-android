package esolz.connexstudent.fragments;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.DisplayMetrics;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.LinkedList;

import esolz.connexstudent.ContactDetailsActivity;
import esolz.connexstudent.R;
import esolz.connexstudent.adapters.FavListingAdapter;
import esolz.connexstudent.database.ConnexDB;
import esolz.connexstudent.models.ContactDataType;

/**
 * Created by Saikat's Mac on 06/07/16.
 */

public class FavouritsContacts extends Fragment {


    private RecyclerView favListing = null;
    private ConnexDB cDB = null;
    private LinkedList<ContactDataType> mainfavData = null;
    private FavListingAdapter favAdapter = null;
    final int MY_PERMISSIONS_REQUEST_READ_CONTACTS = 1;
    private String currentTelNo = "";

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        if (cDB == null) {
            cDB = new ConnexDB(getActivity());
        }
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        mainfavData = cDB.fetchAllFavContacts();

        return inflater.inflate(R.layout.fragments_favourite, container, false);
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        favListing = (RecyclerView) view.findViewById(R.id.fav_listing);
        favListing.setLayoutManager(new GridLayoutManager(getActivity(), 2));
        favListing.setHasFixedSize(true);
        favAdapter = new FavListingAdapter(getActivity(), mainfavData, getScreenWidth());
        favListing.setAdapter(favAdapter);

        if (mainfavData.size() > 0) {
            view.findViewById(R.id.errortext).setVisibility(View.GONE);
        } else {
            view.findViewById(R.id.errortext).setVisibility(View.VISIBLE);
        }


        favAdapter.setOnItemClickListener(new FavListingAdapter.onItemClickListener() {
            @Override
            public void onItemClickdforVoice(final ContactDataType data) {

//                getActivity().runOnUiThread(new Runnable() {
//                    @Override
//                    public void run() {
//                        Intent intent = new Intent(getActivity(), ConnectActivity.class);
//                        intent.setAction(ConnectActivity.ACTION_APP);
//                        intent.putExtra("RECEIVER_ID", data.getDbID());
//                        intent.putExtra("MODE", "voice");
//                        startActivity(intent);
//                    }
//                });


                getActivity().runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        //ConnexApplication.getInstance().setSelectedContacts(data);
                        Intent i = new Intent(getActivity(), ContactDetailsActivity.class);
                        i.putExtra("CONTACT_ID", Integer.parseInt(data.getPhoneContactID()));
                        i.putExtra("PROFILE_IMAGE", data.getProfImage());
                        i.putExtra("PROFILE_NAME", data.getName());
                        startActivity(i);
                    }
                });

            }

            @Override
            public void onItemClickdforVideo(final ContactDataType data) {


//                getActivity().runOnUiThread(new Runnable() {
//                    @Override
//                    public void run() {
//                        Intent intent = new Intent(getActivity(), ConnectActivity.class);
//                        intent.setAction(ConnectActivity.ACTION_APP);
//                        intent.putExtra("RECEIVER_ID", data.getDbID());
//                        intent.putExtra("MODE", "video");
//                        startActivity(intent);
//                    }
//                });


                getActivity().runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        //ConnexApplication.getInstance().setSelectedContacts(data);
                        Intent i = new Intent(getActivity(), ContactDetailsActivity.class);
                        i.putExtra("CONTACT_ID", Integer.parseInt(data.getPhoneContactID()));
                        i.putExtra("PROFILE_IMAGE", data.getProfImage());
                        i.putExtra("PROFILE_NAME", data.getName());
                        startActivity(i);
                    }
                });


            }

            @Override
            public void onItemClickdforInvitation(final ContactDataType data) {
                //==========Can't find any logic now to do this.

//                getActivity().runOnUiThread(new Runnable() {
//                    @Override
//                    public void run() {
//                        new AlertDialog.Builder(getActivity())
//                                .setTitle("Connex")
//                                .setMessage(data.getName() + " is not a Connex user. Would you like to call from you device default calling?")
//                                .setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {
//                                    public void onClick(DialogInterface dialog, int which) {
//                                        // continue with delete
//                                        currentTelNo = data.getOriginalPhoneNumber();
//                                        callMe(currentTelNo);
//                                    }
//                                })
//                                .setNegativeButton(android.R.string.no, new DialogInterface.OnClickListener() {
//                                    public void onClick(DialogInterface dialog, int which) {
//                                        // do nothing
//                                    }
//                                })
//                                .setIcon(R.drawable.connex_demo)
//                                .show();
//                    }
//                });


                getActivity().runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        //ConnexApplication.getInstance().setSelectedContacts(data);
                        Intent i = new Intent(getActivity(), ContactDetailsActivity.class);
                        i.putExtra("CONTACT_ID", Integer.parseInt(data.getPhoneContactID()));
                        i.putExtra("PROFILE_IMAGE", data.getProfImage());
                        i.putExtra("PROFILE_NAME", data.getName());
                        startActivity(i);
                    }
                });


            }


            @Override
            public void onDetailsClickd(final ContactDataType data) {
                getActivity().runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        //ConnexApplication.getInstance().setSelectedContacts(data);
                        Intent i = new Intent(getActivity(), ContactDetailsActivity.class);
                        i.putExtra("CONTACT_ID", Integer.parseInt(data.getPhoneContactID()));
                        i.putExtra("PROFILE_IMAGE", data.getProfImage());
                        i.putExtra("PROFILE_NAME", data.getName());
                        startActivity(i);
                    }
                });
            }
        });

    }


    public float getScreenWidth() {
        DisplayMetrics metrics = new DisplayMetrics();
        getActivity().getWindowManager().getDefaultDisplay().getMetrics(metrics);
        return metrics.widthPixels;
    }


    public void callMe(final String telNO) {
        if (ActivityCompat.checkSelfPermission(getActivity(), android.Manifest.permission.CALL_PHONE) != PackageManager.PERMISSION_GRANTED) {
            // TODO: Consider calling

            if (ActivityCompat.shouldShowRequestPermissionRationale(getActivity(), android.Manifest.permission.CALL_PHONE)) {
                ActivityCompat.requestPermissions(getActivity(), new String[]{android.Manifest.permission.CALL_PHONE}, MY_PERMISSIONS_REQUEST_READ_CONTACTS);
            } else {
                // No explanation needed, we can request the permission.
                ActivityCompat.requestPermissions(getActivity(), new String[]{android.Manifest.permission.CALL_PHONE}, MY_PERMISSIONS_REQUEST_READ_CONTACTS);
            }
            return;
        } else {
            Intent callIntent = new Intent(Intent.ACTION_CALL);
            callIntent.setData(Uri.parse("tel:" + telNO));
            startActivity(callIntent);
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);

        switch (requestCode) {
            case MY_PERMISSIONS_REQUEST_READ_CONTACTS: {
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    try {
                        callMe(currentTelNo);
                    } catch (Exception e2) {
                        e2.printStackTrace();
                    }
                } else {
                    AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(getActivity());
                    alertDialogBuilder.setMessage("With out these permissions you cant call from this app.");
                    alertDialogBuilder.setPositiveButton("Retry", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface arg0, int arg1) {
                            callMe(currentTelNo);
                        }
                    });
                    alertDialogBuilder.setNegativeButton("DISCARD", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                        }
                    });
                    AlertDialog alertDialog = alertDialogBuilder.create();
                    alertDialog.show();
                }
                return;
            }
        }
    }

}
