package esolz.connexstudent.fragments;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.provider.ContactsContract;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.LinkedList;
import java.util.Locale;

import esolz.connexstudent.ContactDetailsActivity;
import esolz.connexstudent.R;
import esolz.connexstudent.adapters.ContactNameAdapter;
import esolz.connexstudent.customview.AvenirLightEditText;
import esolz.connexstudent.database.ConnexDB;
import esolz.connexstudent.models.DeviceCotactNameOnly;
import io.fabric.sdk.android.services.concurrency.AsyncTask;

/**
 * Created by Saikat's Mac on 06/07/16.
 */


public class AllContacts extends Fragment {

    private final String TAG = "LandingActivity";
    private RecyclerView contactView;
    private ContactNameAdapter mainAdapter = null;
    private ConnexDB cDB = null;
    private LinkedList<String> indexHelper = null;
    private AvenirLightEditText serachBox = null;
    private ContactReceiver updateReceiver = null;

    //=================v2.0 changes


    private LinkedList<DeviceCotactNameOnly> newVData = null;
    private LinkedList<DeviceCotactNameOnly> currentSetSearch = null;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragments_all_contacts, container, false);
    }


    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);


        cDB = new ConnexDB(getActivity());

        serachBox = (AvenirLightEditText) view.findViewById(R.id.search_box);
        contactView = (RecyclerView) view.findViewById(R.id.meeting_list);
        contactView.setHasFixedSize(true);
        contactView.setLayoutManager(new LinearLayoutManager(getActivity()));


//        if (getIntent().getAction().equals("login")) {
//            mainAdapter = new ContactsAdapter(getActivity(), cDB.fetchAllContacts(), ConnexApplication.getInstance().getMapIndex());
//            contactView.setAdapter(mainAdapter);
//        } else {
//            if (currentSet == null) {
//                currentSet = new LinkedList<ContactDataType>();
//                mainAdapter = new ContactsAdapter(getActivity(), currentSet, ConnexApplication.getInstance().getMapIndex());
//                contactView.setAdapter(mainAdapter);
//            }
//        }


//        mainAdapter = new ContactsAdapter(getActivity(), cDB.fetchAllContacts());
//        contactView.setAdapter(mainAdapter);
//
////        mainAdapter = new ContactsAdapter(getActivity(), cDB.fetchAllContacts(), ConnexApplication.getInstance().getMapIndex());
////        contactView.setAdapter(mainAdapter);
////        FastScrollRecyclerViewItemDecoration decoration = new FastScrollRecyclerViewItemDecoration(getActivity());
////        contactView.addItemDecoration(decoration);
////        contactView.setItemAnimator(new DefaultItemAnimator());
//
//
//        mainAdapter.setOnItemClickListener(new ContactsAdapter.onItemClickListener() {
//            @Override
//            public void onItemClickd(final ContactDataType data) {
//                getActivity().runOnUiThread(new Runnable() {
//                    @Override
//                    public void run() {
//                        //======Clicked Data data
////                        createMeetingDilaog(data);
//
//                        ConnexApplication.getInstance().setSelectedContacts(data);
//                        Intent i = new Intent(getActivity(), ContactDetailsActivity.class);
//                        startActivity(i);
//
//                    }
//                });
//            }
//        });


        (new FetchPhoneNumber()).executeOnExecutor(android.os.AsyncTask.THREAD_POOL_EXECUTOR);


    }


    @Override
    public void onStart() {

        if (updateReceiver == null) {
            updateReceiver = new ContactReceiver();
        }

        getActivity().registerReceiver(updateReceiver, new IntentFilter("RESULT_RECEIVER_SEVERSYNK"));

        super.onStart();
    }


    @Override
    public void onPause() {

        if (updateReceiver == null) {
            updateReceiver = new ContactReceiver();
        }

        try {
            getActivity().unregisterReceiver(updateReceiver);
        } catch (Exception e) {
            e.printStackTrace();
        }

        super.onPause();
    }


    //=============Receiver End

    private class ContactReceiver extends BroadcastReceiver {

        @Override
        public void onReceive(Context context, Intent intent) {
            if (intent != null && mainAdapter != null) {
                mainAdapter.updateData(indexHelper.indexOf(intent.getStringExtra("SynkedID")));
            }
        }
    }


    //---------------------------new contact details format


    private class FetchPhoneNumber extends AsyncTask<Void, Void, Void> {

        @Override
        protected Void doInBackground(Void... params) {


            if (newVData == null) {
                newVData = new LinkedList<DeviceCotactNameOnly>();
                currentSetSearch = new LinkedList<DeviceCotactNameOnly>();
            } else {
                newVData.clear();
                currentSetSearch.clear();
            }

            if (indexHelper == null) {
                indexHelper = new LinkedList<String>();
            } else {
                indexHelper.clear();
            }

            //=====================

            Uri contactsUri = ContactsContract.CommonDataKinds.Phone.CONTENT_URI;
            Cursor contactsCursor = getActivity().getContentResolver().query(contactsUri,
                    new String[]{ContactsContract.CommonDataKinds.Phone.CONTACT_ID, ContactsContract.CommonDataKinds.Phone.DISPLAY_NAME, ContactsContract.CommonDataKinds.Phone.PHOTO_URI}, null, null,
                    "upper(" + ContactsContract.CommonDataKinds.Phone.DISPLAY_NAME + ")");

            int contactIdIndex = contactsCursor.getColumnIndex(ContactsContract.CommonDataKinds.Phone.CONTACT_ID);
            int contactNameIndex = contactsCursor.getColumnIndex(ContactsContract.CommonDataKinds.Phone.DISPLAY_NAME);
            int contactImageIndex = contactsCursor.getColumnIndex(ContactsContract.CommonDataKinds.Phone.PHOTO_URI);


            if (contactsCursor.getCount() > 0) {

                for (int i = 0; i < contactsCursor.getCount(); i++) {

                    contactsCursor.moveToPosition(i);
                    DeviceCotactNameOnly temp_ = new DeviceCotactNameOnly();
                    temp_.setContactID(contactsCursor.getInt(contactIdIndex));
                    temp_.setContactName(contactsCursor.getString(contactNameIndex));
                    temp_.setContactImage(contactsCursor.getString(contactImageIndex));
                    temp_.setIsConnexUser(cDB.getUserDBIDFromPhoneDbId("" + contactsCursor.getInt(contactIdIndex)));

                    if (i > 0) {
                        if (!newVData.get(newVData.size() - 1).getContactName().substring(0, 1).toUpperCase().equals(temp_.getContactName().substring(0, 1).toUpperCase())) {
                            temp_.setIsHeader(true);
                        } else {
                            temp_.setIsHeader(false);
                        }
                    } else {
                        temp_.setIsHeader(true);
                    }
                    indexHelper.add("" + contactsCursor.getInt(contactIdIndex));
                    newVData.add(temp_);
                }
                contactsCursor.close();
            }


            return null;
        }

        @Override
        protected void onPostExecute(Void result) {

            mainAdapter = new ContactNameAdapter(getActivity(), newVData);
            contactView.setAdapter(mainAdapter);


            mainAdapter.setOnItemClickListener(new ContactNameAdapter.onItemClickListener() {
                @Override
                public void onItemClickd(final DeviceCotactNameOnly data) {
                    getActivity().runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            //======Clicked Data data
                            Intent i = new Intent(getActivity(), ContactDetailsActivity.class);
                            i.putExtra("CONTACT_ID", data.getContactID());
                            i.putExtra("PROFILE_IMAGE", data.getContactImage());
                            i.putExtra("PROFILE_NAME", data.getContactName());
                            startActivity(i);

                        }
                    });
                }
            });


            serachBox.addTextChangedListener(new TextWatcher() {

                @Override
                public void beforeTextChanged(CharSequence s, int start, int count, int after) {

                }

                @Override
                public void onTextChanged(CharSequence s, int start, int before, int count) {

                }

                @Override
                public void afterTextChanged(Editable s) {


                    //=========new Searching (non DB)
                    mainAdapter.filter(s.toString());


//                    if (newVData.size() > 0) {
//                        for (DeviceCotactNameOnly userListingDataType : newVData) {
//                            if (userListingDataType.getContactName().toLowerCase(Locale.getDefault()).contains(s.toString())) {
//                                currentSetSearch.add(userListingDataType);
//                            }
//                        }
//
//                        if (serachBox.getText().toString().trim().length() > 0) {
//
//
//                            mainAdapter.filter(s.toString());
//
////                            contactView.setAdapter(new ContactNameAdapter(getActivity(), currentSetSearch));
//                        } else {
//                            contactView.setAdapter(mainAdapter);
//                        }
//                    }
                    //===================

                }
            });


        }

    }


    //===========================


}
