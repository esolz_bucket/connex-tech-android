package esolz.connexstudent.GCMPushManagement;

import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.NotificationCompat;
import android.util.Log;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.google.android.gms.gcm.GcmListenerService;

import org.json.JSONException;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.TimeZone;

import esolz.connexstudent.ChatManagement;
import esolz.connexstudent.ConnexService.RingToneService;
import esolz.connexstudent.R;
import esolz.connexstudent.appliaction.ConnexApplication;
import esolz.connexstudent.appliaction.ConnexConstante;
import esolz.connexstudent.apprtc.ConnectActivity;
import esolz.connexstudent.database.ConnexDB;
import esolz.connexstudent.models.ChatDataModel;


public class MyGcmListenerService extends GcmListenerService {

    private static final String TAG = "MyGcmListenerService";
    private String OPONENT_ID = "";
    private SharedPreferences appbehavoiurPrefrence = null;
    private ConnexDB cDB = null;
    private SharedPreferences sprefState = null;


    long diff, seconds, minutes, hours, days;

    @Override
    public void onMessageReceived(String from, Bundle dataNoti) {
        JSONObject data = null;
        try {
            data = new JSONObject(dataNoti.getString("data"));
            Log.d(TAG, "From: " + from);
            Log.d(TAG, "Bundle: " + data.toString());
            //Log.d(TAG, "Message: " + message);


            //============= Time management
            //------main data "server_datetime":"2016-07-21 11:14:47"
            //------"server_timezone":"UTC"

            try {

                SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

                //Date date = formatter.parse(dateInString);

                Date date = new Date(Calendar.getInstance().getTimeInMillis());
                // To TimeZone America/New_York
                SimpleDateFormat sdfAmerica = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
                TimeZone tzInAmerica = TimeZone.getTimeZone("Atlantic/Azores");
                sdfAmerica.setTimeZone(tzInAmerica);

                String sDateInAmerica = sdfAmerica.format(date); // Convert to String first
                Date dateInAmerica = formatter.parse(sDateInAmerica);

                Date serverDate = formatter.parse(data.getString("server_datetime"));


                diff = dateInAmerica.getTime() - serverDate.getTime();

                Log.i("timezone", "Server Time : " + data.getString("server_datetime"));
                Log.i("timezone", "My Time : " + formatter.format(dateInAmerica));
                Log.i("timezone", "Diff : " + diff);

                seconds = diff / 1000;
//                minutes = seconds / 60;
//                hours = minutes / 60;
//                days = hours / 24;


            } catch (Exception e) {
                e.printStackTrace();
            }


            //========================

            if (data.getString("mod").equalsIgnoreCase("null") || data.getString("mod").equalsIgnoreCase("")) {

                //==========chat management

                //--------insert data to DB

                if (cDB == null) {
                    cDB = new ConnexDB(getApplicationContext());
                }
                ChatDataModel model_ = new ChatDataModel();
                SimpleDateFormat sFormater = new SimpleDateFormat("dd MMMM, yyyy hh:mm a");
                model_.setDateTime(sFormater.format(new Date(Calendar.getInstance().getTimeInMillis())));
                model_.setSenderID(data.getString("sender_id"));
                model_.setReceiverID(data.getString("receiver_id"));
                model_.setMessage(data.getString("message"));
                model_.setIsLoadingFailed(true);
                cDB.insertChat(model_);

                //------------data inserted

                if (appbehavoiurPrefrence == null) {
                    appbehavoiurPrefrence = getSharedPreferences("ConnexTracker", MODE_PRIVATE);
                }


                Intent i = new Intent(getApplicationContext(), ChatManagement.class);
                i.setAction(ChatManagement.ACTION_NOTIFICATION);
                i.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                i.putExtra("OPONENET_ID", data.getString("sender_id"));


                PendingIntent pendingIntent = PendingIntent.getActivity(this, 0, i,
                        PendingIntent.FLAG_ONE_SHOT);

                if (appbehavoiurPrefrence.getBoolean("IS_RUNNING", false) == false) {
                    Uri defaultSoundUri = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
                    NotificationCompat.Builder notificationBuilder = new NotificationCompat.Builder(this)
                            .setSmallIcon(R.drawable.ic_notification)
                            .setContentTitle(data.getString("from_name"))
                            .setContentText("sent you a message.")
                            .setAutoCancel(true)
                            .setContentIntent(pendingIntent)
                            .setSound(defaultSoundUri);

                    NotificationManager notificationManager =
                            (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
                    notificationManager.notify(5, notificationBuilder.build());
                } else {
                    informCharRoom(data.getString("message"), data.getString("sender_id"), data.getString("receiver_id"));
                }

            } else {

                if (data.getString("receiver_id").equalsIgnoreCase(ConnexApplication.getInstance().getUserID())) {
                    OPONENT_ID = data.getString("sender_id");
                } else {
                    OPONENT_ID = data.getString("receiver_id");
                }

                sprefState = getSharedPreferences("CallActivity", MODE_PRIVATE);

                Log.i("DHDH", "" + sprefState.getString("status", ""));
                Log.i("DHDH", "" + data.getString("mod"));

                if ((sprefState.getString("status", "").equalsIgnoreCase("busy") && data.getString("mod").equalsIgnoreCase("voice")) ||
                        (sprefState.getString("status", "").equalsIgnoreCase("busy") && data.getString("mod").equalsIgnoreCase("video"))) {

                    //========user busy State: Dialing someone's number

//                    pushHelperDhoperUrl(ConnexConstante.DOMAIN_URL + "push_send_v2?roomId=000000&sender_id=" + ConnexApplication.getInstance().getUserID() + "&receiver_id=" + OPONENT_ID + "&mod=reject&pem_mod=prod");
//
//                    userBusyNotification(data.getString("from_name"));


                    hittingBrodcastManagementHoldMangement(data.getString("roomId"), data.getString("from_name"), data.getString("mod"), data.getString("phone"));//phone


                } else {
                    sprefState = getSharedPreferences("ConnectionActivity", MODE_PRIVATE);
                    if ((sprefState.getString("status", "").equalsIgnoreCase("busy") && data.getString("mod").equalsIgnoreCase("voice")) ||
                            (sprefState.getString("status", "").equalsIgnoreCase("busy") && data.getString("mod").equalsIgnoreCase("video"))) {

                        //========user busy State: is in call

//                        pushHelperDhoperUrl(ConnexConstante.DOMAIN_URL + "push_send_v2?roomId=000000&sender_id=" + ConnexApplication.getInstance().getUserID() + "&receiver_id=" + OPONENT_ID + "&mod=reject&pem_mod=prod");
//
//                        userBusyNotification(data.getString("from_name"));


                        hittingBrodcastManagementHoldMangement(data.getString("roomId"), data.getString("from_name"), data.getString("mod"), data.getString("phone"));//phone


                    } else {


                        if (data.getString("mod").equalsIgnoreCase("reject") || data.getString("mod").equalsIgnoreCase("callEnded") ||
                                data.getString("mod").equalsIgnoreCase("voiceaccept") ||
                                data.getString("mod").equalsIgnoreCase("videoaccept")) {

                            String temp_ = data.getString("mod");
                            if (temp_.equalsIgnoreCase("callEnded")) {
                                temp_ = "reject";
                            }


                            hittingBrodcastManagementHoldMangement(data.getString("roomId"), data.getString("from_name"), temp_, data.getString("phone"));//phone

                            hittingBrodcastManagement(data.getString("roomId"), data.getString("from_name"), temp_, data.getString("phone"));//phone

                        } else if (data.getString("mod").equalsIgnoreCase("voice") ||
                                data.getString("mod").equalsIgnoreCase("video")) {

                            Log.i("timezone", "My UST date seconds : " + seconds + " minutes: " + minutes + " hours: " + hours + " days: " + days);

                            if (seconds < 90) {
                                sentMessageToIntent(data.getString("roomId"), data.getString("from_name"), data.getString("mod"), data.getString("phone"));//phone
                                sendNotification(data.getString("from_name") + " is calling you.", data.getString("roomId"), data.getString("from_name"), data.getString("mod"), data.getString("phone"));
                            } else {

                                minutes = seconds / 60;
                                seconds = seconds % 60;

                                hours = minutes / 60;
                                minutes = minutes % 60;

                                days = hours / 24;
                                minutes = minutes % 60;

                                String temp_ = "";
                                if (days > 0) {
                                    temp_ = temp_ + days + " days";
                                }

                                if (hours > 0) {
                                    temp_ = temp_ + hours + " hours";
                                }

                                if (minutes > 0) {
                                    temp_ = temp_ + minutes + " minutes";
                                }

                                if (seconds > 0) {
                                    temp_ = temp_ + seconds + " seconds";
                                }

                                Log.i("timezone", "My UST date : " + temp_);

                                timeManagedNotification(data.getString("from_name"), temp_);
                            }

                        }
                    }
                }
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    public void sentMessageToIntent(final String meetingRoom, final String callerName, final String mode, final String phone) {
        Log.d(TAG, "testingbysaikat");
        Intent i = new Intent(getApplicationContext(), RingToneService.class);
        i.setAction(RingToneService.ACTION_START);
        i.putExtra("ROOM_NAME", meetingRoom);
        i.putExtra("CALLER_NAME", callerName);
        i.putExtra("MODE", mode);
        i.putExtra("PHONE", phone);
        i.putExtra("OPONENT_ID", OPONENT_ID);
        startService(i);

    }


    public void hittingBrodcastManagement(final String meetingRoom, final String callerName, final String mode, final String phone) {
        Log.d(TAG, "testingbysaikat");
        Intent intent = new Intent();
        intent.setAction("RESULT_RECEIVER_CONNECT");
        intent.putExtra("ROOM_NAME", meetingRoom);
        intent.putExtra("CALLER_NAME", callerName);
        intent.putExtra("MODE", mode);
        intent.putExtra("OPONENT_ID", OPONENT_ID);
        sendBroadcast(intent);
    }

    public void hittingBrodcastManagementHoldMangement(final String meetingRoom, final String callerName, final String mode, final String phone) {
        Log.d(TAG, "testingbysaikat");
        Intent intent = new Intent();
        intent.setAction("RESULT_RECEIVER_HOLDING");
        intent.putExtra("ROOM_NAME", meetingRoom);
        intent.putExtra("CALLER_NAME", callerName);
        intent.putExtra("MODE", mode);
        intent.putExtra("OPONENT_ID", OPONENT_ID);
        intent.putExtra("PHONE", phone);
        sendBroadcast(intent);
    }

    private void sendNotification(String message, String roomID, String callerName, final String mode, final String phone) {

        Intent intent = new Intent(this, ConnectActivity.class);
        intent.setAction(ConnectActivity.ACTION_NOTIFICATION);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        intent.putExtra("ROOM_NAME", roomID);
        intent.putExtra("CALLER_NAME", callerName);
        intent.putExtra("MODE", mode);
        intent.putExtra("OPONENT_ID", OPONENT_ID);


        PendingIntent pendingIntent = PendingIntent.getActivity(this, 0, intent,
                PendingIntent.FLAG_ONE_SHOT);

        Intent yesReceive = new Intent(getApplicationContext(), RingToneService.class);
        yesReceive.setAction(RingToneService.ACTION_KILL);
//        PendingIntent pendingIntentNo = PendingIntent.getService(this, 12345, yesReceive, PendingIntent.FLAG_UPDATE_CURRENT);


        // Uri defaultSoundUri = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
        NotificationCompat.Builder notificationBuilder = new NotificationCompat.Builder(this)
                .setSmallIcon(R.drawable.ic_notification)
                .setContentTitle("Connex")
                .setContentText(message)
                .setAutoCancel(true)
                .addAction(R.drawable.ic_action_accept_call, "OK", pendingIntent);


        NotificationManager notificationManager =
                (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
        notificationManager.cancel(2);
        notificationManager.notify(2, notificationBuilder.build());


    }


    public void informCharRoom(final String meesage, final String senderID, final String receiverID) {
        Log.d(TAG, "testingbysaikat");
        Intent intent = new Intent();
        intent.setAction("CHAT_ROOM_ENTRY");
        intent.putExtra("RECEIVER_ID", receiverID);
        intent.putExtra("SENDER_ID", senderID);
        intent.putExtra("MESSAGE", meesage);
        sendBroadcast(intent);
    }


    private void userBusyNotification(final String callerName) {


        Uri defaultSoundUri = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
        NotificationCompat.Builder notificationBuilder = new NotificationCompat.Builder(this)
                .setSmallIcon(R.drawable.ic_notification)
                .setContentTitle(callerName)
                .setContentText("Tried to call you just now.")
                .setAutoCancel(true)
                .setSound(defaultSoundUri);


        NotificationManager notificationManager =
                (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
        notificationManager.cancel(7);
        notificationManager.notify(7, notificationBuilder.build());


    }


    public void pushHelperDhoperUrl(final String URL) {
        JsonObjectRequest jsonObjReq = new JsonObjectRequest(Request.Method.GET, URL,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        Log.i(TAG, response.toString());
                        Log.i(TAG, URL);

                        try {
                            if (response.getBoolean("response")) {

                            } else {
                            }
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                Log.i(TAG, "Error: " + error.getMessage());
            }
        });
        ConnexApplication.getInstance().addToRequestQueue(jsonObjReq);
    }


    private void timeManagedNotification(final String callerName, final String timeDif) {

        Uri defaultSoundUri = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
        NotificationCompat.Builder notificationBuilder = new NotificationCompat.Builder(this)
                .setSmallIcon(R.drawable.ic_notification)
                .setContentTitle(callerName)
                .setContentText("tried to call you " + timeDif + " ago")
                .setAutoCancel(true)
                .setSound(defaultSoundUri);


        NotificationManager notificationManager =
                (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
        notificationManager.cancel(8);
        notificationManager.notify(8, notificationBuilder.build());


    }

}
