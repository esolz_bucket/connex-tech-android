package esolz.connexstudent;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.Html;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.NetworkResponse;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.StringRequest;
import com.digits.sdk.android.AuthCallback;
import com.digits.sdk.android.DigitsAuthButton;
import com.digits.sdk.android.DigitsException;
import com.digits.sdk.android.DigitsSession;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

import esolz.connexstudent.appliaction.ConnexApplication;
import esolz.connexstudent.appliaction.ConnexConstante;
import esolz.connexstudent.customview.AvenirLightEditText;


public class LoginActivity extends AppCompatActivity {

    private View pBar = null;
    private View loginBttn = null;
    private final String TAG = "LoginActivity";
    private AvenirLightEditText userName, userPhone, userEmail;
    private JSONArray mainArray = null;


    //============== Digits Verification


    DigitsAuthButton dAuthButton = null;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().hide();


        pBar = findViewById(R.id.progbar);
        loginBttn = findViewById(R.id.log_me_in);
        dAuthButton = (DigitsAuthButton) findViewById(R.id.auth_button);
        dAuthButton.setCallback(new AuthCallback() {
            @Override
            public void success(DigitsSession session, String phoneNumber) {
                // Do something with the session
                Log.i("phoneNumber", phoneNumber);
                if (userName.getText().toString().trim().equals("")) {
                    userName.setError("Enter User Name");
                } else {
                    if (userEmail.getText().toString().trim().length() > 0) {
                        if (android.util.Patterns.EMAIL_ADDRESS.matcher(userEmail.getText().toString().trim()).matches()) {
                            (findViewById(R.id.loader_view)).setVisibility(View.VISIBLE);
                            makeMeSignIn(phoneNumber);
                        } else {
                            userEmail.setError("Invalid Email Address");
                        }
                    } else {
                        (findViewById(R.id.loader_view)).setVisibility(View.VISIBLE);
                        makeMeSignIn(phoneNumber);
                    }
                }

            }

            @Override
            public void failure(DigitsException exception) {
                // Do something on failure
            }
        });

        userName = (AvenirLightEditText) findViewById(R.id.user_email);
        userPhone = (AvenirLightEditText) findViewById(R.id.user_password);
        userEmail = (AvenirLightEditText) findViewById(R.id.user_email_main);


        //Digits.getSessionManager().clearActiveSession();


        loginBttn.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {

                dAuthButton.performClick();
            }
        });


    }

    //https://github.com/WhisperSystems/Signal-Android
    //https://open-whisper-systems.readme.io/docs

    public void makeMeSignIn(String phoneNumber) {

        Log.i(TAG, "" + phoneNumber);

        phoneNumber = phoneNumber.replaceAll("\\s", "");// **********removing
        phoneNumber = phoneNumber.replaceAll(" ", ""); // all
        phoneNumber = phoneNumber.replaceAll("[(.^)]*", "");
        phoneNumber = phoneNumber.replaceAll("-", "");
        phoneNumber = phoneNumber.replaceAll("\\,", "");
        phoneNumber = phoneNumber.replaceAll("  ", "");
        phoneNumber = phoneNumber.trim();
        phoneNumber = phoneNumber.substring((phoneNumber.length() - 10), phoneNumber.length());

        Log.i(TAG, "" + phoneNumber);

        final String tempString = phoneNumber;

        pBar.setVisibility(View.VISIBLE);
        final String URL = ConnexConstante.DOMAIN_URL + "login";
        Log.i(TAG, URL);
        StringRequest sr = new StringRequest(Request.Method.POST, URL,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String result) {
                        Log.i(TAG, result.toString());
                        pBar.setVisibility(View.GONE);
                        loginBttn.setEnabled(true);
                        try {
                            JSONObject response2 = new JSONObject(result);
                            if (response2.getBoolean("response")) {
                                ConnexApplication.getInstance().setUserID(response2.getString("user_id"));
                                ConnexApplication.getInstance().setUserLoginPhoneNumber(tempString);
                                Intent i = new Intent(LoginActivity.this, LandingPagev2.class);
                                //i.setAction("login");
                                startActivity(i);
                                finish();
                            } else {
                                (findViewById(R.id.loader_view)).setVisibility(View.GONE);
                                Toast.makeText(getApplicationContext(), "Sign in Failed...", Toast.LENGTH_SHORT).show();
                            }
                        } catch (Exception e) {
                            e.printStackTrace();
                            (findViewById(R.id.loader_view)).setVisibility(View.GONE);
                            Toast.makeText(getApplicationContext(), "Sign in Failed...", Toast.LENGTH_SHORT).show();
                        }
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                (findViewById(R.id.loader_view)).setVisibility(View.GONE);
                VolleyLog.d("Output : ", "Error: " + error.getMessage());
                String json = null;
                NetworkResponse response = error.networkResponse;
                if (response != null && response.data != null) {
                    switch (response.statusCode) {
                        case 400:
                            json = new String(response.data);
                            if (json != null) displayMessage(json);
                            break;
                    }
                }
            }
        }) {
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<String, String>();
                Log.i(TAG, "Post Param : " + createJsonData(tempString));
                params.put("details_info", createJsonData(tempString));
                return params;
            }

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put("Content-Type", "application/x-www-form-urlencoded");
                return params;
            }
        };
        ConnexApplication.getInstance().addToRequestQueue(sr);
    }


    public void displayMessage(String toastString) {
        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(LoginActivity.this);
        alertDialogBuilder.setTitle("Error");
        alertDialogBuilder
                .setMessage(Html.fromHtml(toastString))
                .setCancelable(false)
                .setPositiveButton("Retry", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        dialog.cancel();
                    }
                });
        AlertDialog alertDialog = alertDialogBuilder.create();
        alertDialog.show();
    }

    @Override
    protected void onStart() {
        super.onStart();
        pBar.setVisibility(View.GONE);
    }


    public String createJsonData(final String phoneNumber) {
        try {
            mainArray = new JSONArray();
            JSONObject mainObject = new JSONObject();
            mainObject.put("name", userName.getText().toString().trim());
            mainObject.put("phone", phoneNumber);
            mainObject.put("email", userEmail.getText().toString().trim());
            mainObject.put("profileimg", "");
            mainObject.put("device_token_ios", "");
            mainObject.put("device_token_android", ConnexApplication.getInstance().getGCM_TOKEN());
            mainArray.put(mainObject);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        if (mainArray != null) {
            try {
                Log.i(TAG, mainArray.toString());
                //return URLEncoder.encode(mainArray.toString(), "UTF-8");
                return mainArray.toString();
            } catch (Exception e) {
                e.printStackTrace();
                return "";
            }
        } else {
            return "";
        }
    }
}
