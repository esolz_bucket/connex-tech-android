package esolz.connexstudent.criptography;

import java.security.spec.KeySpec;

import javax.crypto.Cipher;
import javax.crypto.SecretKey;
import javax.crypto.SecretKeyFactory;
import javax.crypto.spec.PBEKeySpec;
import javax.crypto.spec.SecretKeySpec;

/**
 * Created by Saikat's Mac on 13/07/16.
 */

public class SecurityManagement {


    //    private final String TAG = "SecurityManagement";
    private final static String ALGORITHM = "AES";
    private final static String HEX = "0123456789ABCDEF";

//    public String encriptMe(final String text) {
//
//        String result = "";
//        try {
//            String key = "Bar12345Bar12345"; // 128 bit key
//            // Create key and cipher
//            Key aesKey = new SecretKeySpec(key.getBytes(), "AES");
//            Cipher cipher = Cipher.getInstance("AES");
//            // encrypt the text
//            cipher.init(Cipher.ENCRYPT_MODE, aesKey);
//            byte[] encrypted = cipher.doFinal(text.getBytes());
//            result = new String(encrypted);
//            Log.i(TAG, "Encrypted : " + result);
////            // decrypt the text
////            cipher.init(Cipher.DECRYPT_MODE, aesKey);
////            String decrypted = new String(cipher.doFinal(encrypted));
////            Log.i(TAG, "Decrypted : " + new String(decrypted));
//        } catch (Exception e) {
//            e.printStackTrace();
//        }
//
//        return result;
//    }
//
//
//    public String decryptMe(final String text) {
//
//        String result = "";
//
//        try {
//            String key = "Bar12345Bar12345"; // 128 bit key
//            // Create key and cipher
//            Key aesKey = new SecretKeySpec(key.getBytes(), "AES");
//            Cipher cipher = Cipher.getInstance("AES");
////            // encrypt the text
////            cipher.init(Cipher.ENCRYPT_MODE, aesKey);
//            //byte[] encrypted = cipher.doFinal(text.getBytes());
////            Log.i(TAG, "Encrypted : " + new String(encrypted));
//            // decrypt the text
//            cipher.init(Cipher.DECRYPT_MODE, aesKey);
//            byte[] encrypted = cipher.doFinal(text.getBytes());
//            String decrypted = new String(encrypted);
//            result = new String(decrypted);
//            Log.i(TAG, "Decrypted : " + result);
//        } catch (Exception e) {
//            e.printStackTrace();
//        }
//
//        return result;
//    }


    /**
     * Encrypt data
     *
     * @param secretKey - a secret key used for encryption
     * @param data      - data to encrypt
     * @return Encrypted data
     * @throws Exception
     */


    public static String cipher(String secretKey, String data) throws Exception {

        SecretKeyFactory factory = SecretKeyFactory.getInstance("PBKDF2WithHmacSHA1");
        KeySpec spec = new PBEKeySpec(secretKey.toCharArray(), secretKey.getBytes(), 128, 256);
        SecretKey tmp = factory.generateSecret(spec);
        SecretKey key = new SecretKeySpec(tmp.getEncoded(), ALGORITHM);

        Cipher cipher = Cipher.getInstance(ALGORITHM);
        cipher.init(Cipher.ENCRYPT_MODE, key);

        return toHex(cipher.doFinal(data.getBytes()));
    }

    /**
     * Decrypt data
     *
     * @param secretKey - a secret key used for decryption
     * @param data      - data to decrypt
     * @return Decrypted data
     * @throws Exception
     */
    public static String decipher(String secretKey, String data) throws Exception {

        SecretKeyFactory factory = SecretKeyFactory.getInstance("PBKDF2WithHmacSHA1");
        KeySpec spec = new PBEKeySpec(secretKey.toCharArray(), secretKey.getBytes(), 128, 256);
        SecretKey tmp = factory.generateSecret(spec);
        SecretKey key = new SecretKeySpec(tmp.getEncoded(), ALGORITHM);

        Cipher cipher = Cipher.getInstance(ALGORITHM);

        cipher.init(Cipher.DECRYPT_MODE, key);

        return new String(cipher.doFinal(toByte(data)));
    }

// Helper methods

    private static byte[] toByte(String hexString) {
        int len = hexString.length() / 2;

        byte[] result = new byte[len];

        for (int i = 0; i < len; i++)
            result[i] = Integer.valueOf(hexString.substring(2 * i, 2 * i + 2), 16).byteValue();
        return result;
    }

    public static String toHex(byte[] stringBytes) {
        StringBuffer result = new StringBuffer(2 * stringBytes.length);

        for (int i = 0; i < stringBytes.length; i++) {
            result.append(HEX.charAt((stringBytes[i] >> 4) & 0x0f)).append(HEX.charAt(stringBytes[i] & 0x0f));
        }

        return result.toString();
    }

}
