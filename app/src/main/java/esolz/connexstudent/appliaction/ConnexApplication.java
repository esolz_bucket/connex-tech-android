package esolz.connexstudent.appliaction;

import android.app.Application;
import android.content.SharedPreferences;
import android.text.TextUtils;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.toolbox.Volley;
import com.crashlytics.android.Crashlytics;

import com.digits.sdk.android.AuthCallback;
import com.digits.sdk.android.Digits;
import com.digits.sdk.android.DigitsException;
import com.digits.sdk.android.DigitsSession;
import com.twitter.sdk.android.core.TwitterAuthConfig;
import com.twitter.sdk.android.core.TwitterCore;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.LinkedList;

import esolz.connexstudent.models.ContactDataType;
import io.fabric.sdk.android.Fabric;


/**
 * Created by Saikat's Mac on 03/06/16.
 */

public class ConnexApplication extends Application {

    // Note: Your consumer key and secret should be obfuscated in your source code before shipping.
    private static final String TWITTER_KEY = "w4XdiJhSVUySId31sesJfMnbs";
    private static final String TWITTER_SECRET = "tzzT9YalypJqQFCUUEIwds1FsyDzlvP2Rdm0DE5wpzVvy7UNfA";



    private RequestQueue mRequestQueue;
    private static ConnexApplication mInstance;
    public final String TAG = "ConnexApplication";
    private SharedPreferences mainPreference = null;
    private String GCM_TOKEN = "";
    private ContactDataType selectedContacts = null;


    private HashMap<String, Integer> mapIndex = null;
    private LinkedList<ContactDataType> mainContactsData = null;


    public String getGCM_TOKEN() {
        return GCM_TOKEN;
    }

    public void setGCM_TOKEN(String GCM_TOKEN) {
        this.GCM_TOKEN = GCM_TOKEN;
    }



    //================Digits Management



    @Override
    public void onCreate() {
        super.onCreate();
        TwitterAuthConfig authConfig = new TwitterAuthConfig(TWITTER_KEY, TWITTER_SECRET);
        Fabric.with(this, new Crashlytics(), new TwitterCore(authConfig), new Digits.Builder().build());
        mInstance = this;

        mainContactsData = new LinkedList<ContactDataType>();
        mapIndex = new LinkedHashMap<String, Integer>();




    }

    public static synchronized ConnexApplication getInstance() {
        return mInstance;
    }

    public RequestQueue getRequestQueue() {
        if (mRequestQueue == null) {
            mRequestQueue = Volley.newRequestQueue(getApplicationContext());
        }
        return mRequestQueue;
    }

    public <T> void addToRequestQueue(Request<T> req, String tag) {
        req.setTag(TextUtils.isEmpty(tag) ? TAG : tag);
        getRequestQueue().add(req);
    }

    public <T> void addToRequestQueue(Request<T> req) {
        req.setTag(TAG);
        getRequestQueue().add(req);
    }

    public void cancelPendingRequests(Object tag) {
        if (mRequestQueue != null) {
            mRequestQueue.cancelAll(tag);
        }
    }


    public void setUserID(final String userID) {
        if (mainPreference == null) {
            mainPreference = getSharedPreferences("ConnexUser", MODE_PRIVATE);
        }
        SharedPreferences.Editor edit = mainPreference.edit();
        edit.putString("userID", userID);
        edit.commit();
    }

    public void setUserLoginPhoneNumber(final String phoneNumber) {
        if (mainPreference == null) {
            mainPreference = getSharedPreferences("ConnexUser", MODE_PRIVATE);
        }
        SharedPreferences.Editor edit = mainPreference.edit();
        edit.putString("userPHONE", phoneNumber);
        edit.commit();
    }


    public String getUserID() {
        if (mainPreference == null) {
            mainPreference = getSharedPreferences("ConnexUser", MODE_PRIVATE);
        }
        return mainPreference.getString("userID", "");
    }


    public String getUserPhone() {
        if (mainPreference == null) {
            mainPreference = getSharedPreferences("ConnexUser", MODE_PRIVATE);
        }
        return mainPreference.getString("userPHONE", "");
    }


    public HashMap<String, Integer> getMapIndex() {
        return mapIndex;
    }

    public void setMapIndex(HashMap<String, Integer> mapIndex) {
        this.mapIndex = mapIndex;
    }

    public LinkedList<ContactDataType> getMainContactsData() {
        return mainContactsData;
    }

    public void setMainContactsData(LinkedList<ContactDataType> mainContactsData) {
        this.mainContactsData = mainContactsData;
    }


    public ContactDataType getSelectedContacts() {
        return selectedContacts;
    }

    public void setSelectedContacts(ContactDataType selectedContacts) {
        this.selectedContacts = selectedContacts;
    }
}
