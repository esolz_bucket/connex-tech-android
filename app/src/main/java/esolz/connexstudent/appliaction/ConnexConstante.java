package esolz.connexstudent.appliaction;

/**
 * Created by Saikat's Mac on 03/06/16.
 */

public final class ConnexConstante {

    //public static final String DOMAIN_URL = "http://esolz.co.in/lab6/connex/jsonapp_control/";
    public static final String DOMAIN_URL = "http://ec2-52-34-171-68.us-west-2.compute.amazonaws.com/api/jsonapp_control/";
    public static final int SMART_DB_VERSION = 8;

}
