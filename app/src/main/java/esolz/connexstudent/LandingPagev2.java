package esolz.connexstudent;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v4.view.ViewPager.OnPageChangeListener;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.LinearLayout;

import esolz.connexstudent.ConnexService.ContactFetchingService;
import esolz.connexstudent.criptography.SecurityManagement;
import esolz.connexstudent.fragments.AllContacts;
import esolz.connexstudent.fragments.CallLogs;
import esolz.connexstudent.fragments.FavouritsContacts;

public class LandingPagev2 extends AppCompatActivity {


    private TabLayout tabs = null;
    private ViewPager mainPager = null;
    private String[] header = new String[3];
    private LinearLayout clearLog = null;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_landing_pagev2);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        //getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        //getSupportActionBar().setHomeAsUpIndicator(R.drawable.ic_action_arrow_left);
        getSupportActionBar().setTitle("");


        Intent i = new Intent(getApplicationContext(), ContactFetchingService.class);
        i.setAction(ContactFetchingService.ACTION_ON);
        startService(i);

        tabs = (TabLayout) findViewById(R.id.tabs);
        mainPager = (ViewPager) findViewById(R.id.main_frag_folder);
        clearLog = (LinearLayout) findViewById(R.id.clear_log);


        header[2] = "Favorite";
        header[1] = "Logs";
        header[0] = "Contacts";

        mainPager.setAdapter(new SectionsPagerAdapter(getSupportFragmentManager()));
        tabs.setupWithViewPager(mainPager);


        mainPager.addOnPageChangeListener(new OnPageChangeListener() {

            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

            }

            @Override
            public void onPageSelected(int position) {
                try {
                    if (position == 1) {
                        clearLog.setVisibility(View.VISIBLE);
                    } else {
                        clearLog.setVisibility(View.GONE);
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });




//        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
//        fab.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                Intent i = new Intent(LandingPagev2.this, ChatManagement.class);
//                i.setAction(ChatManagement.ACTION_APP);
//                startActivity(i);
//            }
//        });


        //==========tempurary encription test


//        SecurityManagement tempEnc_ = new SecurityManagement();
//
//
//        tempEnc_.decryptMe(tempEnc_.encriptMe("Hey, how are you?"));


        String text = "Hey, how are you?";
        String key = "keliyebrindabondekhiyedobo";

        String crypted = null;
        try {
            crypted = SecurityManagement.cipher(key, text);
            String decrypted = SecurityManagement.decipher(key, crypted);

            Log.d("CRYPTO-TEST", "plain: " + text);

            Log.d("CRYPTO-TEST", "crypted: " + crypted);
            Log.d("CRYPTO-TEST", "decrypted: " + decrypted);

        } catch (Exception e) {
            e.printStackTrace();
        }

    }


    public class SectionsPagerAdapter extends FragmentPagerAdapter {

        public SectionsPagerAdapter(FragmentManager fm) {
            super(fm);
            if (fm.getFragments() != null) {
                fm.getFragments().clear();
            }
        }

        @Override
        public Fragment getItem(int position) {

            if (position == 2) {
                return new FavouritsContacts();
            } else if (position == 1) {
                return new CallLogs();
            } else {
                return new AllContacts();
            }

        }

        @Override
        public int getCount() {
            return 3;
        }

        @Override
        public CharSequence getPageTitle(int position) {
            return header[position];
        }
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_splash, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == R.id.action_settings) {
            Intent intent = new Intent(Intent.ACTION_VIEW);
            intent.setData(Uri.parse("market://details?id=esolz.connexstudent"));
            startActivity(intent);
        }

        return super.onOptionsItemSelected(item);
    }


    public void fireFromSearch(final String phoneID, final String profileImage, final String profName) {
        Intent i = new Intent(LandingPagev2.this, ContactDetailsActivity.class);
        i.putExtra("CONTACT_ID", Integer.parseInt(phoneID));
        i.putExtra("PROFILE_IMAGE", profileImage);
        i.putExtra("PROFILE_NAME", profName);
        startActivity(i);
    }


    @Override
    protected void onResume() {
        super.onResume();

        try {
            if (mainPager.getCurrentItem() == 1) {
                clearLog.setVisibility(View.VISIBLE);
            } else {
                clearLog.setVisibility(View.GONE);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

    }
}
