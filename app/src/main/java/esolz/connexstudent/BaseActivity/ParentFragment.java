package esolz.connexstudent.BaseActivity;

import android.content.SharedPreferences;
import android.support.v4.app.Fragment;
import android.util.Log;

/**
 * Created by Saikat's Mac on 11/07/16.
 */

public class ParentFragment extends Fragment {

    private SharedPreferences appbehavoiurPrefrence = null;

    @Override
    public void onStart() {
        if (appbehavoiurPrefrence == null) {
            appbehavoiurPrefrence = getActivity().getSharedPreferences("ConnexTracker", getActivity().MODE_PRIVATE);
        }
        SharedPreferences.Editor edit = appbehavoiurPrefrence.edit();
        edit.putBoolean("IS_RUNNING", true);
        edit.commit();
        Log.i("ParentFragment","onStart");
        super.onStart();
    }


    @Override
    public void onPause() {
        if (appbehavoiurPrefrence == null) {
            appbehavoiurPrefrence = getActivity().getSharedPreferences("ConnexTracker", getActivity().MODE_PRIVATE);
        }
        SharedPreferences.Editor edit = appbehavoiurPrefrence.edit();
        edit.putBoolean("IS_RUNNING", false);
        edit.commit();
        Log.i("ParentFragment", "onPause");

        super.onPause();
    }
}
