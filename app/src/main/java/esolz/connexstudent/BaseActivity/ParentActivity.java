package esolz.connexstudent.BaseActivity;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;

import esolz.connexstudent.apprtc.ConnectActivity;


/**
 * Created by Saikat's Mac on 08/06/16.
 */

public class ParentActivity extends AppCompatActivity {


    private SharedPreferences appbehavoiurPrefrence = null;
    private CallAcceptenceReceiver updateReceiver = null;
    private boolean isregistered = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (appbehavoiurPrefrence == null) {
            appbehavoiurPrefrence = getSharedPreferences("ConnexTracker", MODE_PRIVATE);
        }
        if (updateReceiver == null) {
            updateReceiver = new CallAcceptenceReceiver();
        }
    }

    @Override
    protected void onStart() {
        super.onStart();
        if (appbehavoiurPrefrence == null) {
            appbehavoiurPrefrence = getSharedPreferences("ConnexTracker", MODE_PRIVATE);
        }
        SharedPreferences.Editor edit = appbehavoiurPrefrence.edit();
        edit.putBoolean("IS_RUNNING", true);
        edit.commit();
        //====================
        if (updateReceiver == null) {
            updateReceiver = new CallAcceptenceReceiver();
        }

        registerReceiver(updateReceiver, new IntentFilter("RESULT_RECEIVER"));
        isregistered = true;
    }

    @Override
    protected void onPause() {
        super.onPause();
        if (appbehavoiurPrefrence == null) {
            appbehavoiurPrefrence = getSharedPreferences("ConnexTracker", MODE_PRIVATE);
        }
        SharedPreferences.Editor edit = appbehavoiurPrefrence.edit();
        edit.putBoolean("IS_RUNNING", false);
        edit.commit();
        //====================
        try {
            unregisterReceiver(updateReceiver);
        } catch (Exception e) {
            e.printStackTrace();
        }

    }


    private class CallAcceptenceReceiver extends BroadcastReceiver {

        @Override
        public void onReceive(Context context, Intent intent) {
            Log.d("MyGcmListenerService", " onReceive ");

            Intent i = new Intent(getApplicationContext(), ConnectActivity.class);
            i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
            i.setAction(ConnectActivity.ACTION_NOTIFICATION);
            i.putExtra("ROOM_NAME", intent.getStringExtra("ROOM_NAME"));
            i.putExtra("CALLER_NAME", intent.getStringExtra("CALLER_NAME"));
            i.putExtra("MODE", intent.getStringExtra("MODE"));
            startActivity(i);
        }
    }




}
