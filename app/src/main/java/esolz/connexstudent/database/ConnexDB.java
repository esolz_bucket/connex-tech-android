package esolz.connexstudent.database;

import android.content.ContentValues;
import android.content.Context;
import android.database.sqlite.SQLiteCursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.LinkedList;

import esolz.connexstudent.appliaction.ConnexConstante;
import esolz.connexstudent.models.CallLogsDType;
import esolz.connexstudent.models.ChatDataModel;
import esolz.connexstudent.models.ContactDataType;

/**
 * Created by Saikat's Mac on 24/06/16.
 */


public class ConnexDB extends SQLiteOpenHelper {

    private Context context = null;
    private final String TAG = "ConnexDB";

    private final String TABLE_CONTACTS = "ConnexContacts";

    private final String CONTACTS_NUMBER = "CONTACTS_NUMBER";
    private final String CONTACTS_DEVICE_NUMBER = "CONTACTS_DEVICE_NUMBER";
    private final String CONTACTS_NAME = "CONTACTS_NAME";
    private final String CONTACTS_PROF_IMAGE = "CONTACTS_PROF_IMAGE";
    private final String CONTACTS_IS_CONNEX_USER = "CONTACTS_IS_CONNEX_USER";
    private final String CONTACTS_IS_SYNKED = "CONTACTS_IS_SYNKED";
    private final String CONTACTS_DBID = "CONTACTS_DBID";
    private final String CONTACTS_PHONE_ID = "CONTACTS_PHONE_ID";
    private final String CONTACTS_IS_FAVOURITE = "CONTACTS_IS_FAVOURITE";


    private final String TABLE_CALL_LOGS = "ConnexCallLogs";

    private final String CALLLOG_NUMBER = "CALLLOG_NUMBER";
    private final String CALLLOG_NAME = "CALLLOG_NAME";
    private final String CALLLOG_IMAGE = "CALLLOG_IMAGE";
    private final String CALLLOG_MISS = "CALLLOG_MISS";
    private final String CALLLOG_DIALED = "CALLLOG_DIALED";
    private final String CALLLOG_RECEIVED = "CALLLOG_RECEIVED";
    private final String CALLLOG_DATE = "CALLLOG_DATE";
    private final String CALLLOG_TIME = "CALLLOG_TIME";


    private final String TABLE_CHAT = "ConnexChatRoom";

    private final String CHAT_SENDER = "CHAT_SENDER";
    private final String CHAT_RECEIVER = "CHAT_RECEIVER";
    private final String CHAT_MESSAGE = "CHAT_MESSAGE";
    private final String CHAT_SUCCESS_STATUS = "CHAT_SUCCESS_STATUS";
    private final String CHAT_DATE_TIME = "CHAT_DATE_TIME";


    //-------------------
    public ConnexDB(Context context) {
        super(context, "androidsqlite.db", null, ConnexConstante.SMART_DB_VERSION);
        this.context = context;
    }

    @Override
    public void onCreate(SQLiteDatabase db) {


        String sqlQueryAlbum = "CREATE TABLE " + TABLE_CONTACTS
                + " (id INTEGER PRIMARY KEY,"
                + CONTACTS_NUMBER + " TEXT,"
                + CONTACTS_NAME + " TEXT,"
                + CONTACTS_DEVICE_NUMBER + " TEXT,"
                + CONTACTS_DBID + " TEXT,"
                + CONTACTS_PROF_IMAGE + " TEXT,"
                + CONTACTS_IS_CONNEX_USER + " TEXT,"
                + CONTACTS_PHONE_ID + " TEXT,"
                + CONTACTS_IS_FAVOURITE + " TEXT,"
                + CONTACTS_IS_SYNKED + " TEXT)";


        String sqlQueryLogs = "CREATE TABLE " + TABLE_CALL_LOGS
                + " (id INTEGER PRIMARY KEY,"
                + CALLLOG_NUMBER + " TEXT,"
                + CALLLOG_NAME + " TEXT,"
                + CALLLOG_IMAGE + " TEXT,"
                + CALLLOG_MISS + " INTEGER,"
                + CALLLOG_DIALED + " INTEGER,"
                + CALLLOG_RECEIVED + " INTEGER,"
                + CALLLOG_DATE + " TEXT,"
                + CALLLOG_TIME + " TEXT)";

        String sqlQueryChat = "CREATE TABLE " + TABLE_CHAT
                + " (id INTEGER PRIMARY KEY,"
                + CHAT_SENDER + " TEXT,"
                + CHAT_RECEIVER + " TEXT,"
                + CHAT_MESSAGE + " TEXT,"
                + CHAT_DATE_TIME + " TEXT,"
                + CHAT_SUCCESS_STATUS + " TEXT)";

        try {
            db.execSQL(sqlQueryAlbum);
            db.execSQL(sqlQueryLogs);
            db.execSQL(sqlQueryChat);
            //------------Putting a default album named "Vault in Vault"
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_CONTACTS);
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_CALL_LOGS);
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_CHAT);
        onCreate(db);
    }


    //=========Main Data Management

    public boolean insertDatatoContacts(final ContactDataType filePath_) {
        SQLiteDatabase db = null;
        boolean flg = false;
        try {
            db = this.getReadableDatabase();
            String selectQuery = "SELECT " + CONTACTS_NUMBER + " FROM " + TABLE_CONTACTS + " WHERE " + CONTACTS_NUMBER + "='" + filePath_.getPhone() + "'";
            SQLiteCursor cursor = (SQLiteCursor) db.rawQuery(selectQuery, null);

            if (cursor.getCount() <= 0) {
                cursor.close();
                db.close();

                db = this.getWritableDatabase();
                ContentValues values = new ContentValues();

                values.put(CONTACTS_DEVICE_NUMBER, filePath_.getOriginalPhoneNumber());
                values.put(CONTACTS_IS_CONNEX_USER, filePath_.getIsConnexUser());
                values.put(CONTACTS_IS_SYNKED, "yes");
                values.put(CONTACTS_NAME, filePath_.getName());
                values.put(CONTACTS_NUMBER, filePath_.getPhone());
                values.put(CONTACTS_PROF_IMAGE, filePath_.getProfImage());
                values.put(CONTACTS_DBID, "-1");
                values.put(CONTACTS_PHONE_ID, filePath_.getPhoneContactID());
                values.put(CONTACTS_IS_FAVOURITE, "no");

                db.insert(TABLE_CONTACTS, null, values);

            } else {
                //Toast.makeText(context, "Already Exist...", Toast.LENGTH_SHORT).show();

                cursor.close();
                db.close();

                db = this.getWritableDatabase();
                ContentValues values = new ContentValues();

                values.put(CONTACTS_DEVICE_NUMBER, filePath_.getOriginalPhoneNumber());
                values.put(CONTACTS_IS_SYNKED, "yes");
                values.put(CONTACTS_NAME, filePath_.getName());
                values.put(CONTACTS_NUMBER, filePath_.getPhone());
                values.put(CONTACTS_PROF_IMAGE, filePath_.getProfImage());
                values.put(CONTACTS_IS_CONNEX_USER, filePath_.getIsConnexUser());
                values.put(CONTACTS_DBID, filePath_.getDbID());
                db.update(TABLE_CONTACTS, values, CONTACTS_NUMBER + "='" + filePath_.getPhone() + "'", null);
            }
            flg = true;
        } catch (Exception e) {
            Log.i(TAG, e.toString());
            flg = false;
        } finally {
            if (db != null) {
                db.close();
            }
        }
        return flg;
    }


    public LinkedList<ContactDataType> fetchAllContacts() {
        LinkedList<ContactDataType> mainData = new LinkedList<ContactDataType>();
        SQLiteDatabase db = null;
        try {
            db = this.getReadableDatabase();
            String selectQuery = "SELECT * FROM " + TABLE_CONTACTS + " ORDER BY " + CONTACTS_NAME + " COLLATE NOCASE";// + " WHERE " + CONTACTS_NUMBER + "='" + filePath_.getPhone() + "'";
            SQLiteCursor cursor = (SQLiteCursor) db.rawQuery(selectQuery, null);
            if (cursor.getCount() > 0) {
                for (int i = 0; i < cursor.getCount(); i++) {
                    cursor.moveToPosition(i);
                    ContactDataType temp_ = new ContactDataType();
                    temp_.setOriginalPhoneNumber(cursor.getString(cursor.getColumnIndex(CONTACTS_DEVICE_NUMBER)));
                    temp_.setPhone(cursor.getString(cursor.getColumnIndex(CONTACTS_NUMBER)));
                    temp_.setProfImage(cursor.getString(cursor.getColumnIndex(CONTACTS_PROF_IMAGE)));
                    temp_.setName(cursor.getString(cursor.getColumnIndex(CONTACTS_NAME)));
                    temp_.setIsConnexUser(cursor.getString(cursor.getColumnIndex(CONTACTS_IS_CONNEX_USER)));
                    temp_.setDbID(cursor.getString(cursor.getColumnIndex(CONTACTS_DBID)));
                    temp_.setPhoneContactID(cursor.getString(cursor.getColumnIndex(CONTACTS_PHONE_ID)));

                    Log.i(TAG, "Fetcher : " + temp_.getPhoneContactID());


                    if (i > 0) {
                        if (!mainData.get(mainData.size() - 1).getName().substring(0, 1).toUpperCase().equals(temp_.getName().substring(0, 1).toUpperCase())) {
                            temp_.setIsHeader(true);
                        } else {
                            temp_.setIsHeader(false);
                        }
                    } else {
                        temp_.setIsHeader(true);
                    }

                    mainData.add(temp_);
                }
            } else {
                //Toast.makeText(context, "No data found...", Toast.LENGTH_SHORT).show();
            }
        } catch (Exception e) {
            Log.i(TAG, e.toString());
        } finally {
            if (db != null) {
                db.close();
            }
        }
        return mainData;
    }

    public LinkedList<ContactDataType> fetchAllContacts(int OFFSET) {
        LinkedList<ContactDataType> mainData = new LinkedList<ContactDataType>();
        SQLiteDatabase db = null;
        try {
            db = this.getReadableDatabase();

            Log.i("OFFSET", "OFFSET : " + OFFSET);
            String selectQuery = "SELECT * FROM " + TABLE_CONTACTS + " ORDER BY " + CONTACTS_NAME + " COLLATE NOCASE";// + " LIMIT 0,500";// OFFSET " + OFFSET;// + " WHERE " + CONTACTS_NUMBER + "='" + filePath_.getPhone() + "'";
            SQLiteCursor cursor = (SQLiteCursor) db.rawQuery(selectQuery, null);
            if (cursor.getCount() > 0) {
                if (OFFSET > 0) {
                    OFFSET--;
                }
                for (int i = OFFSET; i < cursor.getCount(); i++) {
                    cursor.moveToPosition(i);
                    ContactDataType temp_ = new ContactDataType();
                    temp_.setOriginalPhoneNumber(cursor.getString(cursor.getColumnIndex(CONTACTS_DEVICE_NUMBER)));
                    temp_.setPhone(cursor.getString(cursor.getColumnIndex(CONTACTS_NUMBER)));
                    temp_.setProfImage(cursor.getString(cursor.getColumnIndex(CONTACTS_PROF_IMAGE)));
                    temp_.setName(cursor.getString(cursor.getColumnIndex(CONTACTS_NAME)));
                    temp_.setIsConnexUser(cursor.getString(cursor.getColumnIndex(CONTACTS_IS_CONNEX_USER)));
                    temp_.setDbID(cursor.getString(cursor.getColumnIndex(CONTACTS_DBID)));
                    temp_.setPhoneContactID(cursor.getString(cursor.getColumnIndex(CONTACTS_PHONE_ID)));

                    if (i > 0) {
                        if (!mainData.get(mainData.size() - 1).getName().substring(0, 1).toUpperCase().equals(temp_.getName().substring(0, 1).toUpperCase())) {
                            temp_.setIsHeader(true);
                        } else {
                            temp_.setIsHeader(false);
                        }
                    } else {
                        temp_.setIsHeader(true);
                    }

                    mainData.add(temp_);
                }
            } else {
                //Toast.makeText(context, "No data found...", Toast.LENGTH_SHORT).show();
            }
        } catch (Exception e) {
            Log.i(TAG, e.toString());
        } finally {
            if (db != null) {
                db.close();
            }
        }

        Log.i("OFFSET", "Db COUNT : " + mainData.size());

        return mainData;
    }


    public boolean makingSynkNo() {
        SQLiteDatabase db = null;
        boolean flg_ = false;
        try {
            db = this.getWritableDatabase();
            ContentValues values = new ContentValues();
            values.put(CONTACTS_IS_SYNKED, "no");
            db.update(TABLE_CONTACTS, values, null, null);
            flg_ = true;
        } catch (Exception e) {
            Log.i(TAG, e.toString());
        } finally {
            if (db != null) {
                db.close();
            }
        }
        return flg_;
    }


    public boolean favouriteManagement(final String phoneNumber, final String favouriteTag) {
        SQLiteDatabase db = null;
        boolean flg_ = false;
        try {
            db = this.getWritableDatabase();
            ContentValues values = new ContentValues();
            values.put(CONTACTS_IS_FAVOURITE, favouriteTag);
            db.update(TABLE_CONTACTS, values, CONTACTS_PHONE_ID + "='" + phoneNumber + "'", null);
            flg_ = true;
        } catch (Exception e) {
            Log.i(TAG, e.toString());
        } finally {
            if (db != null) {
                db.close();
            }
        }
        return flg_;
    }


//    public boolean makingSynkYes(final String phoneNumber) {
//
//        SQLiteDatabase db = null;
//        boolean flg_ = false;
//        try {
//            db = this.getWritableDatabase();
//            ContentValues values = new ContentValues();
//            values.put(CONTACTS_IS_SYNKED, "yes");
//            db.update(TABLE_CONTACTS, values, CONTACTS_NUMBER + "='" + phoneNumber + "'", null);
//            flg_ = true;
//        } catch (Exception e) {
//            Log.i(TAG, e.toString());
//        } finally {
//            if (db != null) {
//                db.close();
//            }
//        }
//        return flg_;
//    }


    public boolean changingConnexUserStatus(final String phoneNumber, final String connexStatus, final String dbID) {

        SQLiteDatabase db = null;
        boolean flg_ = false;
        try {
            Log.i(TAG, "phoneNumber : " + phoneNumber + " connexStatus : " + connexStatus);
            db = this.getWritableDatabase();
            ContentValues values = new ContentValues();
            values.put(CONTACTS_IS_CONNEX_USER, connexStatus);
            values.put(CONTACTS_DBID, dbID);
            db.update(TABLE_CONTACTS, values, CONTACTS_NUMBER + "='" + phoneNumber + "'", null);
            flg_ = true;
        } catch (Exception e) {
            Log.i(TAG, e.toString());
        } finally {
            if (db != null) {
                db.close();
            }
        }
        return flg_;
    }

    public boolean deleteNonSynkedData() {

        SQLiteDatabase db = null;
        boolean flg_ = false;
        try {
            db = this.getWritableDatabase();
            db.execSQL("DELETE FROM " + TABLE_CONTACTS + " WHERE " + CONTACTS_IS_SYNKED + "='no'");
            flg_ = true;
        } catch (Exception e) {
            Log.i(TAG, e.toString());
        } finally {
            if (db != null) {
                db.close();
            }
        }
        return flg_;
    }


    public LinkedList<ContactDataType> searchData(final String key) {
        LinkedList<ContactDataType> mainData = new LinkedList<ContactDataType>();
        SQLiteDatabase db = null;
        try {
            db = this.getReadableDatabase();

            String selectQuery = "SELECT * FROM " + TABLE_CONTACTS + " WHERE " + CONTACTS_NAME + " LIKE '%" + key + "%' ORDER BY " + CONTACTS_NAME + " COLLATE NOCASE";// + " LIMIT 0,500";// OFFSET " + OFFSET;// + " WHERE " + CONTACTS_NUMBER + "='" + filePath_.getPhone() + "'";
            SQLiteCursor cursor = (SQLiteCursor) db.rawQuery(selectQuery, null);
            if (cursor.getCount() > 0) {
                for (int i = 0; i < cursor.getCount(); i++) {
                    cursor.moveToPosition(i);
                    ContactDataType temp_ = new ContactDataType();
                    temp_.setOriginalPhoneNumber(cursor.getString(cursor.getColumnIndex(CONTACTS_DEVICE_NUMBER)));
                    temp_.setPhone(cursor.getString(cursor.getColumnIndex(CONTACTS_NUMBER)));
                    temp_.setProfImage(cursor.getString(cursor.getColumnIndex(CONTACTS_PROF_IMAGE)));
                    temp_.setName(cursor.getString(cursor.getColumnIndex(CONTACTS_NAME)));
                    temp_.setIsConnexUser(cursor.getString(cursor.getColumnIndex(CONTACTS_IS_CONNEX_USER)));
                    temp_.setDbID(cursor.getString(cursor.getColumnIndex(CONTACTS_DBID)));
                    temp_.setPhoneContactID(cursor.getString(cursor.getColumnIndex(CONTACTS_PHONE_ID)));

                    if (i > 0) {
                        if (!mainData.get(mainData.size() - 1).getName().substring(0, 1).toUpperCase().equals(temp_.getName().substring(0, 1).toUpperCase())) {
                            temp_.setIsHeader(true);
                        } else {
                            temp_.setIsHeader(false);
                        }
                    } else {
                        temp_.setIsHeader(true);
                    }

                    mainData.add(temp_);
                }
            } else {
                //Toast.makeText(context, "No data found...", Toast.LENGTH_SHORT).show();
            }
        } catch (Exception e) {
            Log.i(TAG, e.toString());
        } finally {
            if (db != null) {
                db.close();
            }
        }

        Log.i("OFFSET", "Db COUNT : " + mainData.size());

        return mainData;
    }


    public String getUserImage(final String userPhone) {
        String mainData = "";
        SQLiteDatabase db = null;
        try {
            Log.i(TAG, "Phone : " + userPhone);

            db = this.getReadableDatabase();
            String selectQuery = "SELECT * FROM " + TABLE_CONTACTS + " WHERE " + CONTACTS_NUMBER + "='" + userPhone + "'";// + " WHERE " + CONTACTS_NUMBER + "='" + filePath_.getPhone() + "'";
            SQLiteCursor cursor = (SQLiteCursor) db.rawQuery(selectQuery, null);
            if (cursor.getCount() > 0) {
                cursor.moveToPosition(0);
                mainData = "" + cursor.getString(cursor.getColumnIndex(CONTACTS_PROF_IMAGE));
            }

            if (mainData.equalsIgnoreCase("") || mainData.equalsIgnoreCase("null")) {
                mainData = "http://connex.tech/api/assets/images/connec-bg4.png";
            }

        } catch (Exception e) {
            Log.i(TAG, e.toString());
        } finally {
            if (db != null) {
                db.close();
            }
        }
        return mainData;
    }


    public String getUserNameFromPhone(final String userID) {
        String mainData = "";
        SQLiteDatabase db = null;
        try {
            Log.i(TAG, "userID : " + userID);

            db = this.getReadableDatabase();
            String selectQuery = "SELECT * FROM " + TABLE_CONTACTS + " WHERE " + CONTACTS_NUMBER + "='" + userID + "'";// + " WHERE " + CONTACTS_NUMBER + "='" + filePath_.getPhone() + "'";
            SQLiteCursor cursor = (SQLiteCursor) db.rawQuery(selectQuery, null);
            if (cursor.getCount() > 0) {
                cursor.moveToPosition(0);
                mainData = "" + cursor.getString(cursor.getColumnIndex(CONTACTS_NAME));
            }
        } catch (Exception e) {
            Log.i(TAG, e.toString());
        } finally {
            if (db != null) {
                db.close();
            }
        }

        Log.i(TAG, "mainData : " + mainData);

        return mainData;

    }


    public String getUserDBID(final String userConatctID) {
        String mainData = "";
        SQLiteDatabase db = null;
        try {
            Log.i(TAG, "userConatctID : " + userConatctID);

            db = this.getReadableDatabase();
            String selectQuery = "SELECT " + CONTACTS_DBID + " FROM " + TABLE_CONTACTS + " WHERE " + CONTACTS_NUMBER + "='" + userConatctID + "' AND " + CONTACTS_IS_CONNEX_USER + "='yes'";
            SQLiteCursor cursor = (SQLiteCursor) db.rawQuery(selectQuery, null);
            if (cursor.getCount() > 0) {
                cursor.moveToPosition(0);
                mainData = cursor.getString(cursor.getColumnIndex(CONTACTS_DBID));
            }
        } catch (Exception e) {
            Log.i(TAG, e.toString());
        } finally {
            if (db != null) {
                db.close();
            }
        }
        return mainData;
    }


    public String getUserDBIDFromPhoneDbId(final String userConatctID) {
        String mainData = "";
        SQLiteDatabase db = null;
        try {
            Log.i(TAG, "userConatctID : " + userConatctID);

            db = this.getReadableDatabase();
            String selectQuery = "SELECT " + CONTACTS_DBID + " FROM " + TABLE_CONTACTS + " WHERE " + CONTACTS_PHONE_ID + "='" + userConatctID + "' AND " + CONTACTS_IS_CONNEX_USER + "='yes'";
            SQLiteCursor cursor = (SQLiteCursor) db.rawQuery(selectQuery, null);
            if (cursor.getCount() > 0) {
                cursor.moveToPosition(0);
                mainData = cursor.getString(cursor.getColumnIndex(CONTACTS_DBID));
            }
        } catch (Exception e) {
            Log.i(TAG, e.toString());
        } finally {
            if (db != null) {
                db.close();
            }
        }
        return mainData;
    }

    public String isFavourite(final String userConatctID) {
        String mainData = "";
        SQLiteDatabase db = null;
        try {
            Log.i(TAG, "userConatctID : " + userConatctID);

            db = this.getReadableDatabase();
            String selectQuery = "SELECT " + CONTACTS_IS_FAVOURITE + " FROM " + TABLE_CONTACTS + " WHERE " + CONTACTS_PHONE_ID + "='" + userConatctID + "'";// + "' AND " + CONTACTS_IS_CONNEX_USER + "='yes'";
            SQLiteCursor cursor = (SQLiteCursor) db.rawQuery(selectQuery, null);
            if (cursor.getCount() > 0) {
                cursor.moveToPosition(0);
                mainData = cursor.getString(cursor.getColumnIndex(CONTACTS_IS_FAVOURITE));
            } else {
                Log.i(TAG, "No data found...");
            }
        } catch (Exception e) {
            Log.i(TAG, e.toString());
        } finally {
            if (db != null) {
                db.close();
            }
        }
        return mainData;
    }


    public String getUserImageFromID(final String userID) {
        String mainData = "";
        SQLiteDatabase db = null;
        try {
            Log.i(TAG, "userID : " + userID);

            db = this.getReadableDatabase();
            String selectQuery = "SELECT * FROM " + TABLE_CONTACTS + " WHERE " + CONTACTS_DBID + "='" + userID + "'";// + " WHERE " + CONTACTS_NUMBER + "='" + filePath_.getPhone() + "'";
            SQLiteCursor cursor = (SQLiteCursor) db.rawQuery(selectQuery, null);
            if (cursor.getCount() > 0) {
                cursor.moveToPosition(0);
                mainData = "" + cursor.getString(cursor.getColumnIndex(CONTACTS_PROF_IMAGE));
            }

            if (mainData.equalsIgnoreCase("") || mainData.equalsIgnoreCase("null")) {
                mainData = "http://connex.tech/api/assets/images/connec-bg4.png";
            }

        } catch (Exception e) {
            Log.i(TAG, e.toString());
        } finally {
            if (db != null) {
                db.close();
            }
        }

        Log.i(TAG, "mainData : " + mainData);

        return mainData;
    }

    public String getUserPhoneFromID(final String userID) {
        String mainData = "";
        SQLiteDatabase db = null;
        try {
            Log.i(TAG, "userID : " + userID);

            db = this.getReadableDatabase();
            String selectQuery = "SELECT * FROM " + TABLE_CONTACTS + " WHERE " + CONTACTS_DBID + "='" + userID + "'";// + " WHERE " + CONTACTS_NUMBER + "='" + filePath_.getPhone() + "'";
            SQLiteCursor cursor = (SQLiteCursor) db.rawQuery(selectQuery, null);
            if (cursor.getCount() > 0) {
                cursor.moveToPosition(0);
                mainData = "" + cursor.getString(cursor.getColumnIndex(CONTACTS_NUMBER));
            }
        } catch (Exception e) {
            Log.i(TAG, e.toString());
        } finally {
            if (db != null) {
                db.close();
            }
        }

        Log.i(TAG, "mainData : " + mainData);

        return mainData;
    }


    public String getUserNameFromID(final String userID) {
        String mainData = "";
        SQLiteDatabase db = null;
        try {
            Log.i(TAG, "userID : " + userID);

            db = this.getReadableDatabase();
            String selectQuery = "SELECT * FROM " + TABLE_CONTACTS + " WHERE " + CONTACTS_DBID + "='" + userID + "'";// + " WHERE " + CONTACTS_NUMBER + "='" + filePath_.getPhone() + "'";
            SQLiteCursor cursor = (SQLiteCursor) db.rawQuery(selectQuery, null);
            if (cursor.getCount() > 0) {
                cursor.moveToPosition(0);
                mainData = "" + cursor.getString(cursor.getColumnIndex(CONTACTS_NAME));
            }
        } catch (Exception e) {
            Log.i(TAG, e.toString());
        } finally {
            if (db != null) {
                db.close();
            }
        }

        Log.i(TAG, "mainData : " + mainData);

        return mainData;
    }


    public String getUserNameFromImage(final String userID) {
        String mainData = "";
        SQLiteDatabase db = null;
        try {
            Log.i(TAG, "userID : " + userID);

            db = this.getReadableDatabase();
            String selectQuery = "SELECT * FROM " + TABLE_CONTACTS + " WHERE " + CONTACTS_NUMBER + "='" + userID + "'";// + " WHERE " + CONTACTS_NUMBER + "='" + filePath_.getPhone() + "'";
            SQLiteCursor cursor = (SQLiteCursor) db.rawQuery(selectQuery, null);
            if (cursor.getCount() > 0) {
                cursor.moveToPosition(0);
                mainData = "" + cursor.getString(cursor.getColumnIndex(CONTACTS_NAME));
            }
        } catch (Exception e) {
            Log.i(TAG, e.toString());
        } finally {
            if (db != null) {
                db.close();
            }
        }

        Log.i(TAG, "mainData : " + mainData);

        return mainData;
    }


    //========get favourites

    public LinkedList<ContactDataType> fetchAllFavContacts() {
        LinkedList<ContactDataType> mainData = new LinkedList<ContactDataType>();
        SQLiteDatabase db = null;
        try {
            db = this.getReadableDatabase();
            String selectQuery = "SELECT * FROM " + TABLE_CONTACTS + " WHERE " + CONTACTS_IS_FAVOURITE + "='yes' ORDER BY " + CONTACTS_PHONE_ID;// + " WHERE " + CONTACTS_NUMBER + "='" + filePath_.getPhone() + "'";
            SQLiteCursor cursor = (SQLiteCursor) db.rawQuery(selectQuery, null);
            if (cursor.getCount() > 0) {
                for (int i = 0; i < cursor.getCount(); i++) {
                    cursor.moveToPosition(i);
                    ContactDataType temp_ = new ContactDataType();
                    temp_.setOriginalPhoneNumber(cursor.getString(cursor.getColumnIndex(CONTACTS_DEVICE_NUMBER)));
                    temp_.setPhone(cursor.getString(cursor.getColumnIndex(CONTACTS_NUMBER)));
                    temp_.setProfImage(cursor.getString(cursor.getColumnIndex(CONTACTS_PROF_IMAGE)));
                    temp_.setName(cursor.getString(cursor.getColumnIndex(CONTACTS_NAME)));
                    temp_.setIsConnexUser(cursor.getString(cursor.getColumnIndex(CONTACTS_IS_CONNEX_USER)));
                    temp_.setDbID(cursor.getString(cursor.getColumnIndex(CONTACTS_DBID)));
                    temp_.setPhoneContactID(cursor.getString(cursor.getColumnIndex(CONTACTS_PHONE_ID)));

                    if (mainData.size() > 0) {
                        if (!mainData.get(mainData.size() - 1).getPhoneContactID().equalsIgnoreCase(temp_.getPhoneContactID())) {
                            mainData.add(temp_);
                        }
                    } else {
                        mainData.add(temp_);
                    }

                }
            } else {
                //Toast.makeText(context, "No data found...", Toast.LENGTH_SHORT).show();
            }
        } catch (Exception e) {
            Log.i(TAG, e.toString());
        } finally {
            if (db != null) {
                db.close();
            }
        }
        return mainData;
    }


    public LinkedList<ContactDataType> fetchAllConnexContacts() {
        LinkedList<ContactDataType> mainData = new LinkedList<ContactDataType>();
        SQLiteDatabase db = null;
        try {
            db = this.getReadableDatabase();
            String selectQuery = "SELECT * FROM " + TABLE_CONTACTS + " WHERE " + CONTACTS_IS_CONNEX_USER + "='yes' ORDER BY " + CONTACTS_NAME + " COLLATE NOCASE";// + " WHERE " + CONTACTS_NUMBER + "='" + filePath_.getPhone() + "'";
            SQLiteCursor cursor = (SQLiteCursor) db.rawQuery(selectQuery, null);
            if (cursor.getCount() > 0) {
                for (int i = 0; i < cursor.getCount(); i++) {
                    cursor.moveToPosition(i);
                    ContactDataType temp_ = new ContactDataType();
                    temp_.setOriginalPhoneNumber(cursor.getString(cursor.getColumnIndex(CONTACTS_DEVICE_NUMBER)));
                    temp_.setPhone(cursor.getString(cursor.getColumnIndex(CONTACTS_NUMBER)));
                    temp_.setProfImage(cursor.getString(cursor.getColumnIndex(CONTACTS_PROF_IMAGE)));
                    temp_.setName(cursor.getString(cursor.getColumnIndex(CONTACTS_NAME)));
                    temp_.setIsConnexUser(cursor.getString(cursor.getColumnIndex(CONTACTS_IS_CONNEX_USER)));
                    temp_.setDbID(cursor.getString(cursor.getColumnIndex(CONTACTS_DBID)));
                    temp_.setPhoneContactID(cursor.getString(cursor.getColumnIndex(CONTACTS_PHONE_ID)));
                    mainData.add(temp_);
                    Log.i(TAG, "" + temp_.getName());
                }
            } else {
                Log.i(TAG, "No data...");
            }
        } catch (Exception e) {
            Log.i(TAG, e.toString());
        } finally {
            if (db != null) {
                db.close();
            }
        }
        return mainData;
    }

    //---------------------Inserting Call Logs


    public boolean insertInLogTable(final String phoneNumber, String profName, final String profImage, final String phoneType, final String dateTime) {


        //======managing unknown number

        if (profName.trim().equals("")) {
            profName = phoneNumber;
        }
        //====================

        Log.i(TAG + "CLog", "phoneNumber " + phoneNumber + " profName " + profName + " profImage " + profImage + " phoneType " + phoneType);

        SQLiteDatabase db = null;
        boolean flg = false;
        try {
            db = this.getReadableDatabase();
            String temp[] = dateTime.split("-");

            Log.i(TAG, "phoneNumber " + phoneNumber + " profName " + profName + " profImage " + profImage + " phoneType " + phoneType +
                    " date " + temp[0] + " time " + temp[1]);

            String selectQuery = "SELECT * FROM " + TABLE_CALL_LOGS + " WHERE " + CALLLOG_NUMBER + "='" + phoneNumber + "' AND " + CALLLOG_DATE + "='" + temp[0] + "'";
            SQLiteCursor cursor = (SQLiteCursor) db.rawQuery(selectQuery, null);
            if (cursor.getCount() > 0) {

                cursor.moveToPosition(0);
                int missedCall = cursor.getInt(cursor.getColumnIndex(CALLLOG_MISS));
                int receivedCall = cursor.getInt(cursor.getColumnIndex(CALLLOG_RECEIVED));
                int dialledCall = cursor.getInt(cursor.getColumnIndex(CALLLOG_DIALED));

                Log.i(TAG + "CLog", "Exist missed Call : " + missedCall + " receivedCall : " + receivedCall + " dialledCall : " + dialledCall);

                cursor.close();
                db.close();

                db = this.getWritableDatabase();

                ContentValues values = new ContentValues();


                if (phoneType.equalsIgnoreCase("received")) {
                    values.put(CALLLOG_RECEIVED, receivedCall + 1);
                } else if (phoneType.equalsIgnoreCase("missed")) {
                    values.put(CALLLOG_MISS, missedCall + 1);
                } else {
                    values.put(CALLLOG_DIALED, dialledCall + 1);
                }

                db.update(TABLE_CALL_LOGS, values, CALLLOG_NUMBER + "='" + phoneNumber + "' AND " + CALLLOG_DATE + "='" + temp[0] + "'", null);

            } else {

                Log.i(TAG + "CLog", "New Entry....");
                cursor.close();
                db.close();
                db = this.getWritableDatabase();
                ContentValues values = new ContentValues();
                values.put(CALLLOG_NUMBER, phoneNumber);
                values.put(CALLLOG_NAME, profName);
                values.put(CALLLOG_IMAGE, profImage);

                if (phoneType.equalsIgnoreCase("received")) {
                    values.put(CALLLOG_MISS, 0);
                    values.put(CALLLOG_RECEIVED, 1);
                    values.put(CALLLOG_DIALED, 0);
                } else if (phoneType.equalsIgnoreCase("missed")) {
                    values.put(CALLLOG_MISS, 1);
                    values.put(CALLLOG_RECEIVED, 0);
                    values.put(CALLLOG_DIALED, 0);
                } else {
                    values.put(CALLLOG_MISS, 0);
                    values.put(CALLLOG_RECEIVED, 0);
                    values.put(CALLLOG_DIALED, 1);
                }

                values.put(CALLLOG_DATE, temp[0]);
                values.put(CALLLOG_TIME, temp[1]);

                db.insert(TABLE_CALL_LOGS, null, values);
            }
            flg = true;
        } catch (Exception e) {
            Log.i(TAG, e.toString());
            flg = false;
        } finally {
            if (db != null) {
                db.close();
            }
        }
        return flg;
    }


    public LinkedList<CallLogsDType> fetchCallLogs() {
        LinkedList<CallLogsDType> mainData = new LinkedList<CallLogsDType>();
        SQLiteDatabase db = null;
        try {
            db = this.getReadableDatabase();
            String selectQuery = "SELECT * FROM " + TABLE_CALL_LOGS + " ORDER BY id DESC";
            SQLiteCursor cursor = (SQLiteCursor) db.rawQuery(selectQuery, null);
            if (cursor.getCount() > 0) {
                for (int i = 0; i < cursor.getCount(); i++) {
                    cursor.moveToPosition(i);
                    if (cursor.getString(cursor.getColumnIndex(CALLLOG_NAME)).trim().length() > 0) {
                        CallLogsDType temp_ = new CallLogsDType();

                        temp_.setMissed(cursor.getInt(cursor.getColumnIndex(CALLLOG_MISS)));
                        temp_.setDialed(cursor.getInt(cursor.getColumnIndex(CALLLOG_DIALED)));
                        temp_.setReceived(cursor.getInt(cursor.getColumnIndex(CALLLOG_RECEIVED)));

                        temp_.setDate(cursor.getString(cursor.getColumnIndex(CALLLOG_DATE)));
                        temp_.setTime(cursor.getString(cursor.getColumnIndex(CALLLOG_TIME)));
                        temp_.setPhoneNumber(cursor.getString(cursor.getColumnIndex(CALLLOG_NUMBER)));
                        temp_.setProfName(cursor.getString(cursor.getColumnIndex(CALLLOG_NAME)));
                        temp_.setProfImage(cursor.getString(cursor.getColumnIndex(CALLLOG_IMAGE)));

                        if (mainData.size() == 0) {
                            temp_.setIsHeader(true);
                            SimpleDateFormat frmatr = new SimpleDateFormat("dd MMM,yyyy");
                            Log.i(TAG, temp_.getDate());
                            Log.i(TAG, "Formatted : " + frmatr.format(new Date(Calendar.getInstance().getTimeInMillis())));

//                            if (temp_.getDate().equalsIgnoreCase(frmatr.format(new Date(Calendar.getInstance().getTimeInMillis())))) {
//                                temp_.setDate("Today");
//                            }
                        } else {
                            Log.i(TAG, temp_.getDate());
                            try {
                                if (!temp_.getDate().equalsIgnoreCase(mainData.get(mainData.size() - 1).getDate())) {
                                    temp_.setIsHeader(true);
                                }
                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                        }
                        mainData.add(temp_);
                    }
                }
            }
        } catch (Exception e) {
            Log.i(TAG, e.toString());
        } finally {
            if (db != null) {
                db.close();
            }
        }
        return mainData;
    }


    //----------Chat Management


    public LinkedList<ChatDataModel> fetchAllChatofaUser(final String oponenetID) {
        LinkedList<ChatDataModel> mainData_ = new LinkedList<ChatDataModel>();
        SQLiteDatabase db = null;
        try {
            db = this.getReadableDatabase();
            String selectQuery = "SELECT * FROM " + TABLE_CHAT + " WHERE " + CHAT_SENDER + "='" + oponenetID + "' OR " + CHAT_RECEIVER + "='" + oponenetID + "' ORDER BY id DESC";
            //String selectQuery = "SELECT * FROM " + TABLE_CHAT;// + " WHERE " + CHAT_SENDER + "='" + oponenetID + "' OR " + CHAT_RECEIVER + "='" + oponenetID + "' ORDER BY id DESC";
            SQLiteCursor cursor = (SQLiteCursor) db.rawQuery(selectQuery, null);
            if (cursor.getCount() > 0) {
                for (int i = 0; i < cursor.getCount(); i++) {
                    cursor.moveToPosition(i);

                    ChatDataModel temp_ = new ChatDataModel();

                    temp_.setIsSendingData(false);
                    temp_.setMessage(cursor.getString(cursor.getColumnIndex(CHAT_MESSAGE)));
                    temp_.setReceiverID(cursor.getString(cursor.getColumnIndex(CHAT_RECEIVER)));
                    temp_.setSenderID(cursor.getString(cursor.getColumnIndex(CHAT_SENDER)));
                    temp_.setDateTime(cursor.getString(cursor.getColumnIndex(CHAT_DATE_TIME)));
                    temp_.setLocalDBID(cursor.getInt(cursor.getColumnIndex("id")));


                    Log.i(TAG, "getReceiverID : " + temp_.getReceiverID());
                    Log.i(TAG, "getSenderID : " + temp_.getSenderID());

                    if (cursor.getString(cursor.getColumnIndex(CHAT_SUCCESS_STATUS)).trim().equalsIgnoreCase("true")) {
                        temp_.setIsLoadingFailed(true);
                    } else {
                        temp_.setIsLoadingFailed(false);
                    }

                    mainData_.add(temp_);
                }
            }
        } catch (Exception e) {
            Log.i(TAG, e.toString());
        } finally {
            if (db != null) {
                db.close();
            }
        }

        return mainData_;
    }

    public boolean insertChat(final ChatDataModel temp_) {
        SQLiteDatabase db = null;
        boolean flg = false;
        try {
            db = this.getWritableDatabase();
            ContentValues values = new ContentValues();

            values.put(CHAT_DATE_TIME, temp_.getDateTime());
            values.put(CHAT_MESSAGE, temp_.getMessage());
            values.put(CHAT_RECEIVER, temp_.getReceiverID());
            values.put(CHAT_SENDER, temp_.getSenderID());
            values.put(CHAT_SUCCESS_STATUS, "" + temp_.isSendingData());

            db.insert(TABLE_CHAT, null, values);

            flg = true;
        } catch (Exception e) {
            Log.i(TAG, e.toString());
            flg = false;
        } finally {
            if (db != null) {
                db.close();
            }
        }
        return flg;
    }


    public boolean updateChatMessageStatus(final ChatDataModel temp_) {
        SQLiteDatabase db = null;
        boolean flg_ = false;
        try {
            db = this.getWritableDatabase();
            ContentValues values = new ContentValues();
            values.put(CHAT_SUCCESS_STATUS, "" + temp_.isLoadingFailed());

            db.update(TABLE_CHAT, values, "id=" + temp_.getLocalDBID(), null);

            flg_ = true;
        } catch (Exception e) {
            Log.i(TAG, e.toString());
        } finally {
            if (db != null) {
                db.close();
            }
        }
        return flg_;
    }


    //========

    public String[] getUsermockUpData(final String phoneNumber) {
        String[] temp_ = new String[3];
        SQLiteDatabase db = null;
        try {
            Log.i(TAG, "phoneNumber : " + phoneNumber);

            db = this.getReadableDatabase();
            String selectQuery = "SELECT * FROM " + TABLE_CONTACTS + " WHERE " + CONTACTS_NUMBER + "='" + phoneNumber + "'";// + " WHERE " + CONTACTS_NUMBER + "='" + filePath_.getPhone() + "'";
            SQLiteCursor cursor = (SQLiteCursor) db.rawQuery(selectQuery, null);
            if (cursor.getCount() > 0) {
                cursor.moveToPosition(0);
                Log.i(TAG, "" + cursor.getString(cursor.getColumnIndex(CONTACTS_PHONE_ID)));

                temp_[0] = "" + cursor.getString(cursor.getColumnIndex(CONTACTS_PHONE_ID));
                temp_[1] = "" + cursor.getString(cursor.getColumnIndex(CONTACTS_NAME));
                temp_[2] = "" + cursor.getString(cursor.getColumnIndex(CONTACTS_PROF_IMAGE));
            } else {
                temp_[0] = "";
                temp_[1] = "";
                temp_[2] = "";
            }
        } catch (Exception e) {
            Log.i(TAG, e.toString());
        } finally {
            if (db != null) {
                db.close();
            }
        }

        Log.i(TAG, "mainData : " + temp_.toString());

        return temp_;
    }


    public boolean clearLogData() {
        SQLiteDatabase db = null;
        boolean flg_ = false;
        try {
            db = this.getWritableDatabase();
            db.execSQL("DELETE FROM " + TABLE_CALL_LOGS);
            flg_ = true;
        } catch (Exception e) {
            Log.i(TAG, e.toString());
        } finally {
            if (db != null) {
                db.close();
            }
        }
        return flg_;
    }


}
