package esolz.connexstudent;

import android.graphics.Color;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Window;
import android.view.WindowManager;

import esolz.connexstudent.chatDilaog.ChatMemberList;
import esolz.connexstudent.chatDilaog.ChatRoom;
import esolz.connexstudent.database.ConnexDB;

public class ChatManagement extends AppCompatActivity {


    private FragmentManager fragmentManager;
    private FragmentTransaction fragmentTransaction;
    private ConnexDB cDB = null;
    public static final String ACTION_APP = "ACTION_APP";
    public static final String ACTION_NOTIFICATION = "ACTION_NOTIFICATION";


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_chatmanagement);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle("");
        getSupportActionBar().setHomeAsUpIndicator(R.drawable.ic_action_arrow_left);



        getSupportActionBar().hide();
//        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
//            Window window = getWindow();
//            window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
//            window.setStatusBarColor(Color.parseColor("#1A2E51"));
//        }

        openMemberList();

        if (getIntent().getAction().equals(ACTION_NOTIFICATION)) {

            if (cDB == null) {
                cDB = new ConnexDB(getApplicationContext());
            }

            openChatRoom(getIntent().getStringExtra("OPONENET_ID"),
                    cDB.getUserImageFromID(getIntent().getStringExtra("OPONENET_ID")),
                    cDB.getUserNameFromID(getIntent().getStringExtra("OPONENET_ID")));
        }

    }


    public void openChatRoom(final String oponentID, final String oponentImg, final String oponentName) {
        fragmentManager = getSupportFragmentManager();
        fragmentTransaction = fragmentManager.beginTransaction();
        fragmentTransaction.replace(R.id.chat_bucket, ChatRoom.getInstance(oponentID, oponentImg, oponentName));
        fragmentTransaction.addToBackStack(null);
        fragmentTransaction.commit();
    }


    public void openMemberList() {
        fragmentManager = getSupportFragmentManager();
        fragmentTransaction = fragmentManager.beginTransaction();
        fragmentTransaction.replace(R.id.chat_bucket, new ChatMemberList());
        fragmentTransaction.commit();
    }

}
