package esolz.connexstudent.models;

/**
 * Created by Saikat's Mac on 11/07/16.
 */

public class ChatDataModel {

    private String senderID = "", receiverID = "", message = "", dateTime = "";
    private int localDBID = 0;
    private boolean isSendingData = false;
    private boolean isLoadingFailed = false;


    public int getLocalDBID() {
        return localDBID;
    }

    public void setLocalDBID(int localDBID) {
        this.localDBID = localDBID;
    }

    public String getDateTime() {
        return dateTime;
    }

    public void setDateTime(String dateTime) {
        this.dateTime = dateTime;
    }

    public boolean isSendingData() {
        return isSendingData;
    }

    public void setIsSendingData(boolean isSendingData) {
        this.isSendingData = isSendingData;
    }

    public boolean isLoadingFailed() {
        return isLoadingFailed;
    }

    public void setIsLoadingFailed(boolean isLoadingFailed) {
        this.isLoadingFailed = isLoadingFailed;
    }

    public String getSenderID() {
        return senderID;
    }

    public void setSenderID(String senderID) {
        this.senderID = senderID;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getReceiverID() {
        return receiverID;
    }

    public void setReceiverID(String receiverID) {
        this.receiverID = receiverID;
    }
}
