package esolz.connexstudent.models;

/**
 * Created by Saikat's Mac on 06/07/16.
 */

public class CallLogsDType {

    private String phoneNumber = "", callType = "", date = "", profName = "", profImage = "", time = "";
    private int missed = 0, received = 0, dialed = 0;
    private boolean isHeader = false;
    private String phoneCallBookID = "";


    public String getPhoneCallBookID() {
        return phoneCallBookID;
    }

    public void setPhoneCallBookID(String phoneCallBookID) {
        this.phoneCallBookID = phoneCallBookID;
    }

    public boolean isHeader() {
        return isHeader;
    }

    public void setIsHeader(boolean isHeader) {
        this.isHeader = isHeader;
    }

    public int getMissed() {
        return missed;
    }

    public void setMissed(int missed) {
        this.missed = missed;
    }

    public int getReceived() {
        return received;
    }

    public void setReceived(int received) {
        this.received = received;
    }

    public int getDialed() {
        return dialed;
    }

    public void setDialed(int dialed) {
        this.dialed = dialed;
    }

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }

    public String getProfName() {
        return profName;
    }

    public void setProfName(String profName) {
        this.profName = profName;
    }

    public String getProfImage() {
        return profImage;
    }

    public void setProfImage(String profImage) {
        this.profImage = profImage;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public String getCallType() {
        return callType;
    }

    public void setCallType(String callType) {
        this.callType = callType;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }
}
