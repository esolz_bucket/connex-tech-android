package esolz.connexstudent.models;

/**
 * Created by Saikat's Mac on 22/06/16.
 */

public class ContactDataType {


    private String name, phone, profImage, originalPhoneNumber, isConnexUser = "no", dbID = "", phoneContactID = "", email = "";


    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    private boolean isHeader = false;

    public String getPhoneContactID() {
        return phoneContactID;
    }

    public void setPhoneContactID(String phoneContactID) {
        this.phoneContactID = phoneContactID;
    }

    public boolean isHeader() {
        return isHeader;
    }

    public void setIsHeader(boolean isHeader) {
        this.isHeader = isHeader;
    }

    public String getDbID() {
        return dbID;
    }

    public void setDbID(String dbID) {
        this.dbID = dbID;
    }

    public String getIsConnexUser() {
        return isConnexUser;
    }

    public void setIsConnexUser(String isConnexUser) {
        this.isConnexUser = isConnexUser;
    }

    public String getOriginalPhoneNumber() {
        return originalPhoneNumber;
    }

    public void setOriginalPhoneNumber(String originalPhoneNumber) {
        this.originalPhoneNumber = originalPhoneNumber;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getProfImage() {
        return profImage;
    }

    public void setProfImage(String profImage) {
        this.profImage = profImage;
    }
}
