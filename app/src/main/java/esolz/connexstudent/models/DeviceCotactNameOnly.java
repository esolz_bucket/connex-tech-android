package esolz.connexstudent.models;

/**
 * Created by Saikat's Mac on 28/07/16.
 */

public class DeviceCotactNameOnly {


    private int contactID = 0;
    private String contactName = "", contactImage = "", isConnexUser = "";
    private boolean isHeader = false;

    public String getIsConnexUser() {
        return isConnexUser;
    }

    public void setIsConnexUser(String isConnexUser) {
        this.isConnexUser = isConnexUser;
    }

    public boolean isHeader() {
        return isHeader;
    }

    public void setIsHeader(boolean isHeader) {
        this.isHeader = isHeader;
    }

    public int getContactID() {
        return contactID;
    }

    public void setContactID(int contactID) {
        this.contactID = contactID;
    }

    public String getContactName() {
        return contactName;
    }

    public void setContactName(String contactName) {
        this.contactName = contactName;
    }

    public String getContactImage() {
        return contactImage;
    }

    public void setContactImage(String contactImage) {
        this.contactImage = contactImage;
    }
}
