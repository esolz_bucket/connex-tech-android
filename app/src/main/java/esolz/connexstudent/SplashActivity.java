package esolz.connexstudent;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.os.Handler;
import android.preference.PreferenceManager;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.widget.Toast;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GoogleApiAvailability;

import esolz.connexstudent.ConnexService.ContactFetchingService;
import esolz.connexstudent.GCMPushManagement.QuickstartPreferences;
import esolz.connexstudent.GCMPushManagement.RegistrationIntentService;
import esolz.connexstudent.appliaction.ConnexApplication;


public class SplashActivity extends AppCompatActivity {


    //========GCM Management Code

    private static final int PLAY_SERVICES_RESOLUTION_REQUEST = 9000;
    private boolean isReceiverRegistered;
    private BroadcastReceiver mRegistrationBroadcastReceiver = null;
    private final String TAG = "SplashActivity";
    final int MY_PERMISSIONS_REQUEST_READ_CONTACTS = 1;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);



        mRegistrationBroadcastReceiver = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                SharedPreferences sharedPreferences =
                        PreferenceManager.getDefaultSharedPreferences(context);
                boolean sentToken = sharedPreferences
                        .getBoolean(QuickstartPreferences.SENT_TOKEN_TO_SERVER, false);

                Log.i("GCM_TOKEN", sharedPreferences.getString("GCM_TOKEN", ""));

                ConnexApplication.getInstance().setGCM_TOKEN(sharedPreferences.getString("GCM_TOKEN", ""));
                if (sentToken) {
                    permissionChk();
                } else {
                    Toast.makeText(SplashActivity.this, "GCM Registration failure.", Toast.LENGTH_SHORT).show();
                }
            }
        };


        //==========================
        registerReceiver();






        if (checkPlayServices()) {
            Intent intent = new Intent(this, RegistrationIntentService.class);
            startService(intent);
        }

    }


    private boolean checkPlayServices() {
        GoogleApiAvailability apiAvailability = GoogleApiAvailability.getInstance();
        int resultCode = apiAvailability.isGooglePlayServicesAvailable(this);
        if (resultCode != ConnectionResult.SUCCESS) {
            if (apiAvailability.isUserResolvableError(resultCode)) {
                apiAvailability.getErrorDialog(this, resultCode, PLAY_SERVICES_RESOLUTION_REQUEST)
                        .show();
            } else {
                Log.i(TAG, "This device is not supported.");
                finish();
            }
            return false;
        }
        return true;
    }


    private void registerReceiver() {
        if (!isReceiverRegistered) {
            LocalBroadcastManager.getInstance(this).registerReceiver(mRegistrationBroadcastReceiver,
                    new IntentFilter(QuickstartPreferences.REGISTRATION_COMPLETE));
            isReceiverRegistered = true;
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        registerReceiver();
    }

    @Override
    protected void onPause() {
        LocalBroadcastManager.getInstance(this).unregisterReceiver(mRegistrationBroadcastReceiver);
        isReceiverRegistered = false;
        super.onPause();
    }


    //==========Contact fetching Portion


//    private class FetchPhoneNumber extends AsyncTask<Void, Void, Void> {
//
//        @Override
//        protected Void doInBackground(Void... params) {
//
//
//            Uri contactsUri = ContactsContract.CommonDataKinds.Phone.CONTENT_URI;
//            Cursor contactsCursor = getContentResolver().query(contactsUri,
//                    new String[]{ContactsContract.CommonDataKinds.Phone.CONTACT_ID}, null, null,
//                    ContactsContract.CommonDataKinds.Phone.DISPLAY_NAME);
//
//
//            if (contactsCursor.getCount() > 0) {
//
//
//                int contactIdIndex = contactsCursor.getColumnIndex(ContactsContract.CommonDataKinds.Phone.CONTACT_ID);
//
//
//                for (int i = 0; i < contactsCursor.getCount(); i++) {
//                    contactsCursor.moveToPosition(i);
//
//
//                    Uri dataUri = ContactsContract.CommonDataKinds.Phone.CONTENT_URI;
//
//                    Cursor dataCursor = getContentResolver().query(
//                            dataUri,
//                            new String[]{ContactsContract.CommonDataKinds.Phone.NUMBER, ContactsContract.CommonDataKinds.Phone.DISPLAY_NAME, ContactsContract.CommonDataKinds.Phone.PHOTO_URI},
//                            ContactsContract.Data.CONTACT_ID + "=" + contactsCursor.getLong(contactIdIndex),
//                            null, null);
//
//
//                    int contactNameIndex = dataCursor.getColumnIndex(ContactsContract.CommonDataKinds.Phone.DISPLAY_NAME);
//                    int contactPhoneIndex = dataCursor.getColumnIndex(ContactsContract.CommonDataKinds.Phone.NUMBER);
//                    int contactPhotoIndex = dataCursor.getColumnIndex(ContactsContract.CommonDataKinds.Phone.PHOTO_URI);
//
//
//                    dataCursor.moveToLast();
//
//
//                    String phoneNumber = "";
//
//                    ContactDataType temp_ = new ContactDataType();
//
//                    temp_.setName(dataCursor.getString(contactNameIndex));
//                    temp_.setProfImage(dataCursor.getString(contactPhotoIndex));
//
//                    phoneNumber = dataCursor.getString(contactPhoneIndex);
//                    temp_.setOriginalPhoneNumber(phoneNumber);
//                    phoneNumber = phoneNumber.replaceAll("\\s", "");// **********removing
//                    phoneNumber = phoneNumber.replaceAll(" ", ""); // all
//                    phoneNumber = phoneNumber.replaceAll("[(.^)]*", "");
//                    phoneNumber = phoneNumber.replaceAll("-", "");
////                  phoneNumber = phoneNumber.replaceAll("\\+", "");
//                    phoneNumber = phoneNumber.replaceAll("\\,", "");
//                    phoneNumber = phoneNumber.replaceAll("  ", "");
//                    phoneNumber = phoneNumber.trim();
//
//                    if (phoneNumber.length() >= 10) {
//                        phoneNumber = phoneNumber.substring((phoneNumber.length() - 10), phoneNumber.length());
//                        temp_.setPhone(phoneNumber);
//                        ConnexApplication.getInstance().getMainContactsData().add(temp_);
//                        ConnexApplication.getInstance().getMapIndex().put(temp_.getName().substring(0, 1).toUpperCase(), ConnexApplication.getInstance().getMainContactsData().indexOf(temp_));
//                    }
//
//                    dataCursor.close();
//                }
//                contactsCursor.close();
//            }
//
//
//            return null;
//        }
//
//        @Override
//        protected void onPostExecute(Void result) {
//
//            if (ConnexApplication.getInstance().getUserID().equals("")) {
//
//                new Handler().postDelayed(new Runnable() {
//                    @Override
//                    public void run() {
//                        Intent i = new Intent(SplashActivity.this, LoginActivity.class);
//                        startActivity(i);
//                        finish();
//                    }
//                }, 2000);
//
//            } else {
//                Intent i = new Intent(SplashActivity.this, LandingActivity.class);
//                startActivity(i);
//                finish();
//            }
//
//        }
//
//    }


    public void permissionChk() {
        if (ActivityCompat.checkSelfPermission(this, android.Manifest.permission.READ_CONTACTS) != PackageManager.PERMISSION_GRANTED ||
                ActivityCompat.checkSelfPermission(this, android.Manifest.permission.WRITE_CONTACTS) != PackageManager.PERMISSION_GRANTED) {
            // TODO: Consider calling
            if (ActivityCompat.shouldShowRequestPermissionRationale(this, android.Manifest.permission.READ_CONTACTS) &&
                    ActivityCompat.shouldShowRequestPermissionRationale(this, android.Manifest.permission.WRITE_CONTACTS) ) {
                ActivityCompat.requestPermissions(this,
                        new String[]{android.Manifest.permission.READ_CONTACTS,
                                android.Manifest.permission.WRITE_CONTACTS},
                        MY_PERMISSIONS_REQUEST_READ_CONTACTS);
            } else {
                // No explanation needed, we can request the permission.
                ActivityCompat.requestPermissions(this,
                        new String[]{android.Manifest.permission.READ_CONTACTS,
                                android.Manifest.permission.WRITE_CONTACTS},
                        MY_PERMISSIONS_REQUEST_READ_CONTACTS);
            }
            return;
        } else {


            Intent i = new Intent(getApplicationContext(), ContactFetchingService.class);
            i.setAction(ContactFetchingService.ACTION_ON);
            startService(i);

            if (ConnexApplication.getInstance().getUserID().equals("")) {

                setContentView(R.layout.activity_splash);
                Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
                setSupportActionBar(toolbar);
                getSupportActionBar().hide();


                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        Intent ii = new Intent(SplashActivity.this, TutorialActivity.class);
                        startActivity(ii);
                        finish();
                    }
                }, 2000);
            } else {
                Intent ii = new Intent(SplashActivity.this, LandingPagev2.class);
                //ii.setAction("none");
                startActivity(ii);
                finish();
            }

        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String permissions[], int[] grantResults) {
        switch (requestCode) {
            case MY_PERMISSIONS_REQUEST_READ_CONTACTS: {
                if (grantResults.length > 1
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED &&
                        grantResults[1] == PackageManager.PERMISSION_GRANTED) {

                    Intent i = new Intent(getApplicationContext(), ContactFetchingService.class);
                    i.setAction(ContactFetchingService.ACTION_ON);
                    startService(i);


                    if (ConnexApplication.getInstance().getUserID().equals("")) {


                        setContentView(R.layout.activity_splash);
                        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
                        setSupportActionBar(toolbar);
                        getSupportActionBar().hide();

                        new Handler().postDelayed(new Runnable() {
                            @Override
                            public void run() {
                                Intent ii = new Intent(SplashActivity.this, TutorialActivity.class);
                                startActivity(ii);
                                finish();
                            }
                        }, 2000);

                    } else {
                        Intent ii = new Intent(SplashActivity.this, LandingPagev2.class);
                        //ii.setAction("none");
                        startActivity(ii);
                        finish();
                    }


                } else {
                    android.support.v7.app.AlertDialog.Builder alertDialogBuilder = new android.support.v7.app.AlertDialog.Builder(SplashActivity.this);
                    alertDialogBuilder.setMessage("With out these permissions, we cant fetch your device contact list.");
                    alertDialogBuilder.setPositiveButton("Retry", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface arg0, int arg1) {
                            permissionChk();
                        }
                    });
                    alertDialogBuilder.setNegativeButton("DISCARD", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            finish();
                        }
                    });
                    android.support.v7.app.AlertDialog alertDialog = alertDialogBuilder.create();
                    alertDialog.show();
                }
                return;
            }
        }
    }


}
