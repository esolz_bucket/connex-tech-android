package esolz.connexstudent.adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.squareup.picasso.Picasso;

import java.util.LinkedList;

import esolz.connexstudent.R;
import esolz.connexstudent.customview.AvenirRegular;
import esolz.connexstudent.customview.CirculerTransformation;
import esolz.connexstudent.models.DeviceCotactNameOnly;

/**
 * Created by apple on 28/07/16.
 */

public class ContactNameAdapter extends RecyclerView.Adapter<ContactNameAdapter.ViewHolder> {

    private LinkedList<DeviceCotactNameOnly> mainArray;
    private LinkedList<DeviceCotactNameOnly> mainArraySrch;
    private Context mContext;
    private LayoutInflater inflater = null;
    private onItemClickListener callback = null;


    public ContactNameAdapter(Context mContext, LinkedList<DeviceCotactNameOnly> mainArray) {
        super();
        this.mainArray = mainArray;
        this.mainArraySrch = new LinkedList<>();
        mainArraySrch.addAll(mainArray);
        this.mContext = mContext;
        inflater = LayoutInflater.from(mContext);
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        if (viewType == 1) {
            return new ViewHolder(inflater.inflate(R.layout.items_contact_row_header, parent, false), viewType);
        } else {
            return new ViewHolder(inflater.inflate(R.layout.items_contacs_row, parent, false), viewType);
        }
    }


    @Override
    public void onBindViewHolder(ViewHolder holder, final int position) {

        try {
            if (mainArray.get(position).getContactImage() != null) {
                Picasso.with(mContext).load(mainArray.get(position).getContactImage()).transform(new CirculerTransformation()).fit().centerCrop().into(holder.profileImage);
            } else {
                Picasso.with(mContext).load("http://static.marblehead.uk.com/wp-content/uploads/2015/10/Placeholder.png").transform(new CirculerTransformation()).fit().centerCrop().into(holder.profileImage);
            }

            holder.profileName.setText(mainArray.get(position).getContactName());


            if (mainArray.get(position).isHeader()) {
                holder.indexerHeader.setText(mainArray.get(position).getContactName().substring(0, 1).toUpperCase());
            }


            if (mainArray.get(position).getIsConnexUser().equals("")) {
                holder.connexUser.setVisibility(View.GONE);
            } else {
                holder.connexUser.setVisibility(View.VISIBLE);
            }

            holder.mainView.setOnClickListener(new View.OnClickListener() {

                @Override
                public void onClick(View v) {
                    if (callback != null) {
                        callback.onItemClickd(mainArray.get(position));
                    }
                }
            });

        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    @Override
    public int getItemCount() {
        return mainArray.size();
    }


    public static class ViewHolder extends RecyclerView.ViewHolder {

        ImageView profileImage, connexUser;
        AvenirRegular profileName, indexerHeader;
        View mainView;

        public ViewHolder(View itemView, int type) {
            super(itemView);
            mainView = itemView;
            connexUser = (ImageView) itemView.findViewById(R.id.connex_user);
            profileImage = (ImageView) itemView.findViewById(R.id.profile_image);
            profileName = (AvenirRegular) itemView.findViewById(R.id.prof_name);
            if (type == 1) {
                indexerHeader = (AvenirRegular) itemView.findViewById(R.id.indexer);
            }
        }
    }


    //=========onItemClickManagement


    public void setOnItemClickListener(onItemClickListener callback) {
        this.callback = callback;

    }

    public interface onItemClickListener {
        void onItemClickd(DeviceCotactNameOnly data);
    }


    @Override
    public int getItemViewType(int position) {
        if (mainArray.get(position).isHeader()) {
            return 1;
        } else {
            return 0;
        }
//        return super.getItemViewType(position);
    }


    public void updateData(final int index) {

        try {
            Log.i("index", "Change Data: " + index);
            mainArray.get(index).setIsConnexUser("yes");
            notifyDataSetChanged();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    //==============


    public void filter(String charText) {

        mainArray.clear();
        if (charText.length() == 0) {
            mainArray.addAll(mainArraySrch);
        } else {
            for (DeviceCotactNameOnly userListingDataType : mainArraySrch) {
                if (userListingDataType.getContactName().toLowerCase().contains(charText.toLowerCase())) {
                    mainArray.add(userListingDataType);
                }
            }
        }

        notifyDataSetChanged();
    }


}