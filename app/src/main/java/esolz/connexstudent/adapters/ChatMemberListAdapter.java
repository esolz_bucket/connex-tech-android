package esolz.connexstudent.adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.squareup.picasso.Picasso;

import java.util.LinkedList;

import esolz.connexstudent.R;
import esolz.connexstudent.customview.AvenirRegular;
import esolz.connexstudent.customview.CirculerTransformation;
import esolz.connexstudent.models.ContactDataType;

/**
 * Created by Saikat's Mac on 11/07/16.
 */

public class ChatMemberListAdapter extends RecyclerView.Adapter<ChatMemberListAdapter.ViewHolder> {

    private LinkedList<ContactDataType> mainArray;
    private Context mContext;
    private LayoutInflater inflater = null;
    private onItemClickListener callback = null;

    public ChatMemberListAdapter(Context mContext, LinkedList<ContactDataType> mainArray) {
        super();
        this.mainArray = mainArray;
        this.mContext = mContext;
        inflater = LayoutInflater.from(mContext);
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = inflater.inflate(R.layout.items_contacs_row, parent, false);
//        view.setLayoutParams(new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, rowHight));
        return new ViewHolder(view);
    }


    @Override
    public void onBindViewHolder(ViewHolder holder, final int position) {

        try {
            if (mainArray.get(position).getProfImage() != null) {
                Picasso.with(mContext).load(mainArray.get(position).getProfImage()).transform(new CirculerTransformation()).fit().centerCrop().into(holder.profileImage);
            } else {
                Picasso.with(mContext).load("http://iphonewalls.net/wp-content/uploads/2014/02/Gradient%20Honeycomb%20Pattern%20iPhone%205%20Wallpaper.jpg").transform(new CirculerTransformation()).fit().centerCrop().into(holder.profileImage);
            }

            holder.profileName.setText(mainArray.get(position).getName());


            holder.selectionView.setOnClickListener(new View.OnClickListener() {

                @Override
                public void onClick(View v) {
                    callback.onDetailsClickd(mainArray.get(position));
                }
            });

        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    @Override
    public int getItemCount() {
        return mainArray.size();
    }


    public static class ViewHolder extends RecyclerView.ViewHolder {

        ImageView profileImage;
        AvenirRegular profileName;
        View selectionView;

        public ViewHolder(View itemView) {
            super(itemView);
            selectionView = itemView;
            profileImage = (ImageView) itemView.findViewById(R.id.profile_image);
            profileName = (AvenirRegular) itemView.findViewById(R.id.prof_name);
        }
    }


    public void setOnItemClickListener(onItemClickListener callback) {
        this.callback = callback;

    }

    public interface onItemClickListener {
        void onDetailsClickd(ContactDataType data);
    }

}
