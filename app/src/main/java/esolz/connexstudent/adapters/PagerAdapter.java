package esolz.connexstudent.adapters;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.squareup.picasso.Picasso;

import esolz.connexstudent.R;


/**
 * Created by apple on 05/05/16.
 */

public class PagerAdapter extends android.support.v4.view.PagerAdapter {


    int[] images;
    Context mContext = null;

    public PagerAdapter(Context mContext, int[] images) {
        super();
        this.images = images;
        this.mContext = mContext;
    }

    @Override
    public int getCount() {
        return images.length;
    }

    @Override
    public boolean isViewFromObject(View view, Object object) {
        return view == object;
    }

    @Override
    public Object instantiateItem(ViewGroup container, int position) {
        View view = LayoutInflater.from(mContext).inflate(R.layout.items_pager_subjects, null);
        Picasso.with(mContext).load(images[position]).fit().into((ImageView) view.findViewById(R.id.pager_image));
        container.addView(view);
        return view;
    }

    @Override
    public void destroyItem(ViewGroup container, int position, Object object) {
        container.removeView((ViewGroup) object);
    }


}