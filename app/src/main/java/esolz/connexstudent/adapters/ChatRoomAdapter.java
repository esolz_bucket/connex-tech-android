package esolz.connexstudent.adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.LinkedList;

import esolz.connexstudent.R;
import esolz.connexstudent.appliaction.ConnexApplication;
import esolz.connexstudent.criptography.SecurityManagement;
import esolz.connexstudent.customview.AvenirLight;
import esolz.connexstudent.customview.AvenirRoman;
import esolz.connexstudent.models.ChatDataModel;

/**
 * Created by Saikat's Mac on 11/07/16.
 */

public class ChatRoomAdapter extends RecyclerView.Adapter<ChatRoomAdapter.ViewHolder> {

    private LinkedList<ChatDataModel> mainArray;
    private Context mContext;
    private LayoutInflater inflater = null;
    private final String key = "keliyebrindabondekhiyedobo";

    public ChatRoomAdapter(Context mContext, LinkedList<ChatDataModel> mainArray) {
        super();
        this.mainArray = mainArray;
        this.mContext = mContext;
        inflater = LayoutInflater.from(mContext);
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        if (viewType == 1) {
            return new ViewHolder(inflater.inflate(R.layout.items_chat_me, parent, false), viewType);
        } else {
            return new ViewHolder(inflater.inflate(R.layout.items_chat_you, parent, false), viewType);
        }
    }


    @Override
    public int getItemViewType(int position) {

        if (mainArray.get(position).getSenderID().equalsIgnoreCase(ConnexApplication.getInstance().getUserID())) {
            return 1;
        } else {
            return 0;
        }

    }

    @Override
    public void onBindViewHolder(ViewHolder holder, final int position) {
        try {
            holder.message.setText("" + SecurityManagement.decipher(key, mainArray.get(position).getMessage()));
            holder.dateHeader.setText("" + mainArray.get(position).getDateTime());
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public int getItemCount() {
        return mainArray.size();
    }


    public static class ViewHolder extends RecyclerView.ViewHolder {

        AvenirRoman message;
        AvenirLight dateHeader;

        public ViewHolder(View itemView, int viewType) {
            super(itemView);
            dateHeader = (AvenirLight) itemView.findViewById(R.id.date_header);
            message = (AvenirRoman) itemView.findViewById(R.id.message);
        }
    }

    public void chageDataSet(LinkedList<ChatDataModel> mainArray) {
        this.mainArray = mainArray;
        notifyDataSetChanged();
    }

    public void addData(final ChatDataModel temp_) {
        this.mainArray.add(0, temp_);
        notifyDataSetChanged();
    }

}
