package esolz.connexstudent.adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.squareup.picasso.Picasso;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.LinkedList;

import esolz.connexstudent.R;
import esolz.connexstudent.customview.AvenirRegular;
import esolz.connexstudent.customview.AvenirRoman;
import esolz.connexstudent.customview.CirculerTransformation;
import esolz.connexstudent.models.CallLogsDType;

/**
 * Created by Saikat's Mac on 07/07/16.
 */

public class CallLogsAdapter extends RecyclerView.Adapter<CallLogsAdapter.ViewHolder> {

    private LinkedList<CallLogsDType> mainArray;
    private Context mContext;
    private LayoutInflater inflater = null;
    private onItemClickListener callback = null;
    SimpleDateFormat frmatr = null;


    public CallLogsAdapter(Context mContext, LinkedList<CallLogsDType> mainArray, onItemClickListener callback) {
        super();
        this.mainArray = mainArray;
        this.mContext = mContext;
        this.callback = callback;
        inflater = LayoutInflater.from(mContext);
        frmatr = new SimpleDateFormat("dd MMM,yyyy");
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        if (viewType == 0) {
            return new ViewHolder(inflater.inflate(R.layout.items_call_log, parent, false), viewType);
        } else {
            return new ViewHolder(inflater.inflate(R.layout.items_call_log_withheader, parent, false), viewType);
        }
    }


    @Override
    public int getItemViewType(int position) {

        if (mainArray.get(position).isHeader()) {
            return 1;
        } else {
            return 0;
        }

    }

    @Override
    public void onBindViewHolder(ViewHolder holder, final int position) {

        try {
            if (mainArray.get(position).getProfImage() != null) {
                Picasso.with(mContext).load(mainArray.get(position).getProfImage()).transform(new CirculerTransformation()).fit().centerCrop().into(holder.profileImage);
            } else {
                Picasso.with(mContext).load("http://sunfieldfarm.org/wp-content/uploads/2014/02/profile-placeholder.png").transform(new CirculerTransformation()).fit().centerCrop().into(holder.profileImage);
            }

            holder.profileName.setText(mainArray.get(position).getProfName());

            holder.missed.setText("" + mainArray.get(position).getMissed());
            holder.dialed.setText("" + mainArray.get(position).getDialed());
            holder.received.setText("" + mainArray.get(position).getReceived());

            if (mainArray.get(position).isHeader()) {
                if (mainArray.get(position).getDate().equalsIgnoreCase(frmatr.format(new Date(Calendar.getInstance().getTimeInMillis())))) {
                    holder.dateHeader.setText("Today");
                }else{
                    holder.dateHeader.setText(mainArray.get(position).getDate());
                }
            }


            holder.mainView.setOnClickListener(new View.OnClickListener() {

                @Override
                public void onClick(View v) {
                    callback.onItemClicked(mainArray.get(position).getPhoneNumber());
                }
            });


        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public int getItemCount() {
        return mainArray.size();
    }


    public static class ViewHolder extends RecyclerView.ViewHolder {

        ImageView profileImage;
        AvenirRoman missed, dialed, received;
        AvenirRegular profileName, dateHeader;
        View mainView = null;

        public ViewHolder(View itemView, int viewType) {
            super(itemView);
            this.mainView = itemView;
            profileImage = (ImageView) itemView.findViewById(R.id.prof_image);

            profileName = (AvenirRegular) itemView.findViewById(R.id.prof_name);

            missed = (AvenirRoman) itemView.findViewById(R.id.text_missed);
            dialed = (AvenirRoman) itemView.findViewById(R.id.text_dialed);
            received = (AvenirRoman) itemView.findViewById(R.id.text_received);

            if (viewType == 1) {
                dateHeader = (AvenirRegular) itemView.findViewById(R.id.header_txt);
            }

        }
    }


    //======================


    public interface onItemClickListener {
        void onItemClicked(final String phoneNumber);
    }


}
