package esolz.connexstudent.adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.squareup.picasso.Picasso;

import java.util.LinkedList;

import esolz.connexstudent.LandingPagev2;
import esolz.connexstudent.R;
import esolz.connexstudent.customview.AvenirRegular;
import esolz.connexstudent.customview.CirculerTransformation;
import esolz.connexstudent.models.ContactDataType;

/**
 * Created by Saikat's Mac on 22/06/16.
 */


public class ContactsAdapter extends RecyclerView.Adapter<ContactsAdapter.ViewHolder> {

    private LinkedList<ContactDataType> mainArray;
    private Context mContext;
    private LayoutInflater inflater = null;
    private onItemClickListener callback = null;


    public ContactsAdapter(Context mContext, LinkedList<ContactDataType> mainArray) {
        super();
        this.mainArray = mainArray;
        this.mContext = mContext;
        inflater = LayoutInflater.from(mContext);
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        if (viewType == 1) {
            return new ViewHolder(inflater.inflate(R.layout.items_contact_row_header, parent, false), viewType);
        } else {
            return new ViewHolder(inflater.inflate(R.layout.items_contacs_row, parent, false), viewType);
        }
    }


    @Override
    public void onBindViewHolder(ViewHolder holder, final int position) {

        try {
            if (mainArray.get(position).getProfImage() != null) {
                Picasso.with(mContext).load(mainArray.get(position).getProfImage()).transform(new CirculerTransformation()).fit().centerCrop().into(holder.profileImage);
            } else {
                Picasso.with(mContext).load("http://static.marblehead.uk.com/wp-content/uploads/2015/10/Placeholder.png").transform(new CirculerTransformation()).fit().centerCrop().into(holder.profileImage);
            }

            holder.profileName.setText(mainArray.get(position).getName());


            if (mainArray.get(position).isHeader()) {
                holder.indexerHeader.setText(mainArray.get(position).getName().substring(0, 1).toUpperCase());
            }


            if (mainArray.get(position).getIsConnexUser().equals("yes")) {
                holder.connexUser.setVisibility(View.VISIBLE);
            } else {
                holder.connexUser.setVisibility(View.GONE);
            }

            holder.mainView.setOnClickListener(new View.OnClickListener() {

                @Override
                public void onClick(View v) {

                    try {
                        ((LandingPagev2) mContext).fireFromSearch(mainArray.get(position).getPhoneContactID(), mainArray.get(position).getProfImage(), mainArray.get(position).getName());
                    } catch (Exception e) {
                        e.printStackTrace();
                    }

                }
            });

        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    @Override
    public int getItemCount() {
        return mainArray.size();
    }

//    @Override
//    public HashMap<String, Integer> getMapIndex() {
//        return mMapIndex;
//    }

    public static class ViewHolder extends RecyclerView.ViewHolder {

        ImageView profileImage, connexUser;
        AvenirRegular profileName, indexerHeader;
        View mainView;

        public ViewHolder(View itemView, int type) {
            super(itemView);
            mainView = itemView;
            connexUser = (ImageView) itemView.findViewById(R.id.connex_user);
            profileImage = (ImageView) itemView.findViewById(R.id.profile_image);
            profileName = (AvenirRegular) itemView.findViewById(R.id.prof_name);
            if (type == 1) {
                indexerHeader = (AvenirRegular) itemView.findViewById(R.id.indexer);
            }
        }
    }


    public void managedataSet(LinkedList<ContactDataType> mainArraytemp_) {
        mainArray = mainArraytemp_;
        Log.i("mainArray", " onReceive Landing " + mainArray.size());

        notifyDataSetChanged();
    }


    //=========onItemClickManagement


    public void setOnItemClickListener(onItemClickListener callback) {
        this.callback = callback;

    }

    public interface onItemClickListener {
        void onItemClickd(ContactDataType data);
    }


    @Override
    public int getItemViewType(int position) {
        if (mainArray.get(position).isHeader()) {
            return 1;
        } else {
            return 0;
        }
//        return super.getItemViewType(position);
    }
}
