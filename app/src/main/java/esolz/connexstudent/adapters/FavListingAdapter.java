package esolz.connexstudent.adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.AlphaAnimation;
import android.widget.ImageView;
import android.widget.LinearLayout;

import com.squareup.picasso.Picasso;

import java.util.LinkedList;

import esolz.connexstudent.R;
import esolz.connexstudent.customview.AvenirLight;
import esolz.connexstudent.customview.AvenirRoman;
import esolz.connexstudent.models.ContactDataType;

/**
 * Created by Saikat's Mac on 06/07/16.
 */

public class FavListingAdapter extends RecyclerView.Adapter<FavListingAdapter.ViewHolder> {

    private LinkedList<ContactDataType> mainArray;
    private Context mContext;
    private LayoutInflater inflater = null;
    private onItemClickListener callback = null;
    private int rowHight = 0;
    private ContactDataType currentSeletced = null;

    public FavListingAdapter(Context mContext, LinkedList<ContactDataType> mainArray, float screenWidth) {
        super();
        this.mainArray = mainArray;
        this.mContext = mContext;
        rowHight = rowHight(screenWidth, 2.0f);
        inflater = LayoutInflater.from(mContext);
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = inflater.inflate(R.layout.items_fav_listing, parent, false);
        view.setLayoutParams(new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, rowHight));
        return new ViewHolder(view);
    }


    @Override
    public void onBindViewHolder(ViewHolder holder, final int position) {

        try {
            if (mainArray.get(position).getProfImage() != null) {
                Picasso.with(mContext).load(mainArray.get(position).getProfImage()).fit().centerCrop().into(holder.profileImage);
            } else {
                Picasso.with(mContext).load("http://iphonewalls.net/wp-content/uploads/2014/02/Gradient%20Honeycomb%20Pattern%20iPhone%205%20Wallpaper.jpg").fit().centerCrop().into(holder.profileImage);
            }

            holder.profileName.setText(mainArray.get(position).getName());

            if (mainArray.get(position).getIsConnexUser().equals("yes")) {
                holder.userType.setText("Connex");
            } else {
                holder.userType.setText("Invite");
            }


//            if (currentSeletced != null) {
//                if (currentSeletced.equals(mainArray.get(position))) {
//                    holder.selectionView.setVisibility(View.VISIBLE);
//                    AlphaAnimation animation1 = new AlphaAnimation(0.0f, 1.0f);
//                    animation1.setDuration(300);
//                    animation1.setFillAfter(true);
//                    holder.selectionView.startAnimation(animation1);
//                } else {
//                    holder.selectionView.clearAnimation();
//                    holder.selectionView.setVisibility(View.GONE);
//                }
//            } else {
//                holder.selectionView.clearAnimation();
//                holder.selectionView.setVisibility(View.GONE);
//            }


//            holder.selectionView.setOnClickListener(new View.OnClickListener() {
//
//                @Override
//                public void onClick(View v) {
//                    currentSeletced = null;
//                    notifyDataSetChanged();
//                }
//            });

            holder.profileImage.setOnClickListener(new View.OnClickListener() {

                @Override
                public void onClick(View v) {

//                    if (mainArray.get(position).getIsConnexUser().equals("yes")) {
//                        currentSeletced = mainArray.get(position);
//                        notifyDataSetChanged();
//                    } else {
//                        currentSeletced = null;
//                        notifyDataSetChanged();
//                        if (callback != null) {
//                            callback.onItemClickdforInvitation(mainArray.get(position));
//                        }
//                    }


                    if (callback != null) {
                        callback.onDetailsClickd(mainArray.get(position));
                    }

                }
            });


            holder.menuMore.setOnClickListener(new View.OnClickListener() {

                @Override
                public void onClick(View v) {
                    if (callback != null) {
                        callback.onDetailsClickd(mainArray.get(position));
                    }
                }
            });


//            holder.voiceCall.setOnClickListener(new View.OnClickListener() {
//
//                @Override
//                public void onClick(View v) {
//                    if (callback != null) {
//                        callback.onItemClickdforVoice(mainArray.get(position));
//                    }
//                }
//            });
//
//
//            holder.videoCall.setOnClickListener(new View.OnClickListener() {
//
//                @Override
//                public void onClick(View v) {
//                    if (callback != null) {
//                        callback.onItemClickdforVideo(mainArray.get(position));
//                    }
//                }
//            });


        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    @Override
    public int getItemCount() {
        return mainArray.size();
    }

//    @Override
//    public HashMap<String, Integer> getMapIndex() {
//        return mMapIndex;
//    }

    public static class ViewHolder extends RecyclerView.ViewHolder {

        ImageView profileImage, menuMore, voiceCall, videoCall;
        AvenirRoman profileName;
        AvenirLight userType;
        View selectionView;

        public ViewHolder(View itemView) {
            super(itemView);
            selectionView = itemView.findViewById(R.id.select_overlay);
            menuMore = (ImageView) itemView.findViewById(R.id.menu_more);
            profileImage = (ImageView) itemView.findViewById(R.id.profile_image);

            voiceCall = (ImageView) itemView.findViewById(R.id.voice_call);
            videoCall = (ImageView) itemView.findViewById(R.id.video_call);

            profileName = (AvenirRoman) itemView.findViewById(R.id.profile_name);
            userType = (AvenirLight) itemView.findViewById(R.id.profile_type);

        }
    }


    public void setOnItemClickListener(onItemClickListener callback) {
        this.callback = callback;

    }

    public interface onItemClickListener {
        void onItemClickdforVoice(ContactDataType data);

        void onItemClickdforVideo(ContactDataType data);

        void onItemClickdforInvitation(ContactDataType data);

        void onDetailsClickd(ContactDataType data);
    }

    public int rowHight(final float screenWidth, final float noOfRows) {
        int hight = Math.round(screenWidth / noOfRows);
        return hight;
    }

}
