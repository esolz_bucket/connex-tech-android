package esolz.connexstudent.adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.LinkedList;

import esolz.connexstudent.ContactDetailsActivity;
import esolz.connexstudent.R;
import esolz.connexstudent.customview.AvenirRegular;
import esolz.connexstudent.customview.AvenirRoman;
import esolz.connexstudent.models.ContactDataType;

/**
 * Created by Saikat's Mac on 28/07/16.
 */

public class COntactDetailsMultipleAdapter extends RecyclerView.Adapter<COntactDetailsMultipleAdapter.ViewHolder> {

    private LinkedList<ContactDataType> mainArray;
    private Context mContext;
    private LayoutInflater inflater = null;
    private boolean isLoading = false;

    public COntactDetailsMultipleAdapter(Context mContext, LinkedList<ContactDataType> mainArray, boolean isLoading) {
        super();
        this.mainArray = mainArray;
        this.mContext = mContext;
        this.isLoading = isLoading;
        inflater = LayoutInflater.from(mContext);
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new ViewHolder(inflater.inflate(R.layout.items_contact_details, parent, false));
    }


    @Override
    public void onBindViewHolder(ViewHolder holder, final int position) {

        try {

            holder.contactNO.setText(mainArray.get(position).getOriginalPhoneNumber());

            if (mainArray.get(position).getDbID().equals("")) {
                holder.invitation.setVisibility(View.VISIBLE);
                holder.connexCall.setVisibility(View.GONE);
                holder.connexVideo.setVisibility(View.GONE);


                if (isLoading) {
                    holder.invitationText.setText("Loading....");
                } else {
                    holder.invitationText.setText("Invite " + mainArray.get(position).getName());
                    holder.invitation.setOnClickListener(new View.OnClickListener() {

                        @Override
                        public void onClick(View v) {

                            try {
                                ((ContactDetailsActivity) mContext).inviteOthers();
                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                        }
                    });
                }
            } else {
                holder.invitation.setVisibility(View.GONE);
                holder.connexCall.setVisibility(View.VISIBLE);
                holder.connexVideo.setVisibility(View.VISIBLE);


                holder.connexCall.setOnClickListener(new View.OnClickListener() {

                    @Override
                    public void onClick(View v) {
                        try {
                            ((ContactDetailsActivity) mContext).connexCalling(mainArray.get(position).getDbID(), "voice");
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                });

                holder.connexVideo.setOnClickListener(new View.OnClickListener() {

                    @Override
                    public void onClick(View v) {
                        try {
                            ((ContactDetailsActivity) mContext).connexCalling(mainArray.get(position).getDbID(), "video");
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                });
            }

        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    @Override
    public int getItemCount() {
        return mainArray.size();
    }


    public static class ViewHolder extends RecyclerView.ViewHolder {

        AvenirRoman contactNO = null;
        AvenirRegular invitationText = null;
        View connexCall, connexVideo, invitation;

        public ViewHolder(View itemView) {
            super(itemView);
            contactNO = (AvenirRoman) itemView.findViewById(R.id.phone_num);
            invitationText = (AvenirRegular) itemView.findViewById(R.id.invitew_text);
            connexCall = itemView.findViewById(R.id.connexblock_call);
            connexVideo = itemView.findViewById(R.id.connexblock_video);
            invitation = itemView.findViewById(R.id.invitation);
        }
    }
}