package esolz.connexstudent;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.view.ViewPager;
import android.support.v4.view.ViewPager.OnPageChangeListener;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.ImageView;

import esolz.connexstudent.ConnexService.ContactFetchingService;
import esolz.connexstudent.adapters.PagerAdapter;
import esolz.connexstudent.customview.AvenirLight;

public class TutorialActivity extends AppCompatActivity {


    private ViewPager tutorialPager = null;
    int[] images = new int[5];
    ImageView dot1, dot2, dot3, dot4, dot5;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_tutorial);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().hide();


        Intent i = new Intent(getApplicationContext(), ContactFetchingService.class);
        i.setAction(ContactFetchingService.ACTION_ON);
        startService(i);


        dot1 = (ImageView) findViewById(R.id.dot_1);
        dot2 = (ImageView) findViewById(R.id.dot_2);
        dot3 = (ImageView) findViewById(R.id.dot_3);
        dot4 = (ImageView) findViewById(R.id.dot_4);
        dot5 = (ImageView) findViewById(R.id.dot_5);

        tutorialPager = (ViewPager) findViewById(R.id.tutorial_page);
        images[0] = R.drawable.screen_one;
        images[1] = R.drawable.screen_two;
        images[2] = R.drawable.screen_three;
        images[3] = R.drawable.screen_four;
        images[4] = R.drawable.screen_five;
        tutorialPager.setAdapter(new PagerAdapter(TutorialActivity.this, images));

        findViewById(R.id.skip_me).setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                Intent ii = new Intent(TutorialActivity.this, LoginActivity.class);
                startActivity(ii);
                finish();
            }
        });

        findViewById(R.id.next_me).setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                try {
                    tutorialPager.setCurrentItem(tutorialPager.getCurrentItem() + 1);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });


        tutorialPager.addOnPageChangeListener(new OnPageChangeListener() {

            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

            }

            @Override
            public void onPageSelected(int position) {


                if (position == 4) {
                    findViewById(R.id.skip_me).setVisibility(View.GONE);
                    ((AvenirLight) findViewById(R.id.next_me)).setText("Done");
                } else {
                    findViewById(R.id.skip_me).setVisibility(View.VISIBLE);
                    ((AvenirLight) findViewById(R.id.next_me)).setText("Skip>>");
                }

                if (position == 0) {
                    dot1.setBackgroundResource(R.drawable.dot_fill);
                    dot2.setBackgroundResource(R.drawable.dot_empty);
                    dot3.setBackgroundResource(R.drawable.dot_empty);
                    dot4.setBackgroundResource(R.drawable.dot_empty);
                    dot5.setBackgroundResource(R.drawable.dot_empty);
                } else if (position == 1) {
                    dot1.setBackgroundResource(R.drawable.dot_empty);
                    dot2.setBackgroundResource(R.drawable.dot_fill);
                    dot3.setBackgroundResource(R.drawable.dot_empty);
                    dot4.setBackgroundResource(R.drawable.dot_empty);
                    dot5.setBackgroundResource(R.drawable.dot_empty);
                } else if (position == 2) {
                    dot1.setBackgroundResource(R.drawable.dot_empty);
                    dot2.setBackgroundResource(R.drawable.dot_empty);
                    dot3.setBackgroundResource(R.drawable.dot_fill);
                    dot4.setBackgroundResource(R.drawable.dot_empty);
                    dot5.setBackgroundResource(R.drawable.dot_empty);
                } else if (position == 3) {
                    dot1.setBackgroundResource(R.drawable.dot_empty);
                    dot2.setBackgroundResource(R.drawable.dot_empty);
                    dot3.setBackgroundResource(R.drawable.dot_empty);
                    dot4.setBackgroundResource(R.drawable.dot_fill);
                    dot5.setBackgroundResource(R.drawable.dot_empty);
                } else {
                    dot1.setBackgroundResource(R.drawable.dot_empty);
                    dot2.setBackgroundResource(R.drawable.dot_empty);
                    dot3.setBackgroundResource(R.drawable.dot_empty);
                    dot4.setBackgroundResource(R.drawable.dot_empty);
                    dot5.setBackgroundResource(R.drawable.dot_fill);
                }

            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });


    }

}
